# README #

### What is this repository for? ###
* A personal project where I've been playing around with various SQL ideas.
* The database is the basis for one used in an ongoing spare time personal C# project.
* The code is provided as is to demonstrate the sort of things I've been working on (or trying to).

### How do I get set up? ###

* This repository contains a VS2015 solution with a Database project inside it, download and unzip somewhere.
* Open the solution and try to build the project.
* You will probably need to tweak to add a missing assembly used for string manipulation.

## Examples ##

### Configuration Scripts ###
* PeopleAndPlaces/Script.GroupConcatInstall.sql (installs GroupConcat Assembly from data - http://groupconcat.codeplex.com)

### Creating Schemas, Tables, Constraints, Indexes and Extended Properties ###
* PeopleAndPlaces/data.sql
* PeopleAndPlaces/gui.sql
* PeopleAndPlaces/strings.sql
* PeopleAndPlaces/tests.sql
* PeopleAndPlaces/schemas/data/tables/
* PeopleAndPlaces/schemas/gui/tables/
* PeopleAndPlaces/schemas/tests/tables/

### Creating Triggers ###
* PeopleAndPlaces/schemas/data/tables/Files.sql (instead of update)
* PeopleAndPlaces/schemas/data/tables/_Errors.sql (for update)

### Creating Views ###
* PeopleAndPlaces/schemas/data/views/
* PeopleAndPlaces/schemas/tests/views/

### Custom Data Types ###
* PeopleAndPlaces/gui.sql
* PeopleAndPlaces/strings.sql

### Users, Roles, Logins and Permissions ###
* PeopleAndPlaces/data_users.sql
* PeopleAndPlaces/gui_users.sql
* PeopleAndPlaces/test_users.sql

### Scalar and Table Value Functions ###
* PeopleAndPlaces/schemas/data/functions/
* PeopleAndPlaces/schemas/gui/functions/
* PeopleAndPlaces/schemes/strings/functions/table-valued
* PeopleAndPlaces/schemes/tests/functions/

### Procedures ###
* PeopleAndPlaces/schemas/data/procedures/
* PeopleAndPlaces/schemas/gui/procedures/
* PeopleAndPlaces/schemas/tests/procedures/

### Error Handlers ###
* PeopleAndPlaces/schemas/data/procedures/_error_handler.sql
* PeopleAndPlaces/schemas/tests/procedures/_error_handler.sql

### Test Suite Entry Point ###
* PeopleAndPlaces/schemas/tests/procedures/Do.sql

## Who do I talk to? ##
* Repo owner