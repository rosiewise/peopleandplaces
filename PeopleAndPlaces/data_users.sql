﻿CREATE ROLE [data_users];
GO

-- Normal database user
CREATE LOGIN [data_user] WITH PASSWORD = 'data123';
GO
CREATE USER [data_user];
GO
ALTER ROLE [data_users] ADD MEMBER [data_user];
GO
GRANT CONNECT TO [data_user];
GO

-- reserved for database impersonation
CREATE LOGIN [db_data_user] WITH PASSWORD = 'secret123';
GO
CREATE USER [db_data_user];
GO
ALTER ROLE [data_users] ADD MEMBER [db_data_user];
GO
GRANT CONNECT TO [db_data_user];
GO

-- Permissions for data_users
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, EXECUTE, ALTER ON SCHEMA::data TO [data_users];
GO
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, EXECUTE ON SCHEMA::gui TO [data_users];
GO
GRANT SELECT, EXECUTE ON SCHEMA::strings TO [data_users];
GO