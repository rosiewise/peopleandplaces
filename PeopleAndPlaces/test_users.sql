﻿CREATE ROLE [test_users];
GO

-- Normal database user
CREATE LOGIN [test_user] WITH PASSWORD = 'test123';
GO
CREATE USER [test_user];
GO
ALTER ROLE [test_users] ADD MEMBER [test_user];
GO
GRANT CONNECT TO [test_user];
GO

-- Permissions for test_users
GRANT SELECT ON SCHEMA::data TO [test_users];
GO
DENY DELETE, INSERT, REFERENCES, UPDATE, EXECUTE, ALTER ON SCHEMA::data TO [test_users];
GO
GRANT SELECT, EXECUTE ON SCHEMA::strings TO [test_users];
GO
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, EXECUTE ON SCHEMA::tests TO [test_users];
GO