﻿CREATE ROLE [gui_users];
GO

-- Normal database user
CREATE LOGIN [gui_user] WITH PASSWORD = 'gui123';
GO
CREATE USER [gui_user];
GO
ALTER ROLE [gui_users] ADD MEMBER [gui_user];
GO
GRANT CONNECT TO [gui_user];
GO

-- Permissions for test_users
GRANT SELECT ON SCHEMA::data TO [gui_users];
GO
DENY DELETE, INSERT, REFERENCES, UPDATE, EXECUTE, ALTER ON SCHEMA::data TO [gui_users];
GO
GRANT SELECT, EXECUTE ON SCHEMA::strings TO [gui_users];
GO
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, EXECUTE ON SCHEMA::gui TO [gui_users];
GO