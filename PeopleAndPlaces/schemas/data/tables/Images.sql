﻿CREATE TABLE [data].[Images] (
    [FileID]      INT            IDENTITY (1, 1) NOT NULL,
    [Title]       NVARCHAR (100) NULL,
    [Description] NVARCHAR (MAX) NULL,
    [PlaceID]     INT            NULL,
    CONSTRAINT [PK_Images] PRIMARY KEY CLUSTERED ([FileID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Images_PlaceID] FOREIGN KEY ([PlaceID]) REFERENCES [data].[Places] ([ID]) ON DELETE SET NULL,
    CONSTRAINT [FK_Images_FileID] FOREIGN KEY ([FileID]) REFERENCES [data].[Files] ([ID])
);