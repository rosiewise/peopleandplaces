﻿CREATE TABLE [data].[_Errors] (
    [Date]           DATETIME        NOT NULL,
    [Object]         VARCHAR (50)    NOT NULL,
    [Note]           VARCHAR (350)   NOT NULL,
    [Occurances]     INT             DEFAULT ((1)) NULL,
    [Error]          NVARCHAR (4000) NULL,
    [ErrorNumber]    INT             NULL,
    [ErrorSeverity]  INT             NULL,
    [ErrorState]     INT             NULL,
    [ErrorProcedure] NVARCHAR (128)  NULL,
    [ErrorLine]      INT             NULL,
    CONSTRAINT [PK__Errors] PRIMARY KEY CLUSTERED ([Object] ASC, [Note] ASC)
);

GO
CREATE TRIGGER [data].[Trigger_Update__Errors]
    ON [data].[_Errors]
    FOR UPDATE
    AS
    BEGIN
        SET NoCount ON;
		UPDATE [_Errors]
		SET [_Errors].[Occurances] = [_Errors].[Occurances] + 1
		FROM [_Errors]
		JOIN [inserted] ON [_Errors].[Object] = [inserted].[Object]
			AND [_Errors].[Note] = [inserted].[Note]
    END