﻿CREATE TABLE [data].[Places] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [Place]      NVARCHAR (50) NOT NULL,
    [AddressID]  INT           NULL,
    [LocationID] INT           NULL,
    [RegionID]   INT           NULL,
    [CountryID]  INT           NULL,
    CONSTRAINT [PK_Places] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Places_AddressID] FOREIGN KEY ([AddressID]) REFERENCES [data].[Addresses] ([ID]) ON DELETE SET NULL,
    CONSTRAINT [FK_Places_RegionID] FOREIGN KEY ([RegionID]) REFERENCES [data].[Regions] ([ID]) ON DELETE SET NULL,
    CONSTRAINT [FK_Places_CountryID] FOREIGN KEY ([CountryID]) REFERENCES [data].[Countries] ([ID]) ON DELETE SET NULL,
    CONSTRAINT [FK_Places_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [data].[Locations] ([ID]) ON DELETE SET NULL
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Places]
    ON [data].[Places]([Place] ASC, [AddressID] ASC, [LocationID] ASC, [RegionID] ASC, [CountryID] ASC) WITH (IGNORE_DUP_KEY = ON);