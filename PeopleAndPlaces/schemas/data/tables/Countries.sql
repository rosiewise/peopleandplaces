﻿CREATE TABLE [data].[Countries] (
    [ID]        INT           IDENTITY (1, 1) NOT NULL,
    [Country]   NVARCHAR (40) NOT NULL,
    [Continent] NVARCHAR (13) NOT NULL,
    CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [CK_Countries_Continent] CHECK ([Continent]='South America' OR [Continent]='North America' OR [Continent]='Middle East' OR [Continent]='Europe' OR [Continent]='Caribbean' OR [Continent]='Australasia' OR [Continent]='Asia' OR [Continent]='Africa')
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Countries]
    ON [data].[Countries]([Country] ASC) WITH (IGNORE_DUP_KEY = ON);