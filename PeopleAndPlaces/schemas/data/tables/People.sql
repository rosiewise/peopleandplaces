﻿CREATE TABLE [data].[People] (
    [PersonID]  INT           IDENTITY (1, 1) NOT NULL,
    [FirstName] NVARCHAR (50) NOT NULL,
    [LastName]  NVARCHAR (50) NULL,
    [Gender]    TINYINT       NOT NULL,
    [DOB]       DATE          NULL,
    CONSTRAINT [PK_People] PRIMARY KEY CLUSTERED ([PersonID] ASC) WITH (IGNORE_DUP_KEY = ON)
);

GO
CREATE NONCLUSTERED INDEX [IX_People_DOB]
    ON [data].[People]([DOB] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_People_Gender]
    ON [data].[People]([Gender] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_People_Name]
    ON [data].[People]([FirstName] ASC, [LastName] ASC);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_People]
    ON [data].[People]([FirstName] ASC, [LastName] ASC, [Gender] ASC, [DOB] ASC) WITH (IGNORE_DUP_KEY = ON);