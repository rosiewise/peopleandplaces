﻿CREATE TABLE [data].[People_People] (
    [PersonID1]     INT NOT NULL,
    [PersonID2]     INT NOT NULL,
    [AssociationID] INT NOT NULL,
    CONSTRAINT [PK_People_People] PRIMARY KEY CLUSTERED ([PersonID1] ASC, [PersonID2] ASC, [AssociationID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_People_People_PersonID1] FOREIGN KEY ([PersonID1]) REFERENCES [data].[People] ([PersonID]),
    CONSTRAINT [FK_People_People_PersonID2] FOREIGN KEY ([PersonID2]) REFERENCES [data].[People] ([PersonID]),
    CONSTRAINT [FK_People_People_AssociationID] FOREIGN KEY ([AssociationID]) REFERENCES [data].[Associations] ([ID]) ON DELETE CASCADE
);

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PersonID1 has an Association to PersonID2', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'People_People', @level2type = N'COLUMN', @level2name = N'PersonID1';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PersonID2 is connected to PersonID1 because of the Association type described', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'People_People', @level2type = N'COLUMN', @level2name = N'PersonID2';