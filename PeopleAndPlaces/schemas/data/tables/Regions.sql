﻿CREATE TABLE [data].[Regions] (
    [ID]        INT           IDENTITY (1, 1) NOT NULL,
    [Region]    NVARCHAR (50) NOT NULL,
    [CountryID] INT           NULL,
    CONSTRAINT [PK_Regions] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Regions_CountryID] FOREIGN KEY ([CountryID]) REFERENCES [data].[Countries] ([ID]) ON DELETE SET NULL
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Regions]
    ON [data].[Regions]([Region] ASC, [CountryID] ASC) WITH (IGNORE_DUP_KEY = ON);

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The region is akin to county/state/province', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Regions', @level2type = N'COLUMN', @level2name = N'Region';