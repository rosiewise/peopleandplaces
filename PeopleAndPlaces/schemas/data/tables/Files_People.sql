﻿CREATE TABLE [data].[Files_People] (
    [FileID]   INT NOT NULL,
    [PersonID] INT NOT NULL,
    [RoleID]   INT NOT NULL,
    CONSTRAINT [PK_Files_People] PRIMARY KEY CLUSTERED ([FileID] ASC, [PersonID] ASC, [RoleID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Files_People_PersonID] FOREIGN KEY ([PersonID]) REFERENCES [data].[People] ([PersonID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Files_People_RoleID] FOREIGN KEY ([RoleID]) REFERENCES [data].[PersonRoles] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Files_People_FileID] FOREIGN KEY ([FileID]) REFERENCES [data].[Files] ([ID]) ON DELETE CASCADE
);