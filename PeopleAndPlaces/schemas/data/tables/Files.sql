﻿CREATE TABLE [data].[Files] (
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    [DirectoryID]  INT            NULL,
    [FileName]     NVARCHAR (255) NOT NULL,
    [Extension]    NVARCHAR (5)   NOT NULL,
    [MimeType]     NVARCHAR (50)  NOT NULL,
    [Actual]       BIT            NULL,
    [HashID]       INT            NULL,
    [Size]         INT            NULL,
    [Created]      DATETIME       NULL,
    [Updated]      DATETIME       NULL,
    [InSynch]      BIT            NULL,
    [Synchronised] DATETIME       NULL,
    [RowUpdated]   DATETIME       NULL,
    [Imported]     DATETIME       NULL,
    CONSTRAINT [PK_Files] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Files_DirectoryID] FOREIGN KEY ([DirectoryID]) REFERENCES [data].[Directories] ([ID]),
    CONSTRAINT [FK_Files_HashID] FOREIGN KEY ([HashID]) REFERENCES [data].[Hashes] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Files]
    ON [data].[Files]([DirectoryID] ASC, [FileName] ASC, [Extension] ASC) WITH (IGNORE_DUP_KEY = ON);

GO
CREATE TRIGGER [data].[Trigger_InsteadOfUpdate_Files]
    ON [data].[Files]
    INSTEAD OF UPDATE
    AS
    BEGIN
		SET NoCount ON;
		IF (UPDATE(InSynch))
		BEGIN
			UPDATE [Files]
			SET
				[Files].[InSynch] = CASE
										WHEN [inserted].[InSynch] IS NOT NULL
										THEN [inserted].[InSynch]
										ELSE 0
									END,
				[Files].[Synchronised] = CASE
										WHEN [inserted].[InSynch] = 1
										THEN GETDATE()
										ELSE [deleted].[Synchronised]
									END
			FROM [Files]
			JOIN [inserted] ON [inserted].[ID] = [Files].[ID]
			JOIN [deleted] ON [deleted].[ID] = [Files].[ID];
		END
		ELSE
		BEGIN
			UPDATE [Files]
			SET [Files].[InSynch] = 0
			FROM [Files]
			JOIN [inserted] ON [inserted].[ID] = [Files].[ID];
		END;

		UPDATE [Files]
		SET
			[Files].[DirectoryID] = CASE
									WHEN [inserted].[DirectoryID] IS NOT NULL
									THEN 
										CASE WHEN EXISTS(SELECT [ID] FROM [Directories] WHERE [ID] = [inserted].[DirectoryID])
										THEN [inserted].[DirectoryID]
										ELSE NULL
										END
									ELSE [deleted].[DirectoryID]
								END,
			[Files].[FileName] = CASE
									WHEN [inserted].[FileName] IS NOT NULL
									THEN [inserted].[FileName]
									ELSE [deleted].[FileName]
								END,
			[Files].[Extension] = CASE
									WHEN [inserted].[Extension] IS NOT NULL
									THEN [inserted].[Extension]
									ELSE [deleted].[Extension]
								END,
			[Files].[MimeType] = CASE
									WHEN [inserted].[MimeType] IS NOT NULL
									THEN [inserted].[MimeType]
									ELSE [deleted].[MimeType]
								END,
			[Files].[Actual] = CASE
									WHEN [inserted].[Actual] IS NOT NULL
									THEN [inserted].[Actual]
									ELSE [deleted].[Actual]
								END,
			[Files].[HashID] = CASE
									WHEN [inserted].[HashID] IS NOT NULL
									THEN
										CASE WHEN EXISTS(SELECT [ID] FROM [Hashes] WHERE [ID] = [inserted].[HashID] AND [KeyType] = 'file')
										THEN [inserted].[HashID]
										ELSE NULL
										END
									ELSE [deleted].[HashID]
								END,
			[Files].[Size] = CASE
									WHEN [inserted].[Size] IS NOT NULL
									THEN [inserted].[Size]
									ELSE [deleted].[Size]
								END,
			[Files].[Created] = CASE
									WHEN [inserted].[Created] IS NOT NULL
									THEN [inserted].[Created]
									ELSE [deleted].[Created]
								END,
			[Files].[Updated] = CASE
									WHEN [inserted].[Updated] IS NOT NULL
									THEN [inserted].[Updated]
									ELSE [deleted].[Updated]
								END,
			[Files].[RowUpdated] = GETDATE()
		FROM [Files]
		JOIN [inserted] ON [inserted].[ID] = [Files].[ID]
		JOIN [deleted] ON [deleted].[ID] = [Files].[ID];
    END

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Is this file still existing', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Files', @level2type = N'COLUMN', @level2name = N'Actual';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the original date of the file which is likely to be before the imported (created) date of this row', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Files', @level2type = N'COLUMN', @level2name = N'Created';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Use NULL to either set to the default value or ignore change', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Files', @level2type = N'COLUMN', @level2name = N'InSynch';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the date the file details were last synchronised with the database', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Files', @level2type = N'COLUMN', @level2name = N'Synchronised';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the date this row was last updated, does not relate at all to the file', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Files', @level2type = N'COLUMN', @level2name = N'RowUpdated';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the date the data was inserted into the table', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Files', @level2type = N'COLUMN', @level2name = N'Imported';