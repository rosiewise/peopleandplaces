﻿CREATE TABLE [data].[Addresses] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [Unit]       NVARCHAR (20) NULL,
    [Street]     NVARCHAR (50) NULL,
    [Locality]   NVARCHAR (50) NULL,
    [LocationID] INT           NULL,
    [Code]       NVARCHAR (50) NULL,
    CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Addresses_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [data].[Locations] ([ID]) ON DELETE SET NULL
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Addresses]
    ON [data].[Addresses]([Unit] ASC, [Street] ASC, [Locality] ASC, [LocationID] ASC, [Code] ASC) WITH (IGNORE_DUP_KEY = ON);

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number or other identifier for specific unit', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Unit';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of the street (address line 1)', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Street';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Locality of the address (address line 2)', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Locality';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Akin to Zip/Postcode', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Code';