﻿CREATE TABLE [data].[Directories] (
    [ID]     INT            IDENTITY (1, 1) NOT NULL,
    [Drive]  NVARCHAR (5)   NOT NULL,
    [Path]   NVARCHAR (MAX) NOT NULL,
    [Actual] BIT            DEFAULT ((1)) NOT NULL,
    [HashID] INT            NOT NULL,
    CONSTRAINT [PK_Directories] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Directories_HashID] FOREIGN KEY ([HashID]) REFERENCES [data].[Hashes] ([ID])
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Directories]
    ON [data].[Directories]([Drive] ASC, [HashID] ASC) WITH (IGNORE_DUP_KEY = ON);