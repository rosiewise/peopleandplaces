﻿CREATE TABLE [data].[Hashes] (
    [ID]      INT          IDENTITY (1, 1) NOT NULL,
    [KeyType] VARCHAR (9)  NOT NULL,
    [Hash]    VARCHAR (34) NOT NULL,
    CONSTRAINT [PK_Hashes] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [CK_Hashes_KeyType] CHECK ([KeyType]='directory' OR [KeyType]='file')
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Hashes]
    ON [data].[Hashes]([KeyType] ASC, [Hash] ASC) WITH (IGNORE_DUP_KEY = ON);