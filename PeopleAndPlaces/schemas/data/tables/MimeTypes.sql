﻿CREATE TABLE [data].[MimeTypes] (
    [Extension]   VARCHAR (10)  NOT NULL,
    [MimeType]    VARCHAR (50)  NOT NULL,
    [Description] VARCHAR (255) NOT NULL,
    [Discover]    BIT           DEFAULT ((0)) NOT NULL,
    [Supported]   BIT           DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Extension] ASC)
);