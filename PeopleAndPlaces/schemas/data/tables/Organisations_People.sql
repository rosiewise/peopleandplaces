﻿CREATE TABLE [data].[Organisations_People] (
    [OrganisationID] INT NOT NULL,
    [PersonID]       INT NOT NULL,
    [RoleID]         INT NOT NULL,
    CONSTRAINT [PK_Organisations_People] PRIMARY KEY CLUSTERED ([OrganisationID] ASC, [PersonID] ASC, [RoleID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Organisations_People_OrganisationID] FOREIGN KEY ([OrganisationID]) REFERENCES [data].[Organisations] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Organisations_People_PersonID] FOREIGN KEY ([PersonID]) REFERENCES [data].[People] ([PersonID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Organisations_People_RoleID] FOREIGN KEY ([RoleID]) REFERENCES [data].[PersonRoles] ([ID]) ON DELETE CASCADE
);