﻿CREATE TABLE [data].[Associations] (
    [ID]          INT        IDENTITY (1, 1) NOT NULL,
    [Association] NCHAR (25) NOT NULL,
    CONSTRAINT [PK_Associations] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (IGNORE_DUP_KEY = ON)
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Associations]
    ON [data].[Associations]([Association] ASC) WITH (IGNORE_DUP_KEY = ON);