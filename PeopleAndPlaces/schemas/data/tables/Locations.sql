﻿CREATE TABLE [data].[Locations] (
    [ID]        INT           IDENTITY (1, 1) NOT NULL,
    [Location]  NVARCHAR (50) NOT NULL,
    [CountryID] INT           NULL,
    [RegionID]  INT           NULL,
    CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Locations_RegionID] FOREIGN KEY ([RegionID]) REFERENCES [data].[Regions] ([ID]) ON DELETE SET NULL,
    CONSTRAINT [FK_Locations_CountryID] FOREIGN KEY ([CountryID]) REFERENCES [data].[Countries] ([ID]) ON DELETE SET NULL
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Locations]
    ON [data].[Locations]([Location] ASC, [CountryID] ASC, [RegionID] ASC) WITH (IGNORE_DUP_KEY = ON);

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This will be the town/city level', @level0type = N'SCHEMA', @level0name = N'data', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'Location';