﻿CREATE TABLE [data].[Organisations] (
    [ID]               INT            IDENTITY (1, 1) NOT NULL,
    [OrganisationName] NVARCHAR (50)  NOT NULL,
    [Description]      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Organisations] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (IGNORE_DUP_KEY = ON)
);