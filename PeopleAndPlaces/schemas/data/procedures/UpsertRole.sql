﻿/*
UpsertRole procedure
--------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-02-29

Tables:				PersonRoles
PK_PersonRoles:		ID
UIX_PersonRoles:	Role

UPDATE: only when @ID and @Role provided
INSERT: only when @Role is provided without @ID
RETURNS: ID of the role found in UIX_PersonRoles (@Role) (not necessarily @ID if provided)
*/

CREATE PROCEDURE [data].[UpsertRole]
	@ID INT = NULL,
	@Role NVARCHAR(50) = NULL
AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	-----------
	-- LOGIC --
	-----------
	BEGIN TRY
		DECLARE @RoleID INT;

		IF (@ID IS NULL AND @Role IS NULL)
		BEGIN
			EXEC [data].[*commit];		
			RAISERROR('%s', 15, 1, 'No @ID or @Role was provided.');
		END;

		-- IF @ID WE ARE UPDATING THE ROW
		IF (@ID IS NOT NULL AND @Role IS NOT NULL)
		BEGIN
			
			SELECT
				@RoleID = COALESCE(MAX([ID]), 0)
			FROM [data].[PersonRoles]
			WHERE [ID] = @ID;

			IF @RoleID > 0
			BEGIN
				BEGIN TRY
					UPDATE [data].[PersonRoles]
					SET [Role] = LTRIM(RTRIM(@Role))
					WHERE [ID] = @ID;
				END	TRY
				BEGIN CATCH
					SET @RoleID = [data].[RoleID](@Role);
				END CATCH;
			END;
		END
		ELSE
		BEGIN
			-- IDENTIFY USING NAME OR INSERT
			SET @RoleID = [data].[RoleID](@Role);

			IF (@RoleID IS NULL)
			BEGIN
				INSERT INTO [data].[PersonRoles] (
					[Role]
				) VALUES (
					@Role
				);
				SET @RoleID = [data].[RoleID](@Role);
			END;
		END;
		RETURN @RoleID;
	END TRY
	BEGIN CATCH
		DECLARE @note VARCHAR(100);

		SELECT @note = [Value]
		FROM [strings].[input_UpsertRole](@ID, @Role, NULL);
		
		EXEC [data].[*error_handler] 'UpsertRole', @note;

		RETURN -1;
	END CATCH
END;