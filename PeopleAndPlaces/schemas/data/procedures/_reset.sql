﻿/*
_Reset procedure, resets the DB ready to run tests on it
--------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-01-08
*/

CREATE PROCEDURE [data].[*reset] (
	@AddTestData BIT = 0
)
AS
	PRINT SYSTEM_USER + ' ' + USER;
	SET NOCOUNT ON;

	DECLARE @err NVARCHAR(4000);
	DECLARE @errNo INT;
	DECLARE @errSev INT;
	DECLARE @errState INT;
	DECLARE @errProc NVARCHAR(128);
	DECLARE @errLine INT;

	BEGIN TRY
		-- DROP FOREIGN KEYS
		ALTER TABLE [data].[Addresses] DROP CONSTRAINT [FK_Addresses_LocationID];
		ALTER TABLE [data].[Directories] DROP CONSTRAINT [FK_Directories_HashID];
		ALTER TABLE [data].[Files] DROP CONSTRAINT [FK_Files_DirectoryID];
		ALTER TABLE [data].[Files] DROP CONSTRAINT [FK_Files_HashID];
		ALTER TABLE [data].[Files_People] DROP CONSTRAINT [FK_Files_People_PersonID];
		ALTER TABLE [data].[Files_People] DROP CONSTRAINT [FK_Files_People_RoleID];
		ALTER TABLE [data].[Files_People] DROP CONSTRAINT [FK_Files_People_FileID];
		ALTER TABLE [data].[Images] DROP CONSTRAINT [FK_Images_FileID];
		ALTER TABLE [data].[Images] DROP CONSTRAINT [FK_Images_PlaceID];
		ALTER TABLE [data].[Locations] DROP CONSTRAINT [FK_Locations_RegionID];
		ALTER TABLE [data].[Locations] DROP CONSTRAINT [FK_Locations_CountryID];
		ALTER TABLE [data].[Organisations_People] DROP CONSTRAINT [FK_Organisations_People_OrganisationID];
		ALTER TABLE [data].[Organisations_People] DROP CONSTRAINT [FK_Organisations_People_PersonID];
		ALTER TABLE [data].[Organisations_People] DROP CONSTRAINT [FK_Organisations_People_RoleID];
		ALTER TABLE [data].[People_People] DROP CONSTRAINT [FK_People_People_PersonID1];
		ALTER TABLE [data].[People_People] DROP CONSTRAINT [FK_People_People_PersonID2];
		ALTER TABLE [data].[People_People] DROP CONSTRAINT [FK_People_People_AssociationID];
		ALTER TABLE [data].[Places] DROP CONSTRAINT [FK_Places_AddressID];
		ALTER TABLE [data].[Places] DROP CONSTRAINT [FK_Places_RegionID];
		ALTER TABLE [data].[Places] DROP CONSTRAINT [FK_Places_CountryID];
		ALTER TABLE [data].[Places] DROP CONSTRAINT [FK_Places_LocationID];
		ALTER TABLE [data].[Regions] DROP CONSTRAINT [FK_Regions_CountryID];

		-- DROP PRIMARY KEYS
		ALTER TABLE [data].[Addresses] DROP CONSTRAINT [PK_Addresses];
		ALTER TABLE [data].[Associations] DROP CONSTRAINT [PK_Associations];
		--ALTER TABLE [data].[Countries] DROP CONSTRAINT [PK_Countries];
		ALTER TABLE [data].[Directories] DROP CONSTRAINT [PK_Directories];
		ALTER TABLE [data].[Files] DROP CONSTRAINT [PK_Files];
		ALTER TABLE [data].[Files_People] DROP CONSTRAINT [PK_Files_People];
		ALTER TABLE [data].[Hashes] DROP CONSTRAINT [PK_Hashes];
		ALTER TABLE [data].[Images] DROP CONSTRAINT [PK_Images];
		ALTER TABLE [data].[Locations] DROP CONSTRAINT [PK_Locations];
		ALTER TABLE [data].[Organisations] DROP CONSTRAINT [PK_Organisations];
		ALTER TABLE [data].[Organisations_People] DROP CONSTRAINT [PK_Organisations_People];
		ALTER TABLE [data].[People] DROP CONSTRAINT [PK_People];
		ALTER TABLE [data].[People_People] DROP CONSTRAINT [PK_People_People];
		ALTER TABLE [data].[PersonRoles] DROP CONSTRAINT [PK_PersonRoles];
		ALTER TABLE [data].[Places] DROP CONSTRAINT [PK_Places];
		ALTER TABLE [data].[Regions] DROP CONSTRAINT [PK_Regions];

		-- DROP INDEXES
		DROP INDEX [IX_People_DOB] ON [data].[People];
		DROP INDEX [IX_People_Gender] ON [data].[People];
		DROP INDEX [IX_People_Name] ON [data].[People];

		DROP INDEX [UIX_Addresses] ON [data].[Addresses];
		DROP INDEX [UIX_Associations] ON [data].[Associations];
		--DROP INDEX [UIX_Countries] ON [data].[Countries];
		DROP INDEX [UIX_Directories] ON [data].[Directories];
		DROP INDEX [UIX_Files] ON [data].[Files];
		DROP INDEX [UIX_Hashes] ON [data].[Hashes];
		DROP INDEX [UIX_Locations] ON [data].[Locations];
		DROP INDEX [UIX_People] ON [data].[People];
		DROP INDEX [UIX_PersonRoles] ON [data].[PersonRoles];
		DROP INDEX [UIX_Places] ON [data].[Places];
		DROP INDEX [UIX_Regions] ON [data].[Regions];

		-- TRUNCATE TABLES
		-- ---------------
		TRUNCATE TABLE [data].[Images];

		-- Location type fields
		TRUNCATE TABLE [data].[Places];
		TRUNCATE TABLE [data].[Addresses];
		TRUNCATE TABLE [data].[Locations];
		TRUNCATE TABLE [data].[Regions];
		--TRUNCATE TABLE [data].[Countries];

		-- People type fields
		TRUNCATE TABLE [data].[Files_People];
		TRUNCATE TABLE [data].[People_People];
		TRUNCATE TABLE [data].[Organisations_People];
		TRUNCATE TABLE [data].[Organisations];
		TRUNCATE TABLE [data].[Associations];
		TRUNCATE TABLE [data].[PersonRoles];
		TRUNCATE TABLE [data].[People];

		-- Files and the like
		TRUNCATE TABLE [data].[Hashes];
		TRUNCATE TABLE [data].[Files];
		TRUNCATE TABLE [data].[Directories];
		----------------------------------

		-- ADD PRIMARY KEYS
		ALTER TABLE [data].[Addresses]
			ADD CONSTRAINT [PK_Addresses]
			PRIMARY KEY CLUSTERED ([ID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[Associations]
			ADD CONSTRAINT [PK_Associations]
			PRIMARY KEY CLUSTERED ([ID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		/*
		ALTER TABLE [data].[Countries]
			ADD CONSTRAINT [PK_Countries]
			PRIMARY KEY CLUSTERED ([ID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
			*/
		ALTER TABLE [data].[Directories]
			ADD CONSTRAINT [PK_Directories]
			PRIMARY KEY CLUSTERED ([ID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[Files]
			ADD CONSTRAINT [PK_Files]
			PRIMARY KEY CLUSTERED ([ID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[Files_People]
			ADD CONSTRAINT [PK_Files_People]
			PRIMARY KEY CLUSTERED ([FileID] ASC, [PersonID] ASC, [RoleID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[Hashes]
			ADD CONSTRAINT [PK_Hashes]
			PRIMARY KEY CLUSTERED ([ID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[Images]
			ADD CONSTRAINT [PK_Images]
			PRIMARY KEY CLUSTERED ([FileID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[Locations]
			ADD CONSTRAINT [PK_Locations]
			PRIMARY KEY CLUSTERED ([ID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[Organisations]
			ADD CONSTRAINT [PK_Organisations]
			PRIMARY KEY CLUSTERED ([ID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[Organisations_People]
			ADD CONSTRAINT [PK_Organisations_People]
			PRIMARY KEY CLUSTERED ([OrganisationID] ASC, [PersonID] ASC, [RoleID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[People]
			ADD CONSTRAINT [PK_People]
			PRIMARY KEY CLUSTERED ([PersonID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[People_People]
			ADD CONSTRAINT [PK_People_People]
			PRIMARY KEY CLUSTERED ([PersonID1] ASC, [PersonID2] ASC, [AssociationID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[PersonRoles]
			ADD CONSTRAINT [PK_PersonRoles]
			PRIMARY KEY CLUSTERED ([ID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[Places]
			ADD CONSTRAINT [PK_Places]
			PRIMARY KEY CLUSTERED ([ID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		ALTER TABLE [data].[Regions]
			ADD CONSTRAINT [PK_Regions]
			PRIMARY KEY CLUSTERED ([ID] ASC)
			WITH (IGNORE_DUP_KEY = ON);

		-- ADD INDEXES
		CREATE NONCLUSTERED INDEX [IX_People_DOB]
		    ON [data].[People]([DOB] ASC);
		CREATE NONCLUSTERED INDEX [IX_People_Gender]
			ON [data].[People]([Gender] ASC);
		CREATE NONCLUSTERED INDEX [IX_People_Name]
		    ON [data].[People]([FirstName] ASC, [LastName] ASC);
		
		CREATE UNIQUE NONCLUSTERED INDEX [UIX_Addresses]
		    ON [data].[Addresses]([Unit] ASC, [Street] ASC, [Locality] ASC, [LocationID] ASC, [Code] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		CREATE UNIQUE NONCLUSTERED INDEX [UIX_Associations]
			ON [data].[Associations]([Association] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		/*
		CREATE UNIQUE NONCLUSTERED INDEX [UIX_Countries]
		    ON [data].[Countries]([Country] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		*/
		CREATE UNIQUE NONCLUSTERED INDEX [UIX_Directories]
		    ON [data].[Directories]([Drive] ASC, [HashID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		CREATE UNIQUE NONCLUSTERED INDEX [UIX_Files]
			ON [data].[Files]([DirectoryID] ASC, [FileName] ASC, [Extension] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		CREATE UNIQUE NONCLUSTERED INDEX [UIX_Hashes]
		    ON [data].[Hashes]([KeyType] ASC, [Hash] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		CREATE UNIQUE NONCLUSTERED INDEX [UIX_Locations]
		    ON [data].[Locations]([Location] ASC, [CountryID] ASC, [RegionID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		CREATE UNIQUE NONCLUSTERED INDEX [UIX_People]
		    ON [data].[People]([FirstName] ASC, [LastName] ASC, [Gender] ASC, [DOB] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		CREATE UNIQUE NONCLUSTERED INDEX [UIX_PersonRoles]
			ON [data].[PersonRoles]([Role] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		CREATE UNIQUE NONCLUSTERED INDEX [UIX_Places]
			ON [data].[Places]([Place] ASC, [AddressID] ASC, [LocationID] ASC, [RegionID] ASC, [CountryID] ASC)
			WITH (IGNORE_DUP_KEY = ON);
		CREATE UNIQUE NONCLUSTERED INDEX [UIX_Regions]
		    ON [data].[Regions]([Region] ASC, [CountryID] ASC)
			WITH (IGNORE_DUP_KEY = ON);

		-- ADD FOREIGN KEYS
		ALTER TABLE [data].[Addresses]
			ADD CONSTRAINT [FK_Addresses_LocationID]
			FOREIGN KEY ([LocationID]) REFERENCES [data].[Locations] ([ID])
			ON DELETE SET NULL;
		ALTER TABLE [data].[Directories]
			ADD CONSTRAINT [FK_Directories_HashID]
			FOREIGN KEY ([HashID]) REFERENCES [data].[Hashes] ([ID]);
			--ON DELETE SET NULL;
		ALTER TABLE [data].[Files]
			ADD CONSTRAINT [FK_Files_DirectoryID]
			FOREIGN KEY ([DirectoryID]) REFERENCES [data].[Directories] ([ID]);
			--ON DELETE SET NULL;
		ALTER TABLE [data].[Files]
			ADD CONSTRAINT [FK_Files_HashID]
			FOREIGN KEY ([HashID]) REFERENCES [data].[Hashes] ([ID]);
			--ON DELETE SET NULL;
		ALTER TABLE [data].[Files_People]
			ADD CONSTRAINT [FK_Files_People_PersonID]
			FOREIGN KEY ([PersonID]) REFERENCES [data].[People] ([PersonID])
			ON DELETE CASCADE;
		ALTER TABLE [data].[Files_People]
			ADD CONSTRAINT [FK_Files_People_RoleID]
			FOREIGN KEY ([RoleID]) REFERENCES [data].[PersonRoles] ([ID])
			ON DELETE CASCADE;
		ALTER TABLE [data].[Files_People]
			ADD CONSTRAINT [FK_Files_People_FileID]
			FOREIGN KEY ([FileID]) REFERENCES [data].[Files] ([ID])
			ON DELETE CASCADE;
		ALTER TABLE [data].[Images]
			ADD CONSTRAINT [FK_Images_PlaceID]
			FOREIGN KEY ([PlaceID]) REFERENCES [data].[Places] ([ID])
			ON DELETE SET NULL;
		ALTER TABLE [data].[Images]
			ADD CONSTRAINT [FK_Images_FileID]
			FOREIGN KEY ([FileID]) REFERENCES [data].[Files] ([ID]);
			--ON DELETE SET NULL;
		ALTER TABLE [data].[Locations]
			ADD CONSTRAINT [FK_Locations_RegionID]
			FOREIGN KEY ([RegionID]) REFERENCES [data].[Regions] ([ID])
			ON DELETE SET NULL;
		ALTER TABLE [data].[Locations]
			ADD CONSTRAINT [FK_Locations_CountryID]
			FOREIGN KEY ([CountryID]) REFERENCES [data].[Countries] ([ID])
			ON DELETE SET NULL;
		ALTER TABLE [data].[Organisations_People]
			ADD CONSTRAINT [FK_Organisations_People_OrganisationID]
			FOREIGN KEY ([OrganisationID]) REFERENCES [data].[Organisations] ([ID])
			ON DELETE CASCADE;
		ALTER TABLE [data].[Organisations_People]
			ADD CONSTRAINT [FK_Organisations_People_PersonID]
			FOREIGN KEY ([PersonID]) REFERENCES [data].[People] ([PersonID])
			ON DELETE CASCADE;
		ALTER TABLE [data].[Organisations_People]
			ADD CONSTRAINT [FK_Organisations_People_RoleID]
			FOREIGN KEY ([RoleID]) REFERENCES [data].[PersonRoles] ([ID])
			ON DELETE CASCADE;
		ALTER TABLE [data].[People_People]
			ADD CONSTRAINT [FK_People_People_PersonID1]
			FOREIGN KEY ([PersonID1]) REFERENCES [data].[People] ([PersonID]);
			--ON DELETE CASCADE;
		ALTER TABLE [data].[People_People]
			ADD CONSTRAINT [FK_People_People_PersonID2]
			FOREIGN KEY ([PersonID2]) REFERENCES [data].[People] ([PersonID]);
			--ON DELETE CASCADE;
		ALTER TABLE [data].[People_People]
			ADD CONSTRAINT [FK_People_People_AssociationID]
			FOREIGN KEY ([AssociationID]) REFERENCES [data].[Associations] ([ID])
			ON DELETE CASCADE;
		ALTER TABLE [data].[Places]
			ADD CONSTRAINT [FK_Places_AddressID]
			FOREIGN KEY ([AddressID]) REFERENCES [data].[Addresses] ([ID])
			ON DELETE SET NULL;
		ALTER TABLE [data].[Places]
			ADD CONSTRAINT [FK_Places_RegionID]
			FOREIGN KEY ([RegionID]) REFERENCES [data].[Regions] ([ID])
			ON DELETE SET NULL;
		ALTER TABLE [data].[Places]
			ADD CONSTRAINT [FK_Places_CountryID]
			FOREIGN KEY ([CountryID]) REFERENCES [data].[Countries] ([ID])
			ON DELETE SET NULL;
		ALTER TABLE [data].[Places]
			ADD CONSTRAINT [FK_Places_LocationID]
			FOREIGN KEY ([LocationID]) REFERENCES [data].[Locations] ([ID])
			ON DELETE SET NULL;
		ALTER TABLE [data].[Regions]
			ADD CONSTRAINT [FK_Regions_CountryID]
			FOREIGN KEY ([CountryID]) REFERENCES [data].[Countries] ([ID])
			ON DELETE SET NULL;

	END TRY
	BEGIN CATCH
		SELECT @err = ERROR_MESSAGE(),
			@errNo = ERROR_NUMBER(),
			@errSev = ERROR_SEVERITY(),
			@errState = ERROR_STATE(),
			@errProc = ERROR_PROCEDURE(),
			@errLine = ERROR_LINE();

		SELECT @err, @errNo, @errSev, @errState, @errProc, @errLine;
	END CATCH;
RETURN 1