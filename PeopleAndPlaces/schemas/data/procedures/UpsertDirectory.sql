﻿/*
UpsertDirectory procedure
-------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-01-10

Table:					Directories
PK_Directories:			ID
FK_Directories_HashID:	HashID
UIX_Directories:		Volume, Hash

UPDATE: If @HashID is of 'directory' type, and when directory exists in UIX_Directories, to matching record
INSERT: If @HashID is of 'directory' type, and when directory does not exist in UIX_Directories
RETURNS: ID of the directory found in UIX_Directories (@Volume, @Path)
*/

CREATE PROCEDURE [data].[UpsertDirectory](
	@Drive NVARCHAR(5),
	@Path NVARCHAR(255),
	@Actual BIT = 1,
	@HashID INT
) AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	-----------
	-- LOGIC --
	-----------
	BEGIN TRY
		DECLARE @DirectoryID INT;
		DECLARE @HashTypeCheck BIT;

		SET @HashTypeCheck = 0;
		SET @DirectoryID = [data].[DirectoryID](@Drive, @Path);

		SELECT @HashTypeCheck = [Value]
		FROM [data].[CheckHashKeyType](@HashID, 'directory');

		IF @HashTypeCheck = 0
		BEGIN
			DECLARE @call VARCHAR(350);
			SELECT @call = '@HashID was not of directory KeyType in call ' + [Value]
			FROM [strings].[input_UpsertDirectory](@Drive, @Path, @Actual, @HashID, NULL);

			EXEC [data].[*commit];
			RAISERROR('%s', 15, 1, @call);
		END;

		IF @DirectoryID IS NULL
		BEGIN
			INSERT INTO [data].[Directories] (
				[Drive],
				[Path],
				[Actual],
				[HashID]
			) VALUES (
				@Drive,
				@Path,
				@Actual,
				@HashID
			);

			SET @DirectoryID = [data].[DirectoryID](@Drive, @Path);
		END;
		ELSE
		BEGIN
			UPDATE [data].[Directories] WITH (SERIALIZABLE)
			SET [Actual] = @Actual,
				[HashID] = @HashID
 			WHERE [ID] = @DirectoryID;
		END;
	
		RETURN @DirectoryID;
	END TRY
	BEGIN CATCH
		DECLARE @note VARCHAR(350);

		SELECT @note = [Value]
		FROM [strings].[input_UpsertDirectory](@Drive, @Path, @Actual, @HashID, NULL);
		
		EXEC [data].[*error_handler] 'UpsertDirectory', @note;

		RETURN -1;
	END CATCH
END;