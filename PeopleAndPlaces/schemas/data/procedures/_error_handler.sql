﻿/*
The error handler for use when tests are being run
--------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-01-08
*/

CREATE PROCEDURE [data].[*error_handler]
	@Object VARCHAR(50),
	@Note VARCHAR(350)
AS
	--SET XACT_ABORT OFF;
	DECLARE @errmsg		NVARCHAR(4000) = error_message(),
			@severity	TINYINT = error_severity(),
			@state		TINYINT = error_state(),
			@errno		INT = error_number(),
			@proc		SYSNAME = error_procedure(),
			@lineno		INT = error_line(),
			@logerrmsg	NVARCHAR(4000) = error_message(),
			@date		DATE = GETDATE(),
			@procid		VARCHAR(20) = @@PROCID,
			@trancount	INT = @@TRANCOUNT;
       
   IF @errmsg NOT LIKE '***%'
   BEGIN
      SELECT @errmsg = '*** ' + coalesce(quotename(@proc), '<dynamic SQL>') + 
                       ', Line ' + ltrim(str(@lineno)) + '. Errno ' + 
                       ltrim(str(@errno)) + ': ' + @errmsg
   END

   PRINT 'ERR: ' + @errmsg;
   -- PRINT 'LOG: ' + @logerrmsg;

   EXEC [data].[*rollback]; -- @procid, @trancount;
   EXEC [data].[*commit];

   	BEGIN TRY
		BEGIN TRY
			--print 'before insert ERR: ' + @Object + ' ' + LEFT(@Note, 350);

			INSERT INTO [_Errors] (
				[Date],
				[Object],
				[Note],
				[Error],
				[ErrorNumber],
				[ErrorSeverity],
				[ErrorState],
				[ErrorProcedure],
				[ErrorLine]
			) VALUES (
				GETDATE(),
				@Object,
				@Note,
				@logerrmsg,
				@errno,
				@severity,
				@state,
				@proc,
				@lineno
			);
			--print 'after insert ERR: ' + @Object + ' ' + LEFT(@Note, 350);

		END TRY
		BEGIN CATCH
			EXEC [data].[*rollback]; -- @procid, @trancount;
			
			UPDATE [_Errors]
			SET [Date] = GETDATE(),
				[Error] = @logerrmsg,
				[ErrorNumber] = @errno,
				[ErrorSeverity] = @severity,
				[ErrorState] = @state,
				[ErrorProcedure] = @proc,
				[ErrorLine] = @lineno
			WHERE [Object] = @Object
			AND [Note] = @Note;
			--print 'after update ERR: ' + @Object + ' ' + LEFT(@Note, 350);

		END CATCH;
	END TRY
	BEGIN CATCH
		
		DECLARE @errmsg1	NVARCHAR(4000) = error_message(),
				@errno1		INT = error_number(),
				@proc1		SYSNAME = error_procedure(),
				@lineno1	INT = error_line();
       
		IF @errmsg1 NOT LIKE '***%'
		BEGIN
			SELECT @errmsg1 = '*** ' + coalesce(quotename(@proc1), '<dynamic SQL>') + 
							', Line ' + ltrim(str(@lineno1)) + '. Errno ' + 
							ltrim(str(@errno1)) + ': ' + @errmsg1
		END

		PRINT '*** FAILED TO LOG ERROR: ' + @errmsg1;

		EXEC [data].[*rollback]; -- @procid, @trancount;
		EXEC [data].[*commit];
	END CATCH;

	EXEC [data].[*commit];
	
	--SET XACT_ABORT ON;
	RAISERROR('%s', @severity, @state, @errmsg)