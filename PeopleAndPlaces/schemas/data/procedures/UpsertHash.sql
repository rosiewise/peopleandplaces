﻿/*
UpsertHash procedure
--------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-11-14

Tables:				Hashes
PK_Hashes:			ID
UIX_Hashes:			KeyType, Hash

UPDATE: no update as yet
INSERT: only when @Role is provided without @ID
RETURNS: ID of the Hash in UIX_Hashes (@KeyType, @Hash)
*/

CREATE PROCEDURE [data].[UpsertHash](
	@KeyType NVARCHAR(9),
	@Hash NVARCHAR(32)
) AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	-----------
	-- LOGIC --
	-----------
	BEGIN TRY
		DECLARE @HashID INT;

		IF @KeyType IS NULL OR @Hash IS NULL OR
			(@KeyType != 'file' AND @KeyType != 'directory')
		BEGIN
			DECLARE @call VARCHAR(350);
			SELECT @call = ' call ' + [Value]
			FROM [strings].[input_UpsertHash](@KeyType, @Hash, NULL);

			IF @KeyType IS NULL OR @Hash IS NULL
				SET @call = 'Both @KeyType and @Hash must be provided' + @call;
			ELSE IF @KeyType != 'file' AND @KeyType != 'directory'
				SET @call = '@KeyType must be "file" or "directory"' + @call;

			EXEC [data].[*commit];
			RAISERROR('%s', 15, 1, @call);
		END;

		SET @HashID = [data].[HashID](@KeyType, @Hash);

		IF @HashID IS NULL
		BEGIN
			INSERT INTO [data].[Hashes] (
				[KeyType],
				[Hash]
			) VALUES (
				@KeyType,
				@Hash
			);

			SET @HashID = [data].[HashID](@KeyType, @Hash);
		END;

		RETURN @HashID;
	END TRY
	BEGIN CATCH
		DECLARE @note VARCHAR(100);

		SELECT @note = [Value]
		FROM [strings].[input_UpsertHash](@KeyType, @Hash, NULL);
		
		EXEC [data].[*error_handler] 'UpsertHash', @note;

		RETURN -1;
	END CATCH
END;