﻿/*
UpsertOrganisation procedure
----------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-02-29
*/

CREATE PROCEDURE [data].[UpsertOrganisation]
	@ID INT = NULL,
	@OrganisationName NVARCHAR(50) = NULL,
	@Description NVARCHAR(MAX) = NULL
AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	-----------
	-- LOGIC --
	-----------
	BEGIN TRY
		DECLARE @OrganisationID INT;

		-- IF @ID WE ARE UPDATING THE ROW
		IF (@ID IS NOT NULL AND @OrganisationName IS NOT NULL)
		BEGIN
			UPDATE [data].[Organisations]
			SET [OrganisationName] = @OrganisationName,
				[Description] = @Description
			WHERE [ID] = @ID;

			SET @OrganisationID = @ID;
		END
		ELSE
		BEGIN
			-- IDENTIFY USING NAME OR INSERT
			SET @OrganisationID = [data].[OrganisationID](@OrganisationName);

			IF (@OrganisationID IS NULL)
			BEGIN
				INSERT INTO [data].[Organisations] (
					[OrganisationName],
					[Description]
				) VALUES (
					@OrganisationName,
					@Description
				);
				SET @OrganisationID = @@IDENTITY;
			END;
		END;
		RETURN @OrganisationID;
	END TRY
	BEGIN CATCH
		DECLARE @note VARCHAR(100);

		SELECT @note = [Value]
		FROM [strings].[input_UpsertOrganisation](@ID, @OrganisationName, @Description, NULL);
		
		EXEC [data].[*error_handler] 'UpsertOrganisation', @note;

		RETURN -1;
	END CATCH
END;