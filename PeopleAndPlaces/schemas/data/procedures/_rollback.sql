﻿/*
A simplistic rollback routine
-----------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-01-08
*/

CREATE PROCEDURE [data].[*rollback]
	--@proc VARCHAR(100),
	--@trancount INT
AS
	DECLARE @xstate SMALLINT = XACT_STATE();

	/*
	PRINT 'rollback attempt: @xstate = ' + CONVERT(VARCHAR(2), @xstate) +
							', @trancount = ' + CONVERT(VARCHAR(2), @trancount) +
							', @@TRANCOUNT = ' + CONVERT(VARCHAR(2), @@TRANCOUNT);
	*/

	IF @xstate = -1 OR @xstate = 1 PRINT 'ATTEMPTING ROLLBACK';
	IF @xstate = -1 OR @xstate = 1 ROLLBACK;

	BEGIN TRANSACTION;
RETURN 0