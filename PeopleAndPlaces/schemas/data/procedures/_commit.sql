﻿/*
The routine to commit now and begin a new transaction
-----------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-01-08
*/

CREATE PROCEDURE [data].[*commit]
AS
	DECLARE @proc VARCHAR(20);
	COMMIT;
	BEGIN TRANSACTION;

	SET @proc = @@PROCID;

	SAVE TRANSACTION @proc;
	--PRINT 'Committed and saved ' + @proc;
RETURN 0