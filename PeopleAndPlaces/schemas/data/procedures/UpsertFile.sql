﻿/*
UpsertFile procedure
--------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE PROCEDURE [data].[UpsertFile](
	@DirectoryID INT,
	@FileName NVARCHAR(255),
	@Extension NVARCHAR(5),
	@MimeType NVARCHAR(50),
	@HashID INT,
	@Created DATETIME = NULL,
	@Updated DATETIME = NULL,
	@Size INT = NULL,
	@InSynch BIT = NULL
) AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	-----------
	-- LOGIC --
	-----------
	BEGIN TRY
		DECLARE @FileID INT;
		DECLARE @HashTypeCheck BIT;
		DECLARE @Now DATETIME = GETDATE();
		DECLARE @call VARCHAR(350);

		SET @HashTypeCheck = 0;
		SET @FileID = [data].[FileID](@DirectoryID, @FileName, @Extension);

		SELECT @HashTypeCheck = [Value]
		FROM [data].[CheckHashKeyType](@HashID, 'file');

		IF @HashTypeCheck = 0
		BEGIN
			SELECT @call = '@HashID was not of file KeyType in call ' + [Value]
			FROM [strings].[input_UpsertFile](
				@DirectoryID, @FileName, @Extension, @MimeType, @HashID,
				@Created, @Updated, @Size, @InSynch, NULL);

			EXEC [data].[*commit];
			RAISERROR('%s', 15, 1, @call);
		END;

		IF @FileID IS NULL
		BEGIN
			IF @Created IS NULL SET @Created = @Now;

			IF @DirectoryID > 0 AND @HashID > 0
				INSERT INTO [data].[Files] (
					[DirectoryID],
					[FileName],
					[Extension],
					[MimeType],
					[HashID],
					[Created],
					[Updated],
					[Size],
					[Imported]
				) VALUES (
					@DirectoryID,
					@FileName,
					@Extension,
					@MimeType,
					@HashID,
					@Created,
					@Updated,
					@Size,
					@Now
				);
			ELSE
			BEGIN
				SELECT @call = '@DirectoryID and @HashID need to be set if record is inserted ' + [Value]
				FROM [strings].[input_UpsertFile](
					@DirectoryID, @FileName, @Extension, @MimeType, @HashID,
					@Created, @Updated, @Size, @InSynch, NULL);

				RAISERROR('%s', 15, 1, @call);
			END;

			SET @FileID = [data].[FileID](@DirectoryID, @FileName, @Extension);
		END;
		ELSE
		BEGIN
			UPDATE [data].[Files] WITH (SERIALIZABLE)
			SET [Extension] = @Extension,
				[MimeType] = @MimeType,
				[HashID] = @HashID,
				[Size] = @Size,
				[InSynch] = @InSynch
			WHERE [ID] = @FileID;
			
			IF @Created != @Now
				UPDATE [data].[Files] WITH (SERIALIZABLE)
				SET [Created] = @Created,
					[InSynch] = @InSynch
				WHERE [ID] = @FileID;

			IF @Updated != @Now
				UPDATE [data].[Files] WITH (SERIALIZABLE)
				SET [Updated] = @Updated,
					[InSynch] = @InSynch
				WHERE [ID] = @FileID; 
		END;
	
		RETURN @FileID;
	END TRY
	BEGIN CATCH
		DECLARE @note VARCHAR(600);

		SELECT @note = [Value]
		FROM [strings].[input_UpsertFile](
			@DirectoryID, @FileName, @Extension, @MimeType, @HashID,
			@Created, @Updated, @Size, @InSynch, NULL);
		
		EXEC [data].[*error_handler] 'UpsertFile', @note;

		RETURN -1;
	END CATCH
END;