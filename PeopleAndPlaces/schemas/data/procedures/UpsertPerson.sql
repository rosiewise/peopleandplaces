﻿/*
UpsertPerson procedure
----------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-02-29
*/

CREATE PROCEDURE [data].[UpsertPerson]
	@FirstName NVARCHAR(50),
	@LastName NVARCHAR(50),
	@Gender TINYINT = 0,
	@DOB DATE = NULL,
	@OrganisationID INT = NULL,
	@OrganisationName NVARCHAR(50) = NULL,
	@OrganisationDescription NVARCHAR(MAX) = NULL,
	@OrganisationRole NVARCHAR(50) = NULL,
	@FileID INT = NULL,
	@FileRole NVARCHAR(50) = NULL
AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	-----------
	-- LOGIC --
	-----------
	BEGIN TRY

		DECLARE @PersonID INT;
		DECLARE @OrganisationRoleID INT;
		DECLARE @FileRoleID INT;

		SET @PersonID = [data].[PersonID](
			@FirstName, @LastName, @Gender, @DOB
		);

		IF @PersonID IS NULL
		BEGIN
			INSERT INTO [data].[People] (
				[FirstName],
				[LastName],
				[Gender],
				[DOB]
			) VALUES (
				@FirstName,
				@LastName,
				@Gender,
				@DOB
			);
			SET @PersonID = @@IDENTITY;
		END;

		-- Now we should have a person, we can build associations
		IF @PersonID > 0
		BEGIN
			EXEC @OrganisationID = [data].[UpsertOrganisation]
				@OrganisationID, @OrganisationName, @OrganisationDescription;

			-- Do we have the organisation role if not, insert
			IF (@OrganisationID IS NOT NULL AND @OrganisationRole IS NOT NULL)
			BEGIN
				SET @OrganisationRoleID = [data].[RoleID](@OrganisationRole);
				IF @OrganisationRoleID IS NULL
				BEGIN
					EXEC @OrganisationRoleID = [data].[UpsertRole]
						@OrganisationRoleID, @OrganisationRole;
				END;

				IF (@PersonID > 0 AND @OrganisationID > 0 AND @OrganisationRoleID > 0)
				BEGIN
					-- We have enough to fill Organisations_People
					INSERT INTO [data].[Organisations_People] (
						[OrganisationID],
						[PersonID],
						[RoleID]
					) VALUES (
						@OrganisationID,
						@PersonID,
						@OrganisationRoleID
					);
				END;
			END;

			-- Are we updating the file association for the person.
			IF (@FileID IS NOT NULL AND @FileRole IS NOT NULL)
			BEGIN
				SET @FileRoleID = [data].[RoleID](@FileRole);
				IF @FileRoleID IS NULL
				BEGIN
					EXEC @FileRoleID = [data].[UpsertRole]
						@FileRoleID, @FileRole;
				END;

				IF (@PersonID > 0 AND @FileID > 0 AND @FileRoleID > 0)
				BEGIN
					-- We have enough to fill Files_People
					INSERT INTO [data].[Files_People] (
						[FileID],
						[PersonID],
						[RoleID]
					) VALUES (
						@FileID,
						@PersonID,
						@FileRoleID
					);
				END;
			END; 
		END;

		RETURN @PersonID;
	END TRY
	BEGIN CATCH
		DECLARE @note VARCHAR(100);

		SELECT @note = [Value]
		FROM [strings].[input_UpsertPerson](
			@FirstName, @LastName, @Gender, @DOB, @OrganisationID, @OrganisationName,
			@OrganisationDescription, @OrganisationRole, @FileID, @FileRole, NULL);
		
		EXEC [data].[*error_handler] 'UpsertPerson', @note;

		RETURN -1;
	END CATCH
END;