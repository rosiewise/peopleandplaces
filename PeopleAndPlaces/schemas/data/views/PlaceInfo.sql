﻿CREATE VIEW [data].[PlaceInfo] AS
SELECT
	pl.ID AS PlaceID,
	pl.Place,
	ad.Unit,
	ad.Street,
	ad.Locality,
	lc.Location,
	r.Region,
	c.Country,
	c.Continent,
	ad.Code,
	pl.AddressID,
	pl.LocationID,
	pl.RegionID,
	pl.CountryID
FROM [data].[Places] pl
JOIN [data].[Addresses] ad ON pl.AddressID = ad.ID
JOIN [data].[Locations] lc ON pl.LocationID = lc.ID
JOIN [data].[Regions] r ON pl.RegionID = r.ID
JOIN [data].[Countries] c ON pl.CountryID = c.ID;