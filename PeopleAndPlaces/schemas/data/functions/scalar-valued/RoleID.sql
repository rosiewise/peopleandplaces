﻿/*
Function to that returns a role ID for the given parameters
-----------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE FUNCTION [data].[RoleID]
(
	@Role NVARCHAR(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;

	SELECT @ID = [ID]
	FROM [data].[PersonRoles]
	WHERE [Role] = LTRIM(RTRIM(@Role));

	RETURN @ID;
END