﻿/*
Function to that returns a directory ID for the given parameters
----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE FUNCTION [data].[DirectoryID]
(
	@Drive NVARCHAR(5),
	@Path NVARCHAR(255)
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;

	SELECT @ID = [ID]
	FROM [Directories] WITH (SERIALIZABLE)
	WHERE [Drive] = @Drive
	AND [Path] = @Path;

	RETURN @ID;
END