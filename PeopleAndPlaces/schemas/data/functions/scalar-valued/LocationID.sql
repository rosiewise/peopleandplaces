﻿/*
Function to that returns a location ID for the given parameters
---------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE FUNCTION [data].[LocationID]
(
	@Location NVARCHAR(50),
	@CountryID INT = NULL,
	@RegionID INT = NULL
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;

	SELECT @ID = [ID]
	FROM [data].[Locations]
	WHERE [Location] = LTRIM(RTRIM(@Location))
	AND ([CountryID] = @CountryID OR ([CountryID] IS NULL))
	AND ([RegionID] = @RegionID OR ([RegionID] IS NULL))
	OPTION (RECOMPILE);

	RETURN @ID;
END