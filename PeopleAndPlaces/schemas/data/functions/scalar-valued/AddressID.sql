﻿/*
Function to that returns an address ID for the given parameters
---------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE FUNCTION [data].[AddressID]
(
	@Unit NVARCHAR(20) = NULL,
	@Street NVARCHAR(50) = NULL,
	@Locality NVARCHAR(50) = NULL,
	@LocationID INT = NULL,
	@Code NVARCHAR(50) = NULL
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;

	SELECT @ID = [ID]
	FROM [data].[Addresses]
	WHERE ([Unit] = LTRIM(RTRIM(@Unit)) OR @Unit IS NULL)
	AND	([Street] = LTRIM(RTRIM(@Street)) OR @Street IS NULL)
	AND ([Locality] = LTRIM(RTRIM(@Locality)) OR @Locality IS NULL)
	AND ([LocationID] = @LocationID OR @LocationID IS NULL)
	AND ([Code] = LTRIM(RTRIM(@Code)) OR @Code IS NULL);

	RETURN @ID;
END