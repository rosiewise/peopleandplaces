﻿/*
Function to that returns a file ID for the given parameters
-----------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE FUNCTION [data].[FileID]
(
	@DirectoryID INT,
	@FileName NVARCHAR(255),
	@Extension VARCHAR(10)
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;

	SELECT @ID = [ID]
	FROM [data].[Files] WITH (SERIALIZABLE)
	WHERE [DirectoryID] = @DirectoryID
		AND [FileName] = @FileName
		AND [Extension] = @Extension;

	RETURN @ID;
END