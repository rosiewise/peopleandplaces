﻿/*
Function to that returns a hash ID for the given parameters
-----------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE FUNCTION [data].[HashID]
(
	@KeyType NVARCHAR(9),
	@Hash NVARCHAR(32)
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;

	SELECT @ID = [ID]
	FROM [Hashes] WITH (SERIALIZABLE)
	WHERE [KeyType] = @KeyType
	AND [Hash] = @Hash;

	RETURN @ID;
END