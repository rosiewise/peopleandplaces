﻿/*
Function to that returns a person ID for the given parameters
-------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE FUNCTION [data].[PersonID]
(
	@FirstName NVARCHAR(50),
	@LastName NVARCHAR(50),
	@Gender TINYINT = 0,
	@DOB DATE = NULL
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;

	SELECT @ID = [PersonID]
	FROM [data].[People]
	WHERE [FirstName] = @FirstName
	AND ([LastName] = @LastName OR [LastName] IS NULL)
	AND [Gender] = @Gender
	AND ([DOB] = @DOB OR [DOB] IS NULL);

	RETURN @ID;
END