﻿/*
Function to that returns a country ID for the given parameters
-----------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE FUNCTION [data].[CountryID]
(
	@Country NVARCHAR(40)
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;

	SELECT @ID = [ID]
	FROM [data].[Countries]
	WHERE [Country] = LTRIM(RTRIM(@Country));

	RETURN @ID;
END