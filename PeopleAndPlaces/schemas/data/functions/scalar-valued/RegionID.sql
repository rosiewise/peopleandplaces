﻿/*
Function to that returns a region ID for the given parameters
-------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE FUNCTION [data].[RegionID]
(
	@Region NVARCHAR(20),
	@CountryID INT = NULL
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;

	IF @CountryID > 0
		SELECT @ID = [ID]
		FROM [data].[Regions]
		WHERE [Region] = LTRIM(RTRIM(@Region))
		AND [CountryID] = @CountryID
		OPTION (RECOMPILE);
	ELSE
		SELECT @ID = [ID]
		FROM [data].[Regions]
		WHERE [Region] = LTRIM(RTRIM(@Region))
		AND [CountryID] IS NULL
		OPTION (RECOMPILE);

	RETURN @ID;
END