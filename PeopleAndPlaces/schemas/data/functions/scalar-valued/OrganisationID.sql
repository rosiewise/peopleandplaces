﻿/*
Function to that returns an organisation ID for the given parameters
--------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE FUNCTION [data].[OrganisationID]
(
	@Name NVARCHAR(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;

	SELECT @ID = [ID]
	FROM [data].[Organisations]
	WHERE [OrganisationName] = @Name;

	RETURN @ID;
END