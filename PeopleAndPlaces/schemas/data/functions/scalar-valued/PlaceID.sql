﻿/*
Function to that returns a place ID for the given parameters
------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-12-25
*/

CREATE FUNCTION [data].[PlaceID]
(
	@Place NVARCHAR(50) = NULL,
	@AddressID INT = NULL,
	@LocationID INT = NULL,
	@RegionID INT = NULL,
	@CountryID INT = NULL
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;

	SELECT @ID = [ID]
	FROM [data].[Places]
	WHERE [Place] = LTRIM(RTRIM(@Place))
	AND ([AddressID] = @AddressID OR @AddressID IS NULL)
	AND ([LocationID] = @LocationID OR @LocationID IS NULL)
	AND ([RegionID] = @RegionID OR @RegionID IS NULL)
	AND ([CountryID] = @CountryID OR @CountryID IS NULL);

	RETURN @ID;
END