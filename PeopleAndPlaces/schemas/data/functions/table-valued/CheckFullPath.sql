﻿/*
Function to check and split file path into it's DB stored components
--------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-05
Updated:

ASSUMPTIONS:
	- drives references contain :
	- files have extensions
*/

CREATE FUNCTION [data].[CheckFullPath](
	@FullPath NVARCHAR(MAX)
)
RETURNS @returntable TABLE
(
    [Drive]			NVARCHAR(5),
	[Path]			NVARCHAR(MAX),
	[PathHash]		VARCHAR(34),
	[FullFileName]	NVARCHAR(255),
	[FileName]		NVARCHAR(255),
	[Extension]		VARCHAR(10),
	[MimeType]		VARCHAR(50),
	[IsSupported]	BIT,
	[Note]			VARCHAR(50)
)
AS
BEGIN

	DECLARE @drive VARCHAR(50);
	DECLARE @path VARCHAR(200);
	DECLARE @hash VARCHAR(34);
	DECLARE @file VARCHAR(50);
	DECLARE @filename VARCHAR(50);
	DECLARE @extension VARCHAR(5);
	DECLARE @mimetype VARCHAR(50);
	DECLARE @issupported BIT;
	DECLARE @note VARCHAR(50);

	SELECT
		@drive = [Drive],
		@path = [Path],
		@hash = CONVERT(VARCHAR(34), HASHBYTES('MD5', [Path]), 1),
		@file =	[FullFileName],
		@filename = [FileName],
		@extension = [Extension]
	FROM [strings].[split_full_path](@FullPath);

	SELECT
		@mimetype = [MimeType],
		@issupported = [IsSupported],
		@note = [Note]
	FROM [data].[CheckExtensionSupport](@extension);

	INSERT @returntable
	SELECT @drive, @path, @hash, @file, @filename,
		@extension, @mimetype, @issupported, @note;

	RETURN;
END