﻿/*
Function to check mime type exists and to bring back support summary
--------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-12-25
Updated:
*/

CREATE FUNCTION [data].[CheckExtensionSupport]
(
	@Extension VARCHAR(10)
)
RETURNS @returntable TABLE
(
	[Extension] VARCHAR(10),
	[MimeType] VARCHAR(50),
	[Description] VARCHAR(255),
	[IsRecognised] BIT,
	[IsSupported] BIT,
	[IsDiscoverable] BIT,
	[Note] VARCHAR(50)
)
AS
BEGIN
	INSERT @returntable
	SELECT
		CASE
			WHEN [Extension] IS NULL THEN @Extension
			ELSE [Extension]
		END AS Extension,
		[MimeType],
		[Description],
		CASE
			WHEN [Extension] IS NULL THEN 0
			ELSE 1
		END AS IsRecognised,
		CASE
			WHEN [Extension] IS NULL THEN 0
			ELSE [Supported]
		END AS IsSupported,
		CASE
			WHEN [Extension] IS NULL THEN 0
			ELSE [Discover]
		END AS IsDiscoverable,
		CASE
			WHEN [MimeType] IS NULL THEN 'Extension ''' + @Extension + ''' is not recognised'
			WHEN [Supported] = 0 THEN 'Extension ''' + @Extension + ''' is not supported'
			WHEN [Discover] = 0 THEN 'Extension ''' + @Extension + ''' is not discoverable'
			ELSE NULL
		END AS Note
	FROM (SELECT 1 AS dummy) AS tmp
	LEFT JOIN [data].[MimeTypes]
		ON [MimeTypes].[Extension] = @Extension
		AND [tmp].[dummy] = 1;
	RETURN
END