﻿/*
Function to check directory type of Hash is as expected
-------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-29
Updated:
*/

CREATE FUNCTION [data].[CheckHashKeyType]
(
	@ID INT,
	@KeyType NVARCHAR(9)
)
RETURNS @returntable TABLE
(
	Value BIT
)
AS
BEGIN
	INSERT @returntable
	SELECT
		(
			CASE WHEN [KeyType] = @KeyType THEN 1
			ELSE 0 END
		)
	FROM [data].[Hashes]
	WHERE [ID] = @ID;
	RETURN
END