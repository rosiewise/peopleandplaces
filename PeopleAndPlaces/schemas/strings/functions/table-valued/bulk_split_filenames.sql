﻿/*
Function to split up a file path into it's DB stored components
---------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-21
Updated:

ASSUMPTIONS:
	- files have extensions
	- no other . are in the file name
*/

CREATE FUNCTION [strings].[bulk_split_filenames](
	@Strings [strings].[ListNVarChars] READONLY
)
RETURNS @returntable TABLE
(
	[FilePathID]	INT,
	[FileName]		VARCHAR(255),
	[Extension]		VARCHAR(5)
)
AS
BEGIN
	DECLARE @OrderedTokens [strings].[SplitStringOrdering];
	DECLARE @TokenCount INT;

	INSERT INTO @OrderedTokens
	SELECT * FROM [strings].[bulk_split](@Strings, '.');

	SELECT @TokenCount = COUNT(*) FROM @OrderedTokens;

	WITH tokens (PathID, RowNumber, Token, ReverseOrder)
	AS
	(
		SELECT TOP (@TokenCount) *
		FROM @OrderedTokens
		ORDER BY [ID], [SortOrder]
	)
	INSERT @returntable
	SELECT
		[FilePathID],
		dbo.GROUP_CONCAT_D([FileName], '.'),
		MAX([Extension])
	FROM (
		SELECT
			[t].[PathID] AS [FilePathID],
			CASE
				WHEN [t].[RowNumber] = [t1].[RowNumber] AND LEN([t].[Token]) < 6 THEN NULL
				ELSE [t].[Token]
			END AS [FileName],
			CASE
				WHEN [t].[RowNumber] = [t1].[RowNumber] AND LEN([t].[Token]) < 6 THEN [t].[Token]
				ELSE NULL
			END AS [Extension]
		FROM [tokens] AS [t]
		LEFT JOIN [tokens] AS [t1]
			ON [t].[PathID] = [t1].[PathID]
			AND [t1].[ReverseOrder] = 1
	) AS [summary]
	JOIN @Strings AS [FileNames] ON [FileNames].[ID] = [summary].[FilePathID]
	GROUP BY [FilePathID];

	RETURN;
END