﻿/*
Function to stringify a call to FileID, for logging purposes
------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:	2015-12-25
*/

CREATE FUNCTION [strings].[input_FileID](
	@DirectoryID INT,
	@FileName NVARCHAR(255),
	@Extension VARCHAR(10),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (280) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @DirectoryID IS NULL
			THEN 'NULL'
			ELSE CONVERT(VARCHAR(12), @DirectoryID)
			END) + ', '
		+ (CASE WHEN @FileName IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(VARCHAR(255), @FileName) + ''''
			END) + ', '
		+ (CASE WHEN @Extension IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(VARCHAR(10), @Extension) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END