﻿/*
Function to split up a file path into it's DB stored components
---------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-05
Updated:

ASSUMPTIONS:
	- drives references contain :
	- directories are seperated by \
*/

CREATE FUNCTION [strings].[split_path](
	@mypath NVARCHAR(MAX)
)
RETURNS @returntable TABLE
(
    [Drive]			VARCHAR(5),
	[Path]			VARCHAR(MAX),
	[FullFileName]	VARCHAR(255)
)
AS
BEGIN

	DECLARE @drive VARCHAR(50);
	DECLARE @path VARCHAR(200);
	DECLARE @file VARCHAR(50);

	-- PARSE THE PATH
	WITH tokens (RowNumber, Item)
	AS
	(
		SELECT
			ROW_NUMBER() OVER (ORDER BY (SELECT 1)),
			[Item]
		FROM [strings].[split](@mypath, '\')
	)
	SELECT
		@drive = MAX([Drive]),
		@path = dbo.GROUP_CONCAT_D([Path], '\'),
		@file = MAX([FileName])
	FROM (
		SELECT
			1 AS [GroupNumber],
			CASE
				WHEN [RowNumber] = 1 AND LEN([Item]) < 6 AND CHARINDEX(':', [Item]) > 0 THEN [Item]
				ELSE NULL
			END AS [Drive],
			CASE
				WHEN [RowNumber] = 1 AND LEN([Item]) < 6 AND CHARINDEX(':', [Item]) > 0 THEN NULL
				WHEN [RowNumber] = (SELECT MAX([RowNumber]) FROM [tokens]) AND CHARINDEX('.', [Item]) > 0 THEN NULL
				ELSE Item
			END AS [Path],
			CASE
				WHEN [RowNumber] = (SELECT MAX([RowNumber]) FROM [tokens]) AND CHARINDEX('.', [Item]) > 0 THEN [Item]
				ELSE NULL
			END AS [FileName]
		FROM [tokens]
	) AS [summary];

	INSERT @returntable
	SELECT @drive, @path, @file;
	RETURN;
END