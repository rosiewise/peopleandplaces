﻿/*
Function to stringify a call to UpsertHash, for logging purposes
----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-09
Updated:	2015-10-31
*/

CREATE FUNCTION [strings].[input_UpsertHash](
	@KeyType NVARCHAR(9),
	@Hash NVARCHAR(32),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (56) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @KeyType IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(VARCHAR(9), @KeyType) + ''''
			END) + ', ' +
		+ (CASE WHEN @Hash IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(VARCHAR(32), @Hash) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END