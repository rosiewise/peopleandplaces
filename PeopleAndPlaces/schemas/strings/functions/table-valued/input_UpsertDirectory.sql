﻿/*
Function to stringify a call to UpsertDirectory, for logging purposes
----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-28
Updated:	2015-11-22
*/

CREATE FUNCTION [strings].[input_UpsertDirectory](
	@Drive NVARCHAR(5),
	@Path NVARCHAR(255),
	@Actual BIT,
	@HashID INT,
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (296) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @Drive IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(5), @Drive) + ''''
			END) + ', '
		+ (CASE WHEN @Path IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(255), @Path) + ''''
			END) + ', '
		+ (CASE
			WHEN @Actual IS NULL THEN 'NULL'
			WHEN @Actual = 1 THEN 'true'
			ELSE 'false'
			END) + ', '
		+ (CASE WHEN @HashID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @HashID)
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END