﻿/*
Function to stringify a call to PersonID, for logging purposes
--------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:
*/

CREATE FUNCTION [strings].[input_PersonID](
	@FirstName NVARCHAR(50),
	@LastName NVARCHAR(50),
	@Gender TINYINT = 0,
	@DOB DATE = NULL,
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (134) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @FirstName IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @FirstName) + ''''
			END) + ', '
		+ (CASE WHEN @LastName IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @LastName) + ''''
			END) + ', '
		+ (CASE WHEN @Gender IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(3), @Gender)
			END) + ', '
		+ (CASE WHEN @DOB IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(10), @DOB) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END