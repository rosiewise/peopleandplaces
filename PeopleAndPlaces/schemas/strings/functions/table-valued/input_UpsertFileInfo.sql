﻿/*
Function to stringify a call to UpsertFileInfo, for logging purposes
--------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-06
Updated:	2015-12-25
*/

CREATE FUNCTION [strings].[input_UpsertFileInfo](
	@FullPath NVARCHAR(MAX),
	@FileHash VARCHAR(34),
	@Created DATETIME,
	@Updated DATETIME,
	@Size INT,
	@InSynch BIT,
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR(1000) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @FullPath IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(VARCHAR(500), @FullPath) + ''''
			END) + ', '
		+ (CASE WHEN @FileHash IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(VARCHAR(34), @FileHash) + ''''
			END) + ', '
		+ (CASE WHEN @Created IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(VARCHAR(32), @Created) + ''''
			END) + ', '
		+ (CASE WHEN @Updated IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(VARCHAR(32), @Updated) + ''''
			END) + ', '
		+ (CASE WHEN @Size IS NULL
			THEN 'NULL'
			ELSE CONVERT(VARCHAR(12), @Size)
			END) + ', '
		+ (CASE
			WHEN @InSynch IS NULL THEN 'NULL'
			WHEN @InSynch = 1 THEN 'true'
			ELSE 'false'
			END) 
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END;