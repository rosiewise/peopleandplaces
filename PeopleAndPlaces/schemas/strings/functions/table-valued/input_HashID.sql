﻿/*
Function to stringify a call to HashID, for logging purposes
------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:
*/

CREATE FUNCTION [strings].[input_HashID](
	@KeyType NVARCHAR(9),
	@Hash NVARCHAR(32),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (52) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @KeyType IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(9), @KeyType) + ''''
			END) + ', '
		+ (CASE WHEN @Hash IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(32), @Hash) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END