﻿/*
Function that returns a table of tokens, marked by string ID and order and reverse order rankings
-------------------------------------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-21
Updated:
*/
CREATE FUNCTION [strings].[bulk_split]
(
	@Strings [strings].[ListNVarChars] READONLY,
	@Delimiter NVARCHAR(255)
)
RETURNS @returntable TABLE
(
	[ID] INT,
	[SortOrder] INT,
	[Token] NVARCHAR(MAX),
	[ReverseOrder] INT
)
AS
BEGIN
	DECLARE @Tokens [strings].[SplitStringGroups];

	DECLARE @minID INT;
	SELECT @minID = MIN([ID]) FROM @Strings;

	WHILE @minID IS NOT NULL
	BEGIN
		DECLARE @ID INT;
		DECLARE @String VARCHAR(MAX);

		SELECT @ID = [ID], @String = [String] FROM @Strings WHERE [ID] = @minID;

		INSERT INTO @Tokens
		SELECT
			@ID,
			ROW_NUMBER() OVER (ORDER BY (SELECT 1)),
			[Item]
		FROM [strings].[split](@String, @Delimiter);

		SELECT @minID = MIN([ID]) FROM @Strings WHERE @minID < [ID];
	END;

	INSERT @returntable
	SELECT [ID], [SortOrder], [Token], [ReverseOrder]
	FROM (
		SELECT
			[ID],
			[SortOrder],
			[Token],
			ROW_NUMBER() OVER (PARTITION BY [ID] ORDER BY [SortOrder] DESC) AS [ReverseOrder]
		FROM @Tokens
	) AS [Tokens]
	ORDER BY [ID], [SortOrder];

	RETURN;
END