﻿/*
Function to stringify a call to UpsertVenue, for logging purposes
-----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-06
Updated:	
*/

CREATE FUNCTION [strings].[input_UpsertVenue](
	@Place NVARCHAR(50),
	@Unit NVARCHAR(20),
	@Street NVARCHAR(50),
	@Locality NVARCHAR(50),
	@Location NVARCHAR(50),
	@Region NVARCHAR(50),
	@Country NVARCHAR(40),
	@Code NVARCHAR(20),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR(369) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @Place IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Place) + ''''
			END) + ', '
		+ (CASE WHEN @Unit IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(20), @Unit) + ''''
			END) + ', '
		+ (CASE WHEN @Street IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Street) + ''''
			END) + ', '
		+ (CASE WHEN @Locality IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Locality) + ''''
			END) + ', '
		+ (CASE WHEN @Location IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Location) + ''''
			END) + ', '
		+ (CASE WHEN @Region IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Region) + ''''
			END) + ', '
		+ (CASE WHEN @Country IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(40), @Country) + ''''
			END) + ', '
		+ (CASE WHEN @Code IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(20), @Code) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END