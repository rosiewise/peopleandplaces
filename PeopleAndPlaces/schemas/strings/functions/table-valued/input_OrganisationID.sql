﻿/*
Function to stringify a call to OrganisationID, for logging purposes
--------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:
*/

CREATE FUNCTION [strings].[input_OrganisationID](
	@Name NVARCHAR(50),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (61) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @Name IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Name) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END