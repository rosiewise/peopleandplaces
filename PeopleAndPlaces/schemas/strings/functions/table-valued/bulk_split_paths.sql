﻿/*
Function that splits a table of full paths into their component parts
---------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-21
Updated:	2016-02-29
*/
CREATE FUNCTION [strings].[bulk_split_paths]
(
	@FullPaths [strings].[ListFilePaths] READONLY
)
RETURNS @returntable TABLE
(
	[FilePathID]	INT,
	[FilePath]		NVARCHAR(MAX),
	[Drive]			NVARCHAR(4000),
	[Path]			NVARCHAR(MAX),
	[File]			NVARCHAR(4000)
)
AS
BEGIN
	DECLARE @OrderedTokens [strings].[SplitStringOrdering];
	DECLARE @Strings [strings].[ListNVarChars];
	DECLARE @TokenCount INT;
	DECLARE @DriveDelimiter NVARCHAR(1) = ':';
	DECLARE @FolderDelimiter NVARCHAR(1) = '\';
	DECLARE @ExtensionDelimiter NVARCHAR(1) = '.';

	INSERT INTO @Strings ([ID], [String])
	SELECT [ID], [FilePath]
	FROM @FullPaths;

	INSERT INTO @OrderedTokens
	SELECT * FROM [strings].[bulk_split](@Strings, @FolderDelimiter);

	SELECT @TokenCount = COUNT(*) FROM @OrderedTokens;

	WITH tokens (PathID, RowNumber, Token, ReverseOrder)
	AS
	(
		SELECT TOP (@TokenCount) *
		FROM @OrderedTokens
		ORDER BY [ID], [SortOrder]
	)
	INSERT @returntable
	SELECT
		[FilePathID],
		[FilePath],
		MAX([Drive]) AS [Drive],
		dbo.GROUP_CONCAT_D([Path], @FolderDelimiter) AS [Path],
		MAX([FileName]) AS [FileName]
	FROM (
		SELECT
			[t].[PathID] AS [FilePathID],
			CASE
				WHEN [t].[RowNumber] = 1 AND LEN([t].[Token]) < 6 AND CHARINDEX(@DriveDelimiter, [t].[Token]) > 0 THEN [t].[Token]
				ELSE NULL
			END AS [Drive],
			CASE
				WHEN [t].[RowNumber] = 1
					AND LEN([t].[Token]) < 6
					AND CHARINDEX(@DriveDelimiter, [t].[Token]) > 0 THEN NULL
				WHEN [t].[RowNumber] = 2
					AND CHARINDEX(@ExtensionDelimiter, [t].[Token]) > 0 THEN @FolderDelimiter
				WHEN [t].[RowNumber] = [t1].[RowNumber]
					AND CHARINDEX(@ExtensionDelimiter, [t].[Token]) > 0 THEN NULL
				ELSE [t].[Token]
			END AS [Path],
			CASE
				WHEN [t].[RowNumber] = [t1].[RowNumber]
					AND CHARINDEX(@ExtensionDelimiter, [t].[Token]) > 0 THEN [t].[Token]
				ELSE NULL
			END AS [FileName]
		FROM [tokens] AS [t]
		LEFT JOIN [tokens] AS [t1]
			ON [t].[PathID] = [t1].[PathID]
			AND [t1].[ReverseOrder] = 1
	) AS [summary]
	JOIN @FullPaths AS [FullPaths] ON [FullPaths].[ID] = [summary].[FilePathID]
	GROUP BY [FilePathID], [FilePath];

	RETURN
END