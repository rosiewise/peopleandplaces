﻿/*
Function to stringify a call to DirectoryID, for logging purposes
-----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:
*/

CREATE FUNCTION [strings].[input_DirectoryID](
	@Drive NVARCHAR(5),
	@Path NVARCHAR(255),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (275) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @Drive IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(5), @Drive) + ''''
			END) + ', '
		+ (CASE WHEN @Path IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(255), @Path) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END