﻿/*
Function to stringify a call to UpsertFile, for logging purposes
----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-10
Updated:	2015-12-25
*/

CREATE FUNCTION [strings].[input_UpsertFile](
	@DirectoryID INT,
	@FileName NVARCHAR(255),
	@Extension NVARCHAR(5),
	@MimeType NVARCHAR(50),
	@HashID INT,
	@Created DATETIME,
	@Updated DATETIME,
	@Size INT,
	@InSynch BIT,
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR(600) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @DirectoryID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @DirectoryID)
			END) + ', '
		+ (CASE WHEN @FileName IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(255), @FileName) + ''''
			END) + ', '
		+ (CASE WHEN @Extension IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(5), @Extension) + ''''
			END) + ', '
		+ (CASE WHEN @MimeType IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @MimeType) + ''''
			END) + ', '
		+ (CASE WHEN @HashID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @HashID)
			END) + ', '
		+ (CASE WHEN @Created IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(32), @Created) + ''''
			END) + ', '
		+ (CASE WHEN @Updated IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(32), @Updated) + ''''
			END) + ', '
		+ (CASE WHEN @Size IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @Size)
			END) + ', '
		+ (CASE
			WHEN @InSynch IS NULL THEN 'NULL'
			WHEN @InSynch = 1 THEN 'true'
			ELSE 'false'
			END) 
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END