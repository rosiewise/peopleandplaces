﻿/*
Function to stringify a call to RoleID, for logging purposes
------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:
*/

CREATE FUNCTION [strings].[input_RoleID](
	@Role NVARCHAR(50),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (61) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @Role IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Role) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END