﻿/*
Function to stringify a call to RegionID, for logging purposes
--------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:
*/

CREATE FUNCTION [strings].[input_RegionID](
	@Region NVARCHAR(20),
	@CountryID INT = NULL,
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (45) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @Region IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(20), @Region) + ''''
			END) + ', '
		+ (CASE WHEN @CountryID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @CountryID)
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END