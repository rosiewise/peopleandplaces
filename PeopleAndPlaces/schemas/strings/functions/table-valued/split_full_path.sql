﻿/*
Function to split up a file path into it's DB stored components
---------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-12-25
Updated:	2016-02-05

ASSUMPTIONS:
	- drives references contain :
	- files have extensions
*/

CREATE FUNCTION [strings].[split_full_path](
	@mypath NVARCHAR(MAX)
)
RETURNS @returntable TABLE
(
    [Drive]			VARCHAR(5),
	[Path]			VARCHAR(MAX),
	[FullFileName]	VARCHAR(255),
	[FileName]		VARCHAR(255),
	[Extension]		VARCHAR(5)
)
AS
BEGIN

	DECLARE @drive VARCHAR(50);
	DECLARE @path VARCHAR(200);
	DECLARE @file VARCHAR(50);
	DECLARE @filename VARCHAR(50);
	DECLARE @extension VARCHAR(5);

	SELECT
		@drive = [Drive],
		@path = COALESCE([Path],'\'),
		@file =	[FullFileName]
	FROM [strings].[split_path](@mypath);

	SELECT
		@filename = [FileName],
		@extension = [Extension]
	FROM [strings].[split_filename](@file);

	INSERT @returntable
	SELECT @drive, @path, @file, @filename, @extension;

	RETURN;
END