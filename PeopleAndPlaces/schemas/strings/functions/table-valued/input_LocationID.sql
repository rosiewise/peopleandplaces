﻿/*
Function to stringify a call to LocationID, for logging purposes
----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:
*/

CREATE FUNCTION [strings].[input_LocationID](
	@Location NVARCHAR(50),
	@CountryID INT = NULL,
	@RegionID INT = NULL,
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (89) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @Location IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Location) + ''''
			END) + ', '
		+ (CASE WHEN @CountryID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @CountryID)
			END) + ', '
		+ (CASE WHEN @RegionID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @RegionID)
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END