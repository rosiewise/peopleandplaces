﻿/*
Function to stringify a call to UpsertPlace, for logging purposes
-----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-06
Updated:	
*/

CREATE FUNCTION [strings].[input_UpsertPlace](
	@ID INT,
	@Place NVARCHAR(50),
	@AddressID INT,
	@LocationID INT,
	@RegionID INT,
	@CountryID INT,
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR(131) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @ID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @ID)
			END) + ', '
		+ (CASE WHEN @Place IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Place) + ''''
			END) + ', '
		+ (CASE WHEN @AddressID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @AddressID)
			END) + ', '
		+ (CASE WHEN @LocationID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @LocationID)
			END) + ', '
		+ (CASE WHEN @RegionID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @RegionID)
			END) + ', '
		+ (CASE WHEN @CountryID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @CountryID)
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END