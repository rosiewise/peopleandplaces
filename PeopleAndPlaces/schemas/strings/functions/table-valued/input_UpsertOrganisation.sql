﻿/*
Function to stringify a call to UpsertOrganisation, for logging purposes
------------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-06
Updated:	
*/

CREATE FUNCTION [strings].[input_UpsertOrganisation](
	@ID INT,
	@OrganisationName NVARCHAR(50),
	@Description NVARCHAR(MAX),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR(MAX) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @ID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @ID)
			END) + ', '
		+ (CASE WHEN @OrganisationName IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @OrganisationName) + ''''
			END) + ', '
		+ (CASE WHEN @Description IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(MAX), @Description) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END