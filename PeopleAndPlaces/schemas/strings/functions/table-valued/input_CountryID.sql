﻿/*
Function to stringify a call to CoutryID, for logging purposes
--------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:
*/

CREATE FUNCTION [strings].[input_CountryID](
	@Country NVARCHAR(40),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (51) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @Country IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(40), @Country) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END