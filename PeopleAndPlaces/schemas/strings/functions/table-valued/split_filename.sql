﻿/*
Function to split up a file path into it's DB stored components
---------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-05
Updated:

ASSUMPTIONS:
	- files have extensions
*/

CREATE FUNCTION [strings].[split_filename](
	@myfile NVARCHAR(MAX)
)
RETURNS @returntable TABLE
(
	[FileName]		VARCHAR(255),
	[Extension]		VARCHAR(5)
)
AS
BEGIN

	DECLARE @filename VARCHAR(50);
	DECLARE @extension VARCHAR(5);

	-- PARSE THE FILE NAME
	WITH tokens (RowNumber, Item)
	AS
	(
		SELECT
			ROW_NUMBER() OVER (ORDER BY (SELECT 1)),
			[Item]
		FROM [strings].[split](@myfile, '.')
	)
	SELECT
		@filename = dbo.GROUP_CONCAT_D([FileName], '.'),
		@extension = MAX([Extension])
	FROM (
		SELECT
			1 AS [GroupNumber],
			CASE
				WHEN [RowNumber] = (SELECT MAX([RowNumber]) FROM [tokens]) AND LEN([Item]) < 6 THEN NULL
				ELSE Item
			END AS [FileName],
			CASE
				WHEN [RowNumber] = (SELECT MAX([RowNumber]) FROM [tokens]) AND LEN([Item]) < 6 THEN [Item]
				ELSE NULL
			END AS [Extension]
		FROM [tokens]
	) AS [summary];

	INSERT @returntable
	SELECT @filename, @extension;

	RETURN;
END