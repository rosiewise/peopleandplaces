﻿/*
Function to stringify a call to CheckHashKeyType, for logging purposes
----------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-28
Updated:	2015-10-31
*/

CREATE FUNCTION [strings].[input_CheckHashKeyType](
	@ID INT,
	@KeyType NVARCHAR(255),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (280) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @ID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @ID)
			END) + ', '
		+ (CASE WHEN @KeyType IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(255), @KeyType) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END