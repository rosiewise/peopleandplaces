﻿/*
Function to stringify a call to PlaceID, for logging purposes
-------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:
*/

CREATE FUNCTION [strings].[input_PlaceID](
	@Place NVARCHAR(50) = NULL,
	@AddressID INT = NULL,
	@LocationID INT = NULL,
	@RegionID INT = NULL,
	@CountryID INT = NULL,
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (119) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @Place IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Place) + ''''
			END) + ', '
		+ (CASE WHEN @AddressID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @AddressID)
			END) + ', '
		+ (CASE WHEN @LocationID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @LocationID)
			END) + ', '
		+ (CASE WHEN @RegionID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @RegionID)
			END) + ', '
		+ (CASE WHEN @CountryID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @CountryID)
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END