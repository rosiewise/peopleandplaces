﻿CREATE FUNCTION [strings].[split]
(@List NVARCHAR (MAX), @Delimiter NVARCHAR (255))
RETURNS 
     TABLE (
        [Item] NVARCHAR (4000) NULL)
AS
 EXTERNAL NAME [CLRUtilities].[UserDefinedFunctions].[SplitString_Multi]