﻿/*
Function that splits a table of full paths into it's consituent parts
---------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-21
Updated:
*/
CREATE FUNCTION [strings].[bulk_split_full_paths]
(
	@FullPaths [strings].[ListFilePaths] READONLY
)
RETURNS @returntable TABLE
(
	[FilePathID] INT,
	[FilePath] NVARCHAR(MAX),
	[Drive] NVARCHAR(4000),
	[Path] NVARCHAR(MAX),
	[File] NVARCHAR(4000),
	[FileName] NVARCHAR(4000),
	[Extension] NVARCHAR(5)
)
AS
BEGIN
	DECLARE @Paths [strings].[ListNVarChars];
	DECLARE @Files [strings].[ListNVarChars];
	DECLARE @SplitPaths TABLE
	(
		[FilePathID]	INT,
		[FilePath]		NVARCHAR(MAX),
		[Drive]			NVARCHAR(4000),
		[Path]			NVARCHAR(MAX),
		[File]			NVARCHAR(4000)
	);

	INSERT INTO @Paths SELECT [ID], [FilePath] FROM @FullPaths;
	INSERT INTO @SplitPaths SELECT * FROM [strings].[bulk_split_paths](@FullPaths);
	INSERT INTO @Files SELECT [FilePathID], [File] FROM @SplitPaths;

	INSERT @returntable
	SELECT [FilePaths].*, [FileName], [Extension] FROM @SplitPaths AS [FilePaths]
	JOIN [strings].[bulk_split_filenames](@Files) AS [Files] ON [FilePaths].[FilePathID] = [Files].[FilePathID];

	RETURN
END