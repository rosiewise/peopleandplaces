﻿/*
Function to stringify a call to AddressID, for logging purposes
---------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:
*/

CREATE FUNCTION [strings].[input_AddressID](
	@Unit NVARCHAR(20),
	@Street NVARCHAR(50),
	@Locality NVARCHAR(50),
	@LocationID INT,
	@Code NVARCHAR(50),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (205) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @Unit IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(20), @Unit) + ''''
			END) + ', '
		+ (CASE WHEN @Street IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Street) + ''''
			END) + ', '
		+ (CASE WHEN @Locality IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Locality) + ''''
			END) + ', '
		+ (CASE WHEN @LocationID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @LocationID)
			END) + ', '
		+ (CASE WHEN @Code IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Code) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END