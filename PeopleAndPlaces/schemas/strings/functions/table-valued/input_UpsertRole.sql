﻿/*
Function to stringify a call to UpsertRole, for logging purposes
----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-06
Updated:	
*/

CREATE FUNCTION [strings].[input_UpsertRole](
	@ID INT,
	@Role NVARCHAR(50),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (75) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @ID IS NULL
			THEN 'NULL'
			ELSE CONVERT(VARCHAR(12), @ID)
			END) + ', ' +
		+ (CASE WHEN @Role IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(VARCHAR(54), @Role) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END