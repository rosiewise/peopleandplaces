﻿/*
Function to stringify a call to UpsertLocation, for logging purposes
--------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-06
Updated:	
*/

CREATE FUNCTION [strings].[input_UpsertLocation](
	@ID INT,
	@Location NVARCHAR(50),
	@CountryID INT,
	@RegionID INT,
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR(103) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @ID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @ID)
			END) + ', '
		+ (CASE WHEN @Location IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @Location) + ''''
			END) + ', '
		+ (CASE WHEN @CountryID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @CountryID)
			END) + ', '
		+ (CASE WHEN @RegionID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @RegionID)
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END