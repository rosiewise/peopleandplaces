﻿/*
Function to stringify a call to Since, for logging purposes
-----------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:
*/

CREATE FUNCTION [strings].[input_Since](
	@started DATETIME,
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR (30) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @started IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(20), @started) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END