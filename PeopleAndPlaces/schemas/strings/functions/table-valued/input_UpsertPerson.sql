﻿/*
Function to stringify a call to UpsertPerson, for logging purposes
------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-06
Updated:	
*/

CREATE FUNCTION [strings].[input_UpsertPerson](
	@FirstName NVARCHAR(50),
	@LastName NVARCHAR(50),
	@Gender TINYINT = 0,
	@DOB DATE,
	@OrganisationID INT,
	@OrganisationName NVARCHAR(50),
	@OrganisationDescription NVARCHAR(MAX),
	@OrganisationRole NVARCHAR(50),
	@FileID INT,
	@FileRole NVARCHAR(50),
	@pass BIT
)
RETURNS @returntable TABLE
(
    [Value]          VARCHAR(MAX) NOT NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT '(' +
		+ (CASE WHEN @FirstName IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @FirstName) + ''''
			END) + ', '
		+ (CASE WHEN @LastName IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @LastName) + ''''
			END) + ', '
		+ (CASE WHEN @Gender IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(3), @Gender)
			END) + ', '
		+ (CASE WHEN @DOB IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @DOB) + ''''
			END) + ', '
		+ (CASE WHEN @OrganisationID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @OrganisationID)
			END) + ', '
		+ (CASE WHEN @OrganisationName IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @OrganisationName) + ''''
			END) + ', '
		+ (CASE WHEN @OrganisationDescription IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(MAX), @OrganisationDescription) + ''''
			END) + ', '
		+ (CASE WHEN @OrganisationRole IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @OrganisationRole) + ''''
			END) + ', '
		+ (CASE WHEN @FileID IS NULL
			THEN 'NULL'
			ELSE CONVERT(NVARCHAR(12), @FileID)
			END) + ', '
		+ (CASE WHEN @FileRole IS NULL
			THEN 'NULL'
			ELSE '''' + CONVERT(NVARCHAR(50), @FileRole) + ''''
			END)
		+ ')'
		+ (CASE WHEN @pass = 1 THEN ' passes'
			WHEN @pass = 0 THEN ' fails'
			ELSE ''
			END)
	RETURN;
END