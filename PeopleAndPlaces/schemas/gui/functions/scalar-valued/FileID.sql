﻿/*
Function to that returns a file ID given a file path
----------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-02-16

ASSUMPTIONS:
	- drives references contain :
	- files have extensions
*/
CREATE FUNCTION [gui].[FileID]
(
	@FullPath NVARCHAR(MAX)
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;
	DECLARE @DirectoryID INT;
	DECLARE @Drive NVARCHAR(5);
	DECLARE @Path NVARCHAR(255);
	DECLARE @FileName NVARCHAR(255);
	DECLARE @Extension VARCHAR(10);

	SELECT
		@Drive = [Drive],
		@Path = [Path],
		@FileName = [FileName],
		@Extension = [Extension]
	FROM [strings].[split_full_path](@FullPath);

	SELECT @DirectoryID = [ID]
	FROM [data].[Directories] WITH (SERIALIZABLE)
	WHERE [Drive] = @Drive
	AND [Path] = @Path;

	SELECT @ID = [ID]
	FROM [data].[Files] WITH (SERIALIZABLE)
	WHERE [DirectoryID] = @DirectoryID
		AND [FileName] = @FileName
		AND [Extension] = @Extension;

	RETURN @ID;
END