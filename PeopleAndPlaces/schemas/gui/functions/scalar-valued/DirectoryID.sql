﻿/*
Function to get a DirectoryID given a file path
--------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-16
Updated:

ASSUMPTIONS:
	- drives references contain :
	- files have extensions
*/
CREATE FUNCTION [gui].[DirectoryID]
(
	@FullPath NVARCHAR(MAX)
)
RETURNS INT
AS
BEGIN
	DECLARE @ID INT;
	DECLARE @Drive NVARCHAR(5);
	DECLARE @Path NVARCHAR(255);

	SELECT
		@Drive = [Drive],
		@Path = [Path]
	FROM [strings].[split_full_path](@FullPath);

	SELECT @ID = [ID]
	FROM [data].[Directories] WITH (SERIALIZABLE)
	WHERE [Drive] = @Drive
	AND [Path] = @Path;

	RETURN @ID;
END