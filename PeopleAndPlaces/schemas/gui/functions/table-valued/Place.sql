﻿/*
Function to get details of places matching one or more of matching criteria
---------------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-17
Updated:	2016-02-23
*/

CREATE FUNCTION [gui].[Place]
(
	@ID INT = NULL,
	@Place NVARCHAR(50) = NULL,
	@Unit NVARCHAR(20) = NULL,
	@Street NVARCHAR(50) = NULL,
	@Locality NVARCHAR(50) = NULL,
	@Region NVARCHAR(20) = NULL,
	@Country NVARCHAR(40) = NULL,
	@Continent NVARCHAR(13) = NULL,
	@Code NVARCHAR(50) = NULL,
	@AddressID INT = NULL,
	@LocationID INT = NULL,
	@RegionID INT = NULL,
	@CountryID INT = NULL
)
RETURNS @returntable TABLE
(
	[PlaceID] INT,
	[Place] NVARCHAR(50),
	[Unit] NVARCHAR(20),
	[Street] NVARCHAR(50),
	[Locality] NVARCHAR(50),
	[Location] NVARCHAR(50),
	[Region] NVARCHAR(50),
	[Country] NVARCHAR(40),
	[Continent] NVARCHAR(13),
	[Code] NVARCHAR(50),
	[AddressID] INT,
	[LocationID] INT,
	[RegionID] INT,
	[CountryID] INT
)
AS
BEGIN
	INSERT @returntable
	SELECT
		[Place].[ID],
		[Place].[Place],
		[Address].[Unit],
		[Address].[Street],
		[Address].[Locality],
		[Location].[Location],
		[Region].[Region],
		[Country].[Country],
		[Country].[Continent],
		[Address].[Code],
		[Place].[AddressID],
		[Place].[LocationID],
		[Place].[RegionID],
		[Place].[CountryID]
	FROM [data].[Places] AS [Place]
	LEFT JOIN [data].[Addresses] AS [Address]
		ON [Place].[AddressID] = [Address].[ID]
	LEFT JOIN [data].[Locations] AS [Location]
		ON [Place].[LocationID] = [Location].[ID]
	LEFT JOIN [data].[Regions] AS [Region]
		ON [Place].[RegionID] = [Region].[ID]
	LEFT JOIN [data].[Countries] AS [Country]
		ON [Place].[CountryID] = [Country].[ID]
	WHERE ([Place].[ID] = @ID OR @ID IS NULL)
	AND ([Place].[Place] = LTRIM(RTRIM(@Place)) OR @Place IS NULL)
	AND ([Address].[Unit] = LTRIM(RTRIM(@Unit)) OR @Unit IS NULL)
	AND	([Address].[Street] = LTRIM(RTRIM(@Street)) OR @Street IS NULL)
	AND ([Address].[Locality] = LTRIM(RTRIM(@Locality)) OR @Locality IS NULL)
	AND ([Region].[Region] = LTRIM(RTRIM(@Region)) OR @Region IS NULL)
	AND ([Country].[Country] = LTRIM(RTRIM(@Country)) OR @Country IS NULL)
	AND ([Country].[Continent] = LTRIM(RTRIM(@Continent)) OR @Continent IS NULL)
	AND ([Address].[Code] = LTRIM(RTRIM(@Code)) OR @Code IS NULL)
	AND ([Place].[AddressID] = @AddressID OR @AddressID IS NULL)
	AND ([Place].[LocationID] = @LocationID OR @LocationID IS NULL)
	AND ([Place].[RegionID] = @RegionID OR @RegionID IS NULL)
	AND ([Place].[CountryID] = @CountryID OR @CountryID IS NULL);

	RETURN
END