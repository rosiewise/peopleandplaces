﻿/*
Function get derived values for the image defined in the path
-------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-16
Updated:

ASSUMPTIONS:
	- drives references contain :
	- files have extensions
*/
CREATE FUNCTION [gui].[Image]
(
	@FilePath NVARCHAR(MAX)
)
RETURNS @returntable TABLE
(
	[DirID] INT,
	[Drive] NVARCHAR(5),
	[Path] NVARCHAR(MAX),
	[Folder] NVARCHAR(200), -- DERIVED
	[DirActual] BIT,
	[DirHashID] INT,
	[DirHashKeyType] VARCHAR(9),
	[DirHash] VARCHAR(34),
	[FileID] INT,
	[FileDirID] INT,
	[FileName] NVARCHAR(255),
	[Extension] NVARCHAR(5),
	[MimeType] NVARCHAR(50),
	[Actual] BIT,
	[Size] INT,
	[Created] DATETIME,
	[CreatedYear] INT, -- DERIVED
	[CreatedQuarter] INT, -- DERIVED
	[CreatedMonth] INT, -- DERIVED
	[CreatedDay] INT, -- DERIVED
	[CreatedISOWeek] INT, -- DERIVED
	[CreatedWeekday] VARCHAR(10), -- DERIVED
	[CreatedDate] DATE, -- DERIVED
	[CreatedHour] INT, -- DERIVED
	[Updated] DATETIME,
	[UpdatedYear] INT, -- DERIVED
	[UpdatedQuarter] INT, -- DERIVED
	[UpdatedMonth] INT, -- DERIVED
	[UpdatedDay] INT, -- DERIVED
	[UpdatedISOWeek] INT, -- DERIVED
	[UpdatedWeekday] VARCHAR(10), -- DERIVED
	[UpdatedDate] DATE, -- DERIVED
	[UpdatedHour] INT, -- DERIVED
	[InSynch] BIT,
	[Synchronised] DATETIME,
	[SynchronisedYear] INT, -- DERIVED
	[SynchronisedQuarter] INT, -- DERIVED
	[SynchronisedMonth] INT, -- DERIVED
	[SynchronisedDay] INT, -- DERIVED
	[SynchronisedWeek] INT, -- DERIVED
	[SynchronisedWeekday] VARCHAR(10), -- DERIVED
	[SynchronisedDate] DATE, -- DERIVED
	[SynchronisedHour] INT, -- DERIVED
	[RowUpdated] DATETIME,
	[RowUpdatedYear] INT, -- DERIVED
	[RowUpdatedQuarter] INT, -- DERIVED
	[RowUpdatedMonth] INT, -- DERIVED
	[RowUpdatedDay] INT, -- DERIVED
	[RowUpdatedISOWeek] INT, -- DERIVED
	[RowUpdatedWeekday] VARCHAR(10), -- DERIVED
	[RowUpdatedDate] DATE, -- DERIVED
	[RowUpdatedHour] INT, -- DERIVED
	[Imported] DATETIME,
	[ImportedYear] INT, -- DERIVED
	[ImportedQuarter] INT, -- DERIVED
	[ImportedMonth] INT, -- DERIVED
	[ImportedDay] INT, -- DERIVED
	[ImportedWeek] INT, -- DERIVED
	[ImportedWeekday] VARCHAR(10), -- DERIVED
	[ImportedDate] DATE, -- DERIVED
	[ImportedHour] INT, -- DERIVED
	[FileHashID] INT,
	[FileHashKeyType] VARCHAR(9),
	[FileHash] VARCHAR(34),
	[Title] NVARCHAR(100),
	[Description] NVARCHAR(MAX),
	[PlaceID] INT,
	[Place] NVARCHAR(50),
	[Unit] NVARCHAR(20),
	[Street] NVARCHAR(50),
	[Locality] NVARCHAR(50),
	[Location] NVARCHAR(50),
	[Region] NVARCHAR(50),
	[Country] NVARCHAR(40),
	[Continent] NVARCHAR(13),
	[Code] NVARCHAR(50),
	[AddressID] INT,
	[LocationID] INT,
	[RegionID] INT,
	[CountryID] INT
)
AS
BEGIN
	DECLARE @FileID INT;
	DECLARE @DirectoryID INT;
	DECLARE @Drive NVARCHAR(5);
	DECLARE @Path NVARCHAR(255);
	DECLARE @FileName NVARCHAR(255);
	DECLARE @Extension VARCHAR(10);

	SELECT
		@Drive = [Drive],
		@Path = [Path],
		@FileName = [FileName],
		@Extension = [Extension]
	FROM [strings].[split_full_path](@FilePath);

	SELECT @DirectoryID = [ID]
	FROM [data].[Directories] WITH (SERIALIZABLE)
	WHERE [Drive] = @Drive
	AND [Path] = @Path;

	SELECT @FileID = [ID]
	FROM [data].[Files] WITH (SERIALIZABLE)
	WHERE [DirectoryID] = @DirectoryID
		AND [FileName] = @FileName
		AND [Extension] = @Extension;

	INSERT @returntable
	SELECT
		d.ID AS DirID,
		d.Drive,
		d.Path,
		--CASE
		--	WHEN LEFT(d.Path, CHARINDEX('\', d.Path)) THEN LEFT(d.Path, CHARINDEX('\', d.Path))
		--	ELSE d.Path
		-- RIGHT(d.Path, CHARINDEX('\', REVERSE(d.Path)) - 1) AS Folder,
		CASE
			WHEN CHARINDEX('\', REVERSE(d.Path)) > 0
			THEN RIGHT(d.Path, CHARINDEX('\', REVERSE(d.Path)) - 1)
			ELSE d.Path
		END AS Folder,
		d.Actual AS DirActual,
		dh.ID AS DirHashID,
		dh.KeyType AS DirHashKeyType,
		dh.Hash AS DirHash,
		f.ID AS FileID,
		f.DirectoryID AS FileDirID,
		f.FileName,
		f.Extension,
		f.MimeType,
		f.Actual,
		f.Size,
		f.Created,
		YEAR(f.Created) AS CreatedYear,
		DATEPART(Q,f.Created) AS CreatedQuarter,
		MONTH(f.Created) AS CreatedMonth,
		DAY(f.Created) AS CreatedDay,
		DATEPART(ISO_WEEK,f.Created) AS CreatedISOWeek,
		DATENAME(DW,f.Created) AS CreatedWeekday,
		CONVERT(DATE,f.Created) AS CreatedDate,
		DATEPART(HOUR,f.Created) AS CreatedHour,
		f.Updated,
		YEAR(f.Updated) AS UpdatedYear,
		DATEPART(Q,f.Updated) AS UpdatedQuarter,
		MONTH(f.Updated) AS UpdatedMonth,
		DAY(f.Updated) AS UpdatedDay,
		DATEPART(ISO_WEEK,f.Updated) AS UpdatedISOWeek,
		DATENAME(DW,f.Updated) AS UpdatedWeekday,
		CONVERT(DATE,f.Updated) AS UpdatedDate,
		DATEPART(HOUR,f.Updated) AS UpdatedHour,
		f.InSynch,
		f.Synchronised,
		YEAR(f.Synchronised) AS SynchronisedYear,
		DATEPART(Q,f.Synchronised) AS SynchronisedQuarter,
		MONTH(f.Synchronised) AS SynchronisedMonth,
		DAY(f.Synchronised) AS SynchronisedDay,
		DATEPART(ISO_WEEK,f.Synchronised) AS SynchronisedWeek,
		DATENAME(DW,f.Synchronised) AS SynchronisedWeekday,
		CONVERT(DATE,f.Synchronised) AS SynchronisedDate,
		DATEPART(HOUR,f.Synchronised) AS SynchronisedHour,
		f.RowUpdated,
		YEAR(f.RowUpdated) AS RowUpdatedYear,
		DATEPART(Q,f.RowUpdated) AS RowUpdatedQuarter,
		MONTH(f.RowUpdated) AS RowUpdatedMonth,
		DAY(f.RowUpdated) AS RowUpdatedDay,
		DATEPART(ISO_WEEK,f.RowUpdated) AS RowUpdatedISOWeek,
		DATENAME(DW,f.RowUpdated) AS RowUpdatedWeekday,
		CONVERT(DATE,f.RowUpdated) AS RowUpdatedDate,
		DATEPART(HOUR,f.RowUpdated) AS RowUpdatedHour,
		f.Imported,
		YEAR(f.Imported) AS ImportedYear,
		DATEPART(Q,f.Imported) AS ImportedQuarter,
		MONTH(f.Imported) AS ImportedMonth,
		DAY(f.Imported) AS ImportedDay,
		DATEPART(ISO_WEEK,f.Imported) AS ImportedWeek,
		DATENAME(DW,f.Imported) AS ImportedWeekday,
		CONVERT(DATE,f.Imported) AS ImportedDate,
		DATEPART(HOUR,f.Imported) AS ImportedHour,
		fh.ID AS FileHashID,
		fh.KeyType AS FileHashKeyType,
		fh.Hash AS FileHash,
		i.Title,
		i.Description,
		i.PlaceID,
		p.Place,
		ad.Unit,
		ad.Street,
		ad.Locality,
		lc.Location,
		r.Region,
		c.Country,
		c.Continent,
		ad.Code,
		p.AddressID,
		p.LocationID,
		p.RegionID,
		p.CountryID
	FROM [data].[Directories] d
	LEFT JOIN [data].[Hashes] dh ON dh.ID = d.HashID AND dh.KeyType = 'directory'
	LEFT JOIN [data].[Files] f ON f.DirectoryID = d.ID
	LEFT JOIN [data].[Hashes] fh ON fh.ID = f.HashID AND fh.KeyType = 'file'
	LEFT JOIN [data].[Images] i ON f.ID = i.FileID
	LEFT JOIN [data].[Places] p ON i.PlaceID = p.ID
	LEFT JOIN [data].[Addresses] ad ON p.AddressID = ad.ID
	LEFT JOIN [data].[Locations] lc ON p.LocationID = lc.ID
	LEFT JOIN [data].[Regions] r ON p.RegionID = r.ID
	LEFT JOIN [data].[Countries] c ON p.CountryID = c.ID
	WHERE f.ID = @FileID;
	RETURN
END