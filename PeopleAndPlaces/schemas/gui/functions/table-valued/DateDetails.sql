﻿CREATE FUNCTION [gui].[DateDetails]
(
	@Dates [gui].[ListDateTimes] READONLY
)
RETURNS @returntable TABLE
(
	[ID] INT,
	[DateTime] DATETIME,
	[Year] INT, -- DERIVED
	[Quarter] INT, -- DERIVED
	[Month] INT, -- DERIVED
	[Day] INT, -- DERIVED
	[ISOWeek] INT, -- DERIVED
	[Weekday] VARCHAR(10), -- DERIVED
	[Date] DATE, -- DERIVED
	[Hour] INT -- DERIVED
)
AS
BEGIN
	INSERT @returntable
	SELECT
		[Dates].[ID],
		[Dates].[DateTime],
		YEAR([Dates].[DateTime]) AS [Year],
		DATEPART(Q,[Dates].[DateTime]) AS [Quarter],
		MONTH([Dates].[DateTime]) AS [Month],
		DAY([Dates].[DateTime]) AS [Day],
		DATEPART(ISO_WEEK,[Dates].[DateTime]) AS [ISOWeek],
		DATENAME(DW,[Dates].[DateTime]) AS [Weekday],
		CONVERT(DATE,[Dates].[DateTime]) AS [Date],
		DATEPART(HOUR,[Dates].[DateTime]) AS [Hour]
	FROM @Dates AS [Dates];
	RETURN
END