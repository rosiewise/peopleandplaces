﻿/*
Function to look for files matching the ID, the list, or matching the search critera
--------------------------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-17
Updated:	2016-03-02

ASSUMPTIONS:
	- drives references contain :
	- files have extensions
*/
CREATE FUNCTION [gui].[FilesForGrouping]
(
	@ID INT = NULL,
	@Drive NVARCHAR(5) = NULL,
	@Path NVARCHAR(MAX) = NULL,
	@FileName NVARCHAR(255) = NULL,
	@Extension NVARCHAR(5) = NULL,
	@DirHash VARCHAR(34) = NULL,
	@FileHash VARCHAR(34) = NULL,
	@FilePaths [strings].[ListFilePaths] READONLY
)
RETURNS @returntable TABLE
(
	[FilePathID] INT,
	[FilePath] NVARCHAR(MAX),
	[File] NVARCHAR(260),
	[DirID] INT,
	[Drive] NVARCHAR(5),
	[Path] NVARCHAR(MAX),
	[Folder] NVARCHAR(200), -- DERIVED
	[DirActual] BIT,
	[DirHashID] INT,
	[DirHashKeyType] VARCHAR(9),
	[DirHash] VARCHAR(34),
	[FileID] INT,
	[FileDirID] INT,
	[FileName] NVARCHAR(255),
	[Extension] NVARCHAR(5),
	[MimeType] NVARCHAR(50),
	[Actual] BIT,
	[Size] INT,
	[Created] DATETIME,
	[CreatedYear] INT, -- DERIVED
	[CreatedQuarter] INT, -- DERIVED
	[CreatedMonth] INT, -- DERIVED
	[CreatedDay] INT, -- DERIVED
	[CreatedISOWeek] INT, -- DERIVED
	[CreatedWeekday] VARCHAR(10), -- DERIVED
	[CreatedDate] DATE, -- DERIVED
	[CreatedHour] INT, -- DERIVED
	[Updated] DATETIME,
	[UpdatedYear] INT, -- DERIVED
	[UpdatedQuarter] INT, -- DERIVED
	[UpdatedMonth] INT, -- DERIVED
	[UpdatedDay] INT, -- DERIVED
	[UpdatedISOWeek] INT, -- DERIVED
	[UpdatedWeekday] VARCHAR(10), -- DERIVED
	[UpdatedDate] DATE, -- DERIVED
	[UpdatedHour] INT, -- DERIVED
	[InSynch] BIT,
	[Synchronised] DATETIME,
	[SynchronisedYear] INT, -- DERIVED
	[SynchronisedQuarter] INT, -- DERIVED
	[SynchronisedMonth] INT, -- DERIVED
	[SynchronisedDay] INT, -- DERIVED
	[SynchronisedISOWeek] INT, -- DERIVED
	[SynchronisedWeekday] VARCHAR(10), -- DERIVED
	[SynchronisedDate] DATE, -- DERIVED
	[SynchronisedHour] INT, -- DERIVED
	[RowUpdated] DATETIME,
	[RowUpdatedYear] INT, -- DERIVED
	[RowUpdatedQuarter] INT, -- DERIVED
	[RowUpdatedMonth] INT, -- DERIVED
	[RowUpdatedDay] INT, -- DERIVED
	[RowUpdatedISOWeek] INT, -- DERIVED
	[RowUpdatedWeekday] VARCHAR(10), -- DERIVED
	[RowUpdatedDate] DATE, -- DERIVED
	[RowUpdatedHour] INT, -- DERIVED
	[Imported] DATETIME,
	[ImportedYear] INT, -- DERIVED
	[ImportedQuarter] INT, -- DERIVED
	[ImportedMonth] INT, -- DERIVED
	[ImportedDay] INT, -- DERIVED
	[ImportedISOWeek] INT, -- DERIVED
	[ImportedWeekday] VARCHAR(10), -- DERIVED
	[ImportedDate] DATE, -- DERIVED
	[ImportedHour] INT, -- DERIVED
	[FileHashID] INT,
	[FileHashKeyType] VARCHAR(9),
	[FileHash] VARCHAR(34)
)
AS
BEGIN
	DECLARE @CreatedDateList [gui].[ListDateTimes];
	DECLARE @UpdatedDateList [gui].[ListDateTimes];
	DECLARE @SynchronisedDateList [gui].[ListDateTimes];
	DECLARE @RowUpdatedDateList [gui].[ListDateTimes];
	DECLARE @ImportedDateList [gui].[ListDateTimes];
	DECLARE @FileList [gui].[ListFiles];

	INSERT INTO @FileList
	SELECT * FROM [gui].[Files](
		DEFAULT,
		DEFAULT,
		DEFAULT,
		DEFAULT,
		DEFAULT,
		DEFAULT,
		DEFAULT,
		@FilePaths
	);

	INSERT INTO @CreatedDateList SELECT [FileID], [Created] FROM @FileList;
	INSERT INTO @UpdatedDateList SELECT [FileID], [Updated] FROM @FileList;
	INSERT INTO @SynchronisedDateList SELECT [FileID], [Synchronised] FROM @FileList;
	INSERT INTO @RowUpdatedDateList SELECT [FileID], [RowUpdated] FROM @FileList;
	INSERT INTO @ImportedDateList SELECT [FileID], [Imported] FROM @FileList;

	INSERT @returntable
	SELECT
		[Files].[FilePathID],
		[Files].[FilePath],
		[Files].[File],
		[Files].[DirID],
		[Files].[Drive],
		[Files].[Path],
		[Files].[Folder],
		[Files].[DirActual],
		[Files].[DirHashID],
		[Files].[DirHashKeyType],
		[Files].[DirHash],
		[Files].[FileID],
		[Files].[FileDirID],
		[Files].[FileName],
		[Files].[Extension],
		[Files].[MimeType],
		[Files].[Actual],
		[Files].[Size],
		[Files].[Created],
		[CreatedDates].[Year] AS [CreatedYear],
		[CreatedDates].[Quarter] AS [CreatedQuarter],
		[CreatedDates].[Month] AS [CreatedMonth],
		[CreatedDates].[Day] AS [CreatedDay],
		[CreatedDates].[ISOWeek] AS [CreatedISOWeek],
		[CreatedDates].[Weekday] AS [CreatedWeekday],
		[CreatedDates].[Date] AS [CreatedDate],
		[CreatedDates].[Hour] AS [CreatedHour],
		[Files].[Updated],
		[UpdatedDates].[Year] AS [UpdatedYear],
		[UpdatedDates].[Quarter] AS [UpdatedQuarter],
		[UpdatedDates].[Month] AS [UpdatedMonth],
		[UpdatedDates].[Day] AS [UpdatedDay],
		[UpdatedDates].[ISOWeek] AS [UpdatedISOWeek],
		[UpdatedDates].[Weekday] AS [UpdatedWeekday],
		[UpdatedDates].[Date] AS [UpdatedDate],
		[UpdatedDates].[Hour] AS [UpdatedHour],
		[Files].[InSynch],
		[Files].[Synchronised],
		[SynchronisedDates].[Year] AS [SynchronisedYear],
		[SynchronisedDates].[Quarter] AS [SynchronisedQuarter],
		[SynchronisedDates].[Month] AS [SynchronisedMonth],
		[SynchronisedDates].[Day] AS [SynchronisedDay],
		[SynchronisedDates].[ISOWeek] AS [SynchronisedISOWeek],
		[SynchronisedDates].[Weekday] AS [SynchronisedWeekday],
		[SynchronisedDates].[Date] AS [SynchronisedDate],
		[SynchronisedDates].[Hour] AS [SynchronisedHour],
		[Files].[RowUpdated],
		[RowUpdatedDates].[Year] AS [RowUpdatedYear],
		[RowUpdatedDates].[Quarter] AS [RowUpdatedQuarter],
		[RowUpdatedDates].[Month] AS [RowUpdatedMonth],
		[RowUpdatedDates].[Day] AS [RowUpdatedDay],
		[RowUpdatedDates].[ISOWeek] AS [RowUpdatedISOWeek],
		[RowUpdatedDates].[Weekday] AS [RowUpdatedWeekday],
		[RowUpdatedDates].[Date] AS [RowUpdatedDate],
		[RowUpdatedDates].[Hour] AS [RowUpdatedHour],
		[Files].[Imported],
		[ImportedDates].[Year] AS [ImportedYear],
		[ImportedDates].[Quarter] AS [ImportedQuarter],
		[ImportedDates].[Month] AS [ImportedMonth],
		[ImportedDates].[Day] AS [ImportedDay],
		[ImportedDates].[ISOWeek] AS [ImportedISOWeek],
		[ImportedDates].[Weekday] AS [ImportedWeekday],
		[ImportedDates].[Date] AS [ImportedDate],
		[ImportedDates].[Hour] AS [ImportedHour],
		[Files].[FileHashID],
		[Files].[FileHashKeyType],
		[Files].[FileHash]

	FROM @FileList AS [Files]
	LEFT JOIN [gui].[DateDetails](@CreatedDateList) AS [CreatedDates]
		ON [Files].[FileID] = [CreatedDates].[ID]
	LEFT JOIN [gui].[DateDetails](@UpdatedDateList) AS [UpdatedDates]
		ON [Files].[FileID] = [UpdatedDates].[ID]
	LEFT JOIN [gui].[DateDetails](@SynchronisedDateList) AS [SynchronisedDates]
		ON [Files].[FileID] = [SynchronisedDates].[ID]
	LEFT JOIN [gui].[DateDetails](@RowUpdatedDateList) AS [RowUpdatedDates]
		ON [Files].[FileID] = [RowUpdatedDates].[ID]
	LEFT JOIN [gui].[DateDetails](@ImportedDateList) AS [ImportedDates]
		ON [Files].[FileID] = [ImportedDates].[ID];
	RETURN
END