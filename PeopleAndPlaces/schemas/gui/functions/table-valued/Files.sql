﻿/*
Function to look for a files matching the ID, the list, or matching the search critera
--------------------------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-17
Updated:	2016-02-23

ASSUMPTIONS:
	- drives references contain :
	- files have extensions
*/
CREATE FUNCTION [gui].[Files]
(
	@ID INT = NULL,
	@Drive NVARCHAR(5) = NULL,
	@Path NVARCHAR(MAX) = NULL,
	@FileName NVARCHAR(255) = NULL,
	@Extension NVARCHAR(5) = NULL,
	@DirHash VARCHAR(34) = NULL,
	@FileHash VARCHAR(34) = NULL,
	@FilePaths [strings].[ListFilePaths] READONLY
)
RETURNS @returntable TABLE
(
	[FilePathID] INT,
	[FilePath] NVARCHAR(MAX),
	[File] NVARCHAR(260),
	[DirID] INT,
	[Drive] NVARCHAR(5),
	[Path] NVARCHAR(MAX),
	[Folder] NVARCHAR(200), -- DERIVED
	[DirActual] BIT,
	[DirHashID] INT,
	[DirHashKeyType] VARCHAR(9),
	[DirHash] VARCHAR(34),
	[FileID] INT,
	[FileDirID] INT,
	[FileName] NVARCHAR(255),
	[Extension] NVARCHAR(5),
	[MimeType] NVARCHAR(50),
	[Actual] BIT,
	[Size] INT,
	[Created] DATETIME,
	[Updated] DATETIME,
	[InSynch] BIT,
	[Synchronised] DATETIME,
	[RowUpdated] DATETIME,
	[Imported] DATETIME,
	[FileHashID] INT,
	[FileHashKeyType] VARCHAR(9),
	[FileHash] VARCHAR(34)
)
AS
BEGIN
	INSERT @returntable
	SELECT
		[Paths].[FilePathID],
		[Paths].[FilePath],
		[Paths].[File],
		[Directories].[ID] AS [DirID],
		[Directories].[Drive],
		[Directories].[Path],
		--CASE
		--	WHEN LEFT([Directory].[Path], CHARINDEX('\', [Directory].[Path])) THEN LEFT([Directory].[Path], CHARINDEX('\', [Directory].[Path]))
		--	ELSE [Directory].[Path]
		-- RIGHT([Directory].[Path], CHARINDEX('\', REVERSE([Directory].[Path])) - 1) AS [Folder],
		CASE
			WHEN CHARINDEX('\', REVERSE([Directories].[Path])) > 0
			THEN RIGHT([Directories].[Path], CHARINDEX('\', REVERSE([Directories].[Path])) - 1)
			ELSE [Directories].[Path]
		END AS [Folder],
		[Directories].[Actual] AS [DirActual],
		[DirHash].[ID] AS [DirHashID],
		[DirHash].KeyType AS [DirHashKeyType],
		[DirHash].Hash AS [DirHash],
		[Files].[ID] AS [FileID],
		[Files].[DirectoryID] AS [FileDirID],
		[Files].[FileName],
		[Files].[Extension],
		[Files].[MimeType],
		[Files].[Actual],
		[Files].[Size],
		[Files].[Created],
		[Files].[Updated],
		[Files].[InSynch],
		[Files].[Synchronised],
		[Files].[RowUpdated],
		[Files].[Imported],
		[FileHash].[ID] AS [FileHashID],
		[FileHash].KeyType AS [FileHashKeyType],
		[FileHash].[Hash] AS [FileHash]
	FROM [data].[Directories]
	JOIN [data].[Hashes] AS [DirHash]
		ON [DirHash].[ID] = [Directories].[HashID]
		AND [DirHash].[KeyType] = 'directory'
	JOIN [data].[Files]
		ON [Directories].[ID] = [Files].[DirectoryID]
	JOIN [data].[Hashes] AS [FileHash]
		ON [FileHash].[ID] = [Files].[HashID]
		AND [FileHash].[KeyType] = 'file'
	LEFT JOIN [strings].[bulk_split_full_paths](@FilePaths) AS [Paths]
		ON [Directories].[Drive] = [Paths].[Drive]
		AND [Directories].[Path] = [Paths].[Path]
		AND [Files].[FileName] = [Paths].[FileName]
		AND [Files].[Extension] = [Paths].[Extension]
	WHERE [Paths].[FilePathID] > 0
	OR [Files].[ID] = @ID
	OR	(
		[Paths].[FilePathID] IS NULL
		AND	(
			[Directories].[Drive] = @Drive
			OR [Directories].[Path] = @Path
			OR [DirHash].[Hash] = @DirHash
			OR [Files].[FileName] = @FileName
			OR [Files].[Extension] = @Extension
			OR [FileHash].[Hash] = @FileHash
		)	)
	OR	(
		[Paths].[FilePath] IS NULL
		AND @ID IS NULL
		AND @Drive IS NULL
		AND @Path IS NULL
		AND @DirHash IS NULL
		AND @FileName IS NULL
		AND @Extension IS NULL
		AND @FileHash IS NULL
	);

	RETURN
END