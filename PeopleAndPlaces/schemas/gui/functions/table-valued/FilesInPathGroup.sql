﻿/*
Function to find files specific to a drive, path, or folder, or combination of those
--------------------------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-29
Updated:	2016-03-02

ASSUMPTIONS:
	- drives references contain :
	- files have extensions
*/
CREATE FUNCTION [gui].[FilesInPathGroup]
(
	@Drive NVARCHAR(5) = NULL,
	@Path NVARCHAR(MAX) = NULL,
	@Folder NVARCHAR(255) = NULL
)
RETURNS @returntable TABLE
(
	[FilePathID] INT,
	[FilePath] NVARCHAR(MAX),
	[File] NVARCHAR(260),
	[DirID] INT,
	[Drive] NVARCHAR(5),
	[Path] NVARCHAR(MAX),
	[Folder] NVARCHAR(200), -- DERIVED
	[DirActual] BIT,
	[DirHashID] INT,
	[DirHashKeyType] VARCHAR(9),
	[DirHash] VARCHAR(34),
	[FileID] INT,
	[FileDirID] INT,
	[FileName] NVARCHAR(255),
	[Extension] NVARCHAR(5),
	[MimeType] NVARCHAR(50),
	[Actual] BIT,
	[Size] INT,
	[Created] DATETIME,
	[CreatedYear] INT, -- DERIVED
	[CreatedQuarter] INT, -- DERIVED
	[CreatedMonth] INT, -- DERIVED
	[CreatedDay] INT, -- DERIVED
	[CreatedISOWeek] INT, -- DERIVED
	[CreatedWeekday] VARCHAR(10), -- DERIVED
	[CreatedDate] DATE, -- DERIVED
	[CreatedHour] INT, -- DERIVED
	[Updated] DATETIME,
	[UpdatedYear] INT, -- DERIVED
	[UpdatedQuarter] INT, -- DERIVED
	[UpdatedMonth] INT, -- DERIVED
	[UpdatedDay] INT, -- DERIVED
	[UpdatedISOWeek] INT, -- DERIVED
	[UpdatedWeekday] VARCHAR(10), -- DERIVED
	[UpdatedDate] DATE, -- DERIVED
	[UpdatedHour] INT, -- DERIVED
	[InSynch] BIT,
	[Synchronised] DATETIME,
	[SynchronisedYear] INT, -- DERIVED
	[SynchronisedQuarter] INT, -- DERIVED
	[SynchronisedMonth] INT, -- DERIVED
	[SynchronisedDay] INT, -- DERIVED
	[SynchronisedISOWeek] INT, -- DERIVED
	[SynchronisedWeekday] VARCHAR(10), -- DERIVED
	[SynchronisedDate] DATE, -- DERIVED
	[SynchronisedHour] INT, -- DERIVED
	[RowUpdated] DATETIME,
	[RowUpdatedYear] INT, -- DERIVED
	[RowUpdatedQuarter] INT, -- DERIVED
	[RowUpdatedMonth] INT, -- DERIVED
	[RowUpdatedDay] INT, -- DERIVED
	[RowUpdatedISOWeek] INT, -- DERIVED
	[RowUpdatedWeekday] VARCHAR(10), -- DERIVED
	[RowUpdatedDate] DATE, -- DERIVED
	[RowUpdatedHour] INT, -- DERIVED
	[Imported] DATETIME,
	[ImportedYear] INT, -- DERIVED
	[ImportedQuarter] INT, -- DERIVED
	[ImportedMonth] INT, -- DERIVED
	[ImportedDay] INT, -- DERIVED
	[ImportedISOWeek] INT, -- DERIVED
	[ImportedWeekday] VARCHAR(10), -- DERIVED
	[ImportedDate] DATE, -- DERIVED
	[ImportedHour] INT, -- DERIVED
	[FileHashID] INT,
	[FileHashKeyType] VARCHAR(9),
	[FileHash] VARCHAR(34)
)
AS
BEGIN
	DECLARE @FilePaths [strings].[ListFilePaths];
	
	INSERT @returntable
	SELECT * FROM [gui].[FilesForGrouping](
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, @FilePaths) AS [Files]
	WHERE ([Files].[Drive] = @Drive OR @Drive IS NULL)
		AND ([Files].[Path] = @Path OR @Path IS NULL)
		AND ([Files].[Folder] = @Folder OR @Folder IS NULL);

	RETURN
END