﻿/*
Function get derived values for the image defined in the path
-------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-02-17
Updated:	2016-02-23

ASSUMPTIONS:
	- drives references contain :
	- files have extensions
*/
CREATE FUNCTION [gui].[ImageDetail]
(
	@FilePath NVARCHAR(MAX)
)
RETURNS @returntable TABLE
(
	[FilePathID] INT,
	[FilePath] NVARCHAR(MAX),
	[File] NVARCHAR(260),
	[DirID] INT,
	[Drive] NVARCHAR(5),
	[Path] NVARCHAR(MAX),
	[Folder] NVARCHAR(200), -- DERIVED
	[DirActual] BIT,
	[DirHashID] INT,
	[DirHashKeyType] VARCHAR(9),
	[DirHash] VARCHAR(34),
	[FileID] INT,
	[FileDirID] INT,
	[FileName] NVARCHAR(255),
	[Extension] NVARCHAR(5),
	[MimeType] NVARCHAR(50),
	[Actual] BIT,
	[Size] INT,
	[Created] DATETIME,
	[CreatedYear] INT, -- DERIVED
	[CreatedQuarter] INT, -- DERIVED
	[CreatedMonth] INT, -- DERIVED
	[CreatedDay] INT, -- DERIVED
	[CreatedISOWeek] INT, -- DERIVED
	[CreatedWeekday] VARCHAR(10), -- DERIVED
	[CreatedDate] DATE, -- DERIVED
	[CreatedHour] INT, -- DERIVED
	[Updated] DATETIME,
	[UpdatedYear] INT, -- DERIVED
	[UpdatedQuarter] INT, -- DERIVED
	[UpdatedMonth] INT, -- DERIVED
	[UpdatedDay] INT, -- DERIVED
	[UpdatedISOWeek] INT, -- DERIVED
	[UpdatedWeekday] VARCHAR(10), -- DERIVED
	[UpdatedDate] DATE, -- DERIVED
	[UpdatedHour] INT, -- DERIVED
	[InSynch] BIT,
	[Synchronised] DATETIME,
	[SynchronisedYear] INT, -- DERIVED
	[SynchronisedQuarter] INT, -- DERIVED
	[SynchronisedMonth] INT, -- DERIVED
	[SynchronisedDay] INT, -- DERIVED
	[SynchronisedISOWeek] INT, -- DERIVED
	[SynchronisedWeekday] VARCHAR(10), -- DERIVED
	[SynchronisedDate] DATE, -- DERIVED
	[SynchronisedHour] INT, -- DERIVED
	[RowUpdated] DATETIME,
	[RowUpdatedYear] INT, -- DERIVED
	[RowUpdatedQuarter] INT, -- DERIVED
	[RowUpdatedMonth] INT, -- DERIVED
	[RowUpdatedDay] INT, -- DERIVED
	[RowUpdatedISOWeek] INT, -- DERIVED
	[RowUpdatedWeekday] VARCHAR(10), -- DERIVED
	[RowUpdatedDate] DATE, -- DERIVED
	[RowUpdatedHour] INT, -- DERIVED
	[Imported] DATETIME,
	[ImportedYear] INT, -- DERIVED
	[ImportedQuarter] INT, -- DERIVED
	[ImportedMonth] INT, -- DERIVED
	[ImportedDay] INT, -- DERIVED
	[ImportedISOWeek] INT, -- DERIVED
	[ImportedWeekday] VARCHAR(10), -- DERIVED
	[ImportedDate] DATE, -- DERIVED
	[ImportedHour] INT, -- DERIVED
	[FileHashID] INT,
	[FileHashKeyType] VARCHAR(9),
	[FileHash] VARCHAR(34),
	[Title] NVARCHAR(100),
	[Description] NVARCHAR(MAX),
	[PlaceID] INT,
	[Place] NVARCHAR(50),
	[Unit] NVARCHAR(20),
	[Street] NVARCHAR(50),
	[Locality] NVARCHAR(50),
	[Location] NVARCHAR(50),
	[Region] NVARCHAR(50),
	[Country] NVARCHAR(40),
	[Continent] NVARCHAR(13),
	[Code] NVARCHAR(50),
	[AddressID] INT,
	[LocationID] INT,
	[RegionID] INT,
	[CountryID] INT
)
AS
BEGIN
	DECLARE @FilePaths [strings].[ListFilePaths];

	INSERT INTO @FilePaths ([ID], [FilePath])
	VALUES (1, @FilePath);

	INSERT @returntable
	SELECT
		[Files].[FilePathID],
		[Files].[FilePath],
		[Files].[File],
		[Files].[DirID],
		[Files].[Drive],
		[Files].[Path],
		[Files].[Folder],
		[Files].[DirActual],
		[Files].[DirHashID],
		[Files].[DirHashKeyType],
		[Files].[DirHash],
		[Files].[FileID],
		[Files].[FileDirID],
		[Files].[FileName],
		[Files].[Extension],
		[Files].[MimeType],
		[Files].[Actual],
		[Files].[Size],
		[Files].[Created],
		[Files].[CreatedYear],
		[Files].[CreatedQuarter],
		[Files].[CreatedMonth],
		[Files].[CreatedDay],
		[Files].[CreatedISOWeek],
		[Files].[CreatedWeekday],
		[Files].[CreatedDate],
		[Files].[CreatedHour],
		[Files].[Updated],
		[Files].[UpdatedYear],
		[Files].[UpdatedQuarter],
		[Files].[UpdatedMonth],
		[Files].[UpdatedDay],
		[Files].[UpdatedISOWeek],
		[Files].[UpdatedWeekday],
		[Files].[UpdatedDate],
		[Files].[UpdatedHour],
		[Files].[InSynch],
		[Files].[Synchronised],
		[Files].[SynchronisedYear],
		[Files].[SynchronisedQuarter],
		[Files].[SynchronisedMonth],
		[Files].[SynchronisedDay],
		[Files].[SynchronisedISOWeek],
		[Files].[SynchronisedWeekday],
		[Files].[SynchronisedDate],
		[Files].[SynchronisedHour],
		[Files].[RowUpdated],
		[Files].[RowUpdatedYear],
		[Files].[RowUpdatedQuarter],
		[Files].[RowUpdatedMonth],
		[Files].[RowUpdatedDay],
		[Files].[RowUpdatedISOWeek],
		[Files].[RowUpdatedWeekday],
		[Files].[RowUpdatedDate],
		[Files].[RowUpdatedHour],
		[Files].[Imported],
		[Files].[ImportedYear],
		[Files].[ImportedQuarter],
		[Files].[ImportedMonth],
		[Files].[ImportedDay],
		[Files].[ImportedISOWeek],
		[Files].[ImportedWeekday],
		[Files].[ImportedDate],
		[Files].[ImportedHour],
		[Files].[FileHashID],
		[Files].[FileHashKeyType],
		[Files].[FileHash],
		[Image].[Title],
		[Image].[Description],
		[Image].[PlaceID],
		[Places].[Place],
		[Places].[Unit],
		[Places].[Street],
		[Places].[Locality],
		[Places].[Location],
		[Places].[Region],
		[Places].[Country],
		[Places].[Continent],
		[Places].[Code],
		[Places].[AddressID],
		[Places].[LocationID],
		[Places].[RegionID],
		[Places].[CountryID]
	FROM [gui].[FilesForGrouping](NULL, NULL, NULL, NULL, NULL, NULL, NULL, @FilePaths) AS [Files]
	LEFT JOIN [data].[Images] AS [Image]
		ON [Files].[FileID] = [Image].[FileID]
	LEFT JOIN [gui].[Place](NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL) AS [Places]
		ON [Image].[PlaceID] = [Places].[PlaceID];

	RETURN
END