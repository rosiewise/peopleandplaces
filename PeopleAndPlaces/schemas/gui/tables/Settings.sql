﻿CREATE TABLE [gui].[Settings] (
    [ID]      INT NOT NULL,
    [Testing] BIT DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);