﻿/*
TagFilesWithPerson procedure to add association between a file and person
-------------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-02-29
*/

CREATE PROCEDURE [gui].[TagFilesWithPerson]
	@Files [strings].[ListInt] READONLY,
	@PersonID INT = NULL,
	@FirstName NVARCHAR(50) = NULL,
	@LastName NVARCHAR(50) = NULL,
	@Gender TINYINT = NULL,
	@DOB DATE = NULL,
	@RoleID INT = NULL,
	@Role NVARCHAR(50) = NULL
AS
BEGIN
	BEGIN TRAN

	IF (@PersonID IS NULL)
		SET @PersonID = [data].[PersonID](
			@FirstName, @LastName, @Gender, @DOB);

	IF (@RoleID IS NULL)
		SET @RoleID = [data].[RoleID](@Role);

	IF (@PersonID > 0 AND @RoleID > 0)
		INSERT INTO [data].[Files_People] (
			[FileID],
			[PersonID],
			[RoleID]
		) SELECT
			f.ID,
			@PersonID,
			@RoleID
		FROM @Files AS f;

	COMMIT TRAN;
	RETURN @PersonID;
END;