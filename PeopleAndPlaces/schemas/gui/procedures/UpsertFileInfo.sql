﻿/*
UpsertFileInfo procedure
------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-02-29
*/

CREATE PROCEDURE [gui].[UpsertFileInfo](
	@FullPath NVARCHAR(MAX),
	@FileHash VARCHAR(34),
	@Created DATETIME = NULL,
	@Updated DATETIME = NULL,
	@Size INT = NULL,
	@InSynch BIT = NULL
) AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	-----------
	-- LOGIC --
	-----------
	BEGIN TRY
		DECLARE @IsSupported BIT = 0;
		DECLARE @Drive NVARCHAR(5) = NULL;
		DECLARE @Path NVARCHAR(255) = NULL;
		DECLARE @FileName NVARCHAR(255) = NULL;
		DECLARE @Extension NVARCHAR(5) = NULL;
		DECLARE @MimeType NVARCHAR(50) = NULL;
		DECLARE @DirectoryHash VARCHAR(34) = NULL;
		DECLARE @Note VARCHAR(1250);
		DECLARE @DirectoryHashID INT;
		DECLARE @FileHashID INT;
		DECLARE @DirectoryID INT;
		DECLARE @FileID INT;

		SELECT
			@Drive = [Drive],
			@Path = [Path],
			@FileName = [FileName],
			@Extension = [Extension],
			@MimeType = [MimeType],
			@IsSupported = [IsSupported],
			@DirectoryHash = COALESCE([PathHash], @DirectoryHash), -- force a row
			@Note = [Note]
		FROM [data].[CheckFullPath](@FullPath);

		IF @IsSupported = 0
		BEGIN

			DECLARE @call VARCHAR(1250);
			SELECT @call = '. Call ' + [Value]
			FROM [strings].[input_UpsertFileInfo](@FullPath, @FileHash, @Created, @Updated, @Size, @InSynch, NULL);
			
			IF @IsSupported = 0 AND @FullPath IS NULL
				SET @call = '@FullPath can not be NULL' + @call;
			ELSE IF @IsSupported = 0 AND @Note IS NOT NULL
				SET @call = @Note + @call;

			EXEC [data].[*commit];
			RAISERROR('%s', 15, 1, @call);

		END;

		-- Add directory hash value
		EXEC @DirectoryHashID = [data].[UpsertHash]
			@KeyType = "directory",
			@Hash = @DirectoryHash;

		-- Add file hash value
		EXEC @FileHashID = [data].[UpsertHash]
			@KeyType = "file",
			@Hash = @FileHash;

		-- Add/update the directory
		EXEC @DirectoryID = [data].[UpsertDirectory]
			@Drive = @Drive,
			@Path = @Path,
			@Actual = true,
			@HashID = @DirectoryHashID;

		-- Add/update the file
		EXEC @FileID = [data].[UpsertFile]
			@DirectoryID = @DirectoryID,
			@FileName = @FileName,
			@Extension = @Extension,
			@MimeType = @MimeType,
			@HashID = @FileHashID,
			@Created = @Created,
			@Updated = @Updated,
			@Size = @Size,
			@InSynch = @InSynch;

		RETURN @FileID;
	END TRY
	BEGIN CATCH
		
		SELECT @note = [Value]
		FROM [strings].[input_UpsertFileInfo](
			@FullPath, @FileHash, @Created, @Updated, @Size, @InSynch, NULL);
		
		EXEC [data].[*error_handler] 'UpsertFileInfo', @note;

		RETURN -1;
	END CATCH
END;