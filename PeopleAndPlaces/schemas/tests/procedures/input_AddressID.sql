﻿/*
Excutes a single input test for AddressID with given parameters
---------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:	2016-01-11
*/

CREATE PROCEDURE [tests].[input_AddressID](
	@Unit NVARCHAR(20),
	@Street NVARCHAR(50),
	@Locality NVARCHAR(50),
	@LocationID INT,
	@Code NVARCHAR(50),
	@pass BIT,
	@testname VARCHAR(100)
) AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	DECLARE @object VARCHAR(50) = 'AddressID';
	DECLARE @testnote VARCHAR(350);
	DECLARE @start DATETIME;
	DECLARE @ms INT = NULL;
	DECLARE @fail BIT = ~@pass;
	DECLARE @result INT = NULL;
		
	SELECT @testnote = 'call ' + [Value]
	FROM [strings].[input_AddressID](@Unit, @Street, @Locality, @LocationID, @Code, @pass);

	-------------------
	-- START OF TEST --
	-------------------
	BEGIN TRY
		-- TEST START
		SET @start = GETDATE();

		SET @result = [data].[AddressID](@Unit, @Street, @Locality, @LocationID, @Code);

		SET @ms = [tests].[Since](@start);

		-- REPORT RESULT
		IF (@pass = 1 AND @result IS NOT NULL) OR (@fail = 1 AND @result IS NULL)
			EXEC [tests].[Results] @object, @testname, @testnote, 1, @ms;
		ELSE
			EXEC [tests].[*error_handler] @object, @testname, @testnote, 0, @ms;

		-- STABILISE END POINT
		EXEC [data].[*commit];
	END TRY
	BEGIN CATCH
		EXEC [data].[*rollback];

		-- REPORT RESULT
		IF @ms IS NULL SET @ms = [tests].[Since](@start);

		EXEC [tests].[Results] @object, @testname, @testnote, @fail, @ms;
	END CATCH;
END;