﻿CREATE PROCEDURE [tests].[*reset] (
	@AddTestData BIT = 0
) WITH EXECUTE AS 'data_user'
AS
	PRINT 'user ' + SYSTEM_USER + ' running as ' + USER;
	EXEC [data].[*reset] @AddTestData;
RETURN 0