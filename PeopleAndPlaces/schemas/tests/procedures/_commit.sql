﻿CREATE PROCEDURE [tests].[*commit]
WITH EXECUTE AS 'data_user'
AS
	EXEC [data].[*commit];
RETURN 0