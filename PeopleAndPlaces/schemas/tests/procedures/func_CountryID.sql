﻿/*
Test Suite for CountryID function
---------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:	2016-01-28
*/

CREATE PROCEDURE [tests].[func_CountryID] AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	DECLARE @object VARCHAR(50);

	SET @object = 'CountryID';

	--DECLARE @trancount INT;
	--SET @trancount = @@TRANCOUNT;

	BEGIN TRY

		EXEC [tests].[*reset];

		-- Country can't be null
		EXEC	[tests].[input_CountryID]
				@Country = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '01. All null';

		-- Country is bad data
		EXEC	[tests].[input_CountryID]
				@Country = 1,
				@pass = 0,
				@expected = NULL,
				@testname = '02. INT Country';

		-- Country is fake
		EXEC	[tests].[input_CountryID]
				@Country = 'Fake',
				@pass = 0,
				@expected = NULL,
				@testname = '03. Invalid country';

		-- Country is valid
		EXEC	[tests].[input_CountryID]
				@Country = 'Cuba',
				@pass = 1,
				@expected = 57,
				@testname = '04. Valid country';

		-- Country is valid
		EXEC	[tests].[input_CountryID]
				@Country = 'Ireland',
				@pass = 1,
				@expected = 103,
				@testname = '05. Valid country';

		-- Country is valid
		EXEC	[tests].[input_CountryID]
				@Country = 'Peru',
				@pass = 1,
				@expected = 169,
				@testname = '06. Valid country';

		-- Country is valid
		EXEC	[tests].[input_CountryID]
				@Country = 'Yemen',
				@pass = 1,
				@expected = 239,
				@testname = '07. Valid country';

		-- Country is partly valid
		EXEC	[tests].[input_CountryID]
				@Country = 'Ire',
				@pass = 0,
				@expected = NULL,
				@testname = '08. Country is part matched';

		-- RECORD TEST SUITE PASS
		EXEC [tests].[Results] @object, '09. Test Suite', 'Attempted all tests', 1, 0;
	END TRY
	BEGIN CATCH
		-- RECORD TEST SUITE FAIL
		EXEC [tests].[*error_handler] @object, '09. Test Suite', 'Attempted all tests', 0, 0;

		RETURN -1
	END CATCH
END;