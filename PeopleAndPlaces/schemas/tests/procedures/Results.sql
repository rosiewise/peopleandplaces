﻿/*
Creates or updates a set of test results in the TestResults table
-----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-09
Updated:	2016-01-08
*/

CREATE PROCEDURE [tests].[Results]
	@Object VARCHAR(50),
	@TestName VARCHAR(100),
	@Note VARCHAR(MAX),
	@Pass BIT = 1,
	@Time INT = NULL,
	@Error NVARCHAR(4000) = NULL,
	@ErrorNumber INT = NULL,
	@ErrorSeverity INT = NULL,
	@ErrorState INT = NULL,
	@ErrorProcedure NVARCHAR(128) = NULL,
	@ErrorLine INT = NULL
AS
	SET NOCOUNT ON;
	
	EXEC [data].[*commit];

	BEGIN TRY
		BEGIN TRY
			--print 'before insert: ' + @Object + ' ' + @TestName + ' ' + LEFT(@Note, 25);
			INSERT INTO [tests].[_TestResults] (
				[Date],
				[Object],
				[TestName],
				[Note],
				[Pass],
				[Time],
				[Error],
				[ErrorNumber],
				[ErrorSeverity],
				[ErrorState],
				[ErrorProcedure],
				[ErrorLine],
				[ShortNote]
			) VALUES (
				GETDATE(),
				@Object,
				@TestName,
				@Note,
				@Pass,
				@Time,
				@Error,
				@ErrorNumber,
				@ErrorSeverity,
				@ErrorState,
				@ErrorProcedure,
				@ErrorLine,
				LEFT(@Note, 25)
			);
			print 'after insert: ' + @Object + ' ' + @TestName + ' ' + LEFT(@Note, 25);
			--SET NOCOUNT OFF;
		END TRY
		BEGIN CATCH
			EXEC [data].[*rollback];

			--print 'before update: ' + @Object + ' ' + @TestName + ' ' + LEFT(@Note, 25);
			UPDATE [tests].[_TestResults]
			SET [Date] = GETDATE(),
				[Note] = @Note,
				[Pass] = @Pass,
				[Time] = @Time,
				[Error] = @Error,
				[ErrorNumber] = @ErrorNumber,
				[ErrorSeverity] = @ErrorSeverity,
				[ErrorState] = @ErrorState,
				[ErrorProcedure] = @ErrorProcedure,
				[ErrorLine] = @ErrorLine
			WHERE [Object] = @Object
			AND [TestName] = @TestName
			AND [ShortNote] = LEFT(@Note, 25);
			print 'after update: ' + @Object + ' ' + @TestName + ' ' + LEFT(@Note, 25);
			--SET NOCOUNT OFF;
		END CATCH;
	END TRY
	BEGIN CATCH
		PRINT 'Error while saving to _TestResults table ' + ERROR_MESSAGE();
		-- SELECT ERROR_MESSAGE();
	END CATCH;
	EXEC [data].[*commit];
RETURN 0