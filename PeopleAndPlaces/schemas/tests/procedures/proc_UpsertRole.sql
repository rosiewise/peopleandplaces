﻿/*
Test Suite for UpsertRole procedure
-----------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-11
Updated:	2016-01-08
*/

CREATE PROCEDURE [tests].[proc_UpsertRole] AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	DECLARE @object VARCHAR(50);
	DECLARE @xID INT;
	
	SET @object = 'UpsertRole';

	BEGIN TRY

		EXEC [tests].[*reset];

		EXEC	[tests].[input_UpsertRole]
				@ID = NULL,
				@Role = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '01. All null';

		EXEC	[tests].[input_UpsertRole]
				@ID = NULL,
				@Role = 'x',
				@pass = 1,
				@expected = 1,
				@testname = '02. Insert role x';

		EXEC	[tests].[input_UpsertRole]
				@ID = NULL,
				@Role = 'x',
				@pass = 1,
				@expected = 1,
				@testname = '03. Able to try add x again without errors';

		EXEC	[tests].[input_UpsertRole]
				@ID = NULL,
				@Role = 'X',
				@pass = 1,
				@expected = 1,
				@testname = '04. Try adding X';

		SET @xID = [data].[RoleID]('X');
		EXEC	[tests].[input_UpsertRole]
				@ID = 1,
				@Role = 'y',
				@pass = 1,
				@expected = @xID,
				@testname = '05. Change X to y';

		EXEC	[tests].[input_UpsertRole]
				@ID = NULL,
				@Role = 'X',
				@pass = 1,
				@expected = 2,
				@testname = '06. Try adding X again';

		-- X is already in as row 2, should return 2
		EXEC	@xID = [tests].[input_UpsertRole]
				@ID = 1,
				@Role = 'X',
				@pass = 0,
				@expected = 2,
				@testname = '07. Try changing ID 1 (should be y) to X';

		-- RECORD TEST SUITE PASS
		EXEC [tests].[Results] @object, '08. Test Suite', 'Attempted all tests', 1, 0;
	END TRY
	BEGIN CATCH
		-- RECORD TEST SUITE FAIL
		EXEC [tests].[*error_handler] @object, '08. Test Suite', 'Attempted all tests', 0, 0;

		RETURN -1
	END CATCH
END;