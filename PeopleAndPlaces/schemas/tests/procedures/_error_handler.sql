﻿/*
The error handler for use when tests are being run
--------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2016-01-28
*/

CREATE PROCEDURE [tests].[*error_handler]
	@Object VARCHAR(50),
	@Name VARCHAR(100),
	@Note VARCHAR(MAX),
	@Pass BIT = 1,
	@Time INT = NULL
WITH EXECUTE AS 'data_user'
AS
	--SET XACT_ABORT OFF;

	DECLARE @errmsg		NVARCHAR(4000) = error_message(),
			@severity	TINYINT = error_severity(),
			@state		TINYINT = error_state(),
			@errno		INT = error_number(),
			@proc		SYSNAME = error_procedure(),
			@lineno		INT = error_line(),
			@logerrmsg	NVARCHAR(4000) = error_message(),
			@date		DATE = GETDATE(),
			@procid		VARCHAR(20) = @@PROCID,
			@trancount	INT = @@TRANCOUNT;
       
	IF @errmsg NOT LIKE '***%'
	BEGIN
		SELECT @errmsg = '*** ' + coalesce(quotename(@proc), '<dynamic SQL>') + 
						', Line ' + ltrim(str(@lineno)) + '. Errno ' + 
						ltrim(str(@errno)) + ': ' + @errmsg
	END

	PRINT 'TEST-ERR: ' + @errmsg;

	EXEC [tests].[*rollback];
	EXEC [tests].[*commit];

	EXEC [tests].[Results]
		@Object, @Name, @Note, @Pass, @Time, @logerrmsg, @errno, @severity, @state, @proc, @lineno;

	EXEC [tests].[*rollback];
	EXEC [tests].[*commit];
	--SET XACT_ABORT ON;

IF @errno IN (226, 50000)
	RAISERROR('%s', 1, @state, @errmsg);
ELSE
	RAISERROR('%s', @severity, @state, @errmsg);