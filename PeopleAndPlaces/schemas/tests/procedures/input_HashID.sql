﻿/*
Excutes a single input test for HashID with given parameters
------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:	2016-01-08
*/

CREATE PROCEDURE [tests].[input_HashID](
	@KeyType NVARCHAR(9),
	@Hash NVARCHAR(32),
	@pass BIT,
	@expected INT,
	@testname VARCHAR(100)
) AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	DECLARE @object VARCHAR(50) = 'HashID';
	DECLARE @testnote VARCHAR(350);
	DECLARE @start DATETIME;
	DECLARE @ms INT = NULL;
	DECLARE @fail BIT = ~@pass;
	DECLARE @result INT = NULL;

	SELECT @testnote = 'call ' + [Value]
	FROM [strings].[input_HashID](@KeyType, @Hash, @pass);

	-------------------
	-- START OF TEST --
	-------------------
	BEGIN TRY
		-- STABALISE START POINT
		EXEC [data].[*commit];

		-- TEST START
		SET @start = GETDATE();

		SET @result = [data].[HashID](@KeyType, @Hash);

		SET @ms = [tests].[Since](@start);

		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;

		-- REPORT RESULT
		IF (@pass = 1 AND @result IS NOT NULL) OR (@fail = 1 AND @result IS NULL)
			EXEC [tests].[Results] @object, @testname, @testnote, 1, @ms;
		ELSE
			EXEC [tests].[*error_handler] @object, @testname, @testnote, 0, @ms;

		-- STABILISE END POINT
		EXEC [data].[*commit];

	END TRY
	BEGIN CATCH
		-- RESET TRANSACTION STATE
		EXEC [data].[*rollback];

		-- REPORT RESULT
		IF @ms IS NULL SET @ms = [tests].[Since](@start);
		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;
		IF @fail = 1 EXEC [tests].[Results] @object, @testname, @testnote, @fail, @ms;
		ELSE EXEC [tests].[*error_handler] @object, @testname, @testnote, @fail, @ms;
	END CATCH;
END;