﻿/*
Test Suite for HashID function
------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-17
Updated:	2016-01-28
*/

CREATE PROCEDURE [tests].[func_HashID] AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	DECLARE @object VARCHAR(50) = 'HashID';

	BEGIN TRY

		EXEC [tests].[*reset];

		-- For these tests, a FAIL is a False or empty result
		INSERT INTO [data].[Hashes] ([KeyType], [Hash])
		VALUES
			('file', 'a'),
			('file', 'b'),
			('directory', 'a'),
			('directory', 'b');

		-----------------------------------
		-- TEST EXPECTED TO FAIL FATALLY --
		-----------------------------------
		BEGIN TRY
			EXEC	[tests].[*commit];
			EXEC	[tests].[input_HashID]
					@pass = 0,
					@expected = NULL,
					@testname = '01. No input';
			EXEC	[tests].[*commit];
		END TRY
		BEGIN CATCH
			EXEC [tests].[*rollback];
			EXEC [tests].[Results] @object, '01. No input', 'call () fails', 1, 0;
		END CATCH;

		BEGIN TRY
			EXEC	[tests].[*commit];
			EXEC	[tests].[input_HashID]
					@Hash = 'a',
					@pass = 0,
					@expected = NULL,
					@testname = '02. No KeyType';
			EXEC	[tests].[*commit];
		END TRY
		BEGIN CATCH
			EXEC [tests].[*rollback];
			EXEC [tests].[Results] @object, '02. No KeyType', 'call (''a'') fails', 1, 0;
		END CATCH;

		BEGIN TRY
			EXEC	[tests].[*commit];
			EXEC	[tests].[input_HashID]
					@KeyType = 'file',
					@pass = 0,
					@expected = 1,
					@testname = '03. No Hash';
			EXEC	[tests].[*commit];
		END TRY
		BEGIN CATCH
			EXEC [tests].[*rollback];
			EXEC [tests].[Results] @object, '03. No Hash', 'call (''file'') fails', 1, 0;
		END CATCH;

		-------------------------------------------
		-- EVERYTHING ELSE THAT FAILS GRACEFULLY --
		-------------------------------------------

		EXEC	[tests].[input_HashID]
				@KeyType = NULL,
				@Hash = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '04. All null';
		
		EXEC	[tests].[input_HashID]
				@KeyType = 1,
				@Hash = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '05. INT KeyType';

		EXEC	[tests].[input_HashID]
				@KeyType = NULL,
				@Hash = 1,
				@pass = 0,
				@expected = NULL,
				@testname = '06. INT Hash';
		
		EXEC	[tests].[input_HashID]
				@KeyType = 'fake',
				@Hash = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '07. Invalid KeyType';

		EXEC	[tests].[input_HashID]
				@KeyType = NULL,
				@Hash = 'fake',
				@pass = 0,
				@expected = NULL,
				@testname = '08. Invalid KeyType, valid Hash';

		EXEC	[tests].[input_HashID]
				@KeyType = 'file',
				@Hash = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '09. Valid KeyType, invalid Hash';

		EXEC	[tests].[input_HashID]
				@KeyType = 'file',
				@Hash = 'a',
				@pass = 1,
				@expected = 1,
				@testname = '10. Valid KeyType, Hash combo';

		EXEC	[tests].[input_HashID]
				@KeyType = 'file',
				@Hash = 'b',
				@pass = 1,
				@expected = 2,
				@testname = '11. Valid KeyType, Hash combo';

		EXEC	[tests].[input_HashID]
				@KeyType = 'file',
				@Hash = 'c',
				@pass = 0,
				@expected = NULL,
				@testname = '12. Valid KeyType, invalid Hash';

		EXEC	[tests].[input_HashID]
				@KeyType = 'directory',
				@Hash = 'a',
				@pass = 1,
				@expected = 3,
				@testname = '13. Valid KeyType, Hash combo';

		EXEC	[tests].[input_HashID]
				@KeyType = 'directory',
				@Hash = 'b',
				@pass = 1,
				@expected = 4,
				@testname = '14. Valid KeyType, Hash combo';

		EXEC	[tests].[input_HashID]
				@KeyType = 'directory',
				@Hash = 'c',
				@pass = 0,
				@expected = NULL,
				@testname = '15. Valid KeyType, invalid Hash';

		-- RECORD TEST SUITE PASS
		EXEC [tests].[Results] @object, '16. Test Suite', 'Attempted all tests', 1, 0;
		
	END TRY
	BEGIN CATCH
		-- RECORD TEST SUITE FAIL
		EXEC [tests].[*error_handler] @object, '16. Test Suite', 'Attempted all tests', 0, 0;

		RETURN -1
	END CATCH
END;