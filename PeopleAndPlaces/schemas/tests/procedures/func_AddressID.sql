﻿/*
Test Suite for AddressID function
---------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-05
Updated:	2016-01-10
*/

CREATE PROCEDURE [tests].[func_AddressID] AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	--DECLARE @trancount INT;
	--SET @trancount = @@TRANCOUNT;

	BEGIN TRY

		EXEC [data].[*reset];

		-- For these tests, a FAIL is a False or empty result
		/*
		INSERT INTO [Countries] ([Country], [Continent])
		VALUES ('England', 'Europe'),
			('Ireland', 'Europe'),
			('Scotland', 'Europe'),
			('Wales', 'Europe');
			*/

		EXEC	[tests].[input_AddressID]
				@Unit = NULL,
				@Street = NULL,
				@Locality = NULL,
				@LocationID = NULL,
				@Code = NULL,
				@pass = 0,
				@testname = '01. All null';

		/*
		-- Country can't be null
		EXEC	[tests].[input_AddressID]
				@Country = NULL,
				@pass = 0,
				@testname = '01. All null';

		-- Country is bad data
		EXEC	[tests].[input_AddressID]
				@Country = 1,
				@pass = 0,
				@testname = '02. INT Country';

		-- Country is fake
		EXEC	[dbo].[_test_input_CountryID]
				@Country = 'Fake',
				@pass = 0,
				@testname = '03. Invalid country';

		-- Country is valid
		EXEC	[dbo].[_test_input_CountryID]
				@Country = 'England',
				@pass = 1,
				@testname = '04. Valid country';

		-- Country is valid
		EXEC	[dbo].[_test_input_CountryID]
				@Country = 'Ireland',
				@pass = 1,
				@testname = '05. Valid country';

		-- Country is valid
		EXEC	[dbo].[_test_input_CountryID]
				@Country = 'Scotland',
				@pass = 1,
				@testname = '06. Valid country';

		-- Country is valid
		EXEC	[dbo].[_test_input_CountryID]
				@Country = 'Wales',
				@pass = 1,
				@testname = '07. Valid country';

		-- Country is partly valid
		EXEC	[dbo].[_test_input_CountryID]
				@Country = 'Eng',
				@pass = 0,
				@testname = '08. Country is part matched';
				*/

		-- RECORD TEST SUITE PASS
		EXEC [tests].[Results] 'AddressID', '02. Test Suite', 'Attempted all tests', 1;
	END TRY
	BEGIN CATCH
		-- RECORD TEST SUITE FAIL
		EXEC [tests].[*error_handler] 'AddressID', '02. Test Suite', 'Attempted all tests', 0;

		RETURN -1
	END CATCH
END;