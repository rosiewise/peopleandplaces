﻿/*
Test Suite for OrganisationID function
--------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:	2016-01-28
*/

CREATE PROCEDURE [tests].[func_OrganisationID] AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	DECLARE @object VARCHAR(50);

	SET @object = 'OrganisationID';

	--DECLARE @trancount INT;
	--SET @trancount = @@TRANCOUNT;

	BEGIN TRY

		EXEC [tests].[*reset];

		-- For these tests, a FAIL is a False or empty result
		INSERT INTO [data].[Organisations] ([OrganisationName], [Description])
		VALUES ('Stereophonics', 'My favourite band'),
			('The Toi', 'Another of my favourite bands'),
			('SnowPatrol', 'Another of my favourite bands'),
			('Casino', 'Another of my favourite bands');

		-- Name can't be null
		EXEC	[tests].[input_OrganisationID]
				@Name = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '01. All null';

		-- Name is bad data
		EXEC	[tests].[input_OrganisationID]
				@Name = 1,
				@pass = 0,
				@expected = NULL,
				@testname = '02. INT Country';

		-- Name is fake
		EXEC	[tests].[input_OrganisationID]
				@Name = 'Fake',
				@pass = 0,
				@expected = NULL,
				@testname = '03. Invalid country';

		-- Name is valid
		EXEC	[tests].[input_OrganisationID]
				@Name = 'Stereophonics',
				@pass = 1,
				@expected = 1,
				@testname = '04. Valid organisation';

		-- Name is valid
		EXEC	[tests].[input_OrganisationID]
				@Name = 'The Toi',
				@pass = 1,
				@expected = 2,
				@testname = '05. Valid organisation';

		-- Name is valid
		EXEC	[tests].[input_OrganisationID]
				@Name = 'Casino',
				@pass = 1,
				@expected = 4,
				@testname = '06. Valid organisation';

		-- Name is valid
		EXEC	[tests].[input_OrganisationID]
				@Name = 'SnowPatrol',
				@pass = 1,
				@expected = 3,
				@testname = '07. Valid organisation';

		-- Name is partly valid
		EXEC	[tests].[input_OrganisationID]
				@Name = 'The',
				@pass = 0,
				@expected = NULL,
				@testname = '08. Organisation is part matched';

		-- RECORD TEST SUITE PASS
		EXEC [tests].[Results] @object, '09. Test Suite', 'Attempted all tests', 1, 0;
	END TRY
	BEGIN CATCH
		-- RECORD TEST SUITE FAIL
		EXEC [tests].[*error_handler] @object, '09. Test Suite', 'Attempted all tests', 0, 0;

		RETURN -1
	END CATCH
END;