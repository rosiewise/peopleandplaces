﻿/*
Test Suite for RegionID function
--------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-17
Updated:	2016-01-28
*/

CREATE PROCEDURE [tests].[func_RegionID] AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	DECLARE @object VARCHAR(50) = 'RegionID';

	BEGIN TRY

		EXEC [tests].[*reset];

		-- For these tests, a FAIL is a False or empty result
		INSERT INTO [data].[Regions] ([Region], [CountryID])
		VALUES
			('Alsace', NULL),
			('Alsace', 76),
			('Alsace', 81),
			('Brandenburg', NULL),
			('Brandenburg', 81),
			('Cumbria', NULL),
			('Cumbria', 230);

		-----------------------------------
		-- TEST EXPECTED TO FAIL FATALLY --
		-----------------------------------
		BEGIN TRY
			EXEC	[tests].[*commit];
			EXEC	[tests].[input_RegionID]
					@pass = 0,
					@expected = NULL,
					@testname = '01. No input';
			EXEC	[tests].[*commit];
		END TRY
		BEGIN CATCH
			EXEC [tests].[*rollback];
			EXEC [tests].[Results] @object, '01. No input', 'call () fails', 1, 0;
		END CATCH;

		BEGIN TRY
			EXEC	[tests].[*commit];
			EXEC	[tests].[input_RegionID]
					@CountryID = 81,
					@pass = 0,
					@expected = NULL,
					@testname = '02. No Region';
			EXEC	[tests].[*commit];
		END TRY
		BEGIN CATCH
			EXEC [tests].[*rollback];
			EXEC [tests].[Results] @object, '02. No Region', 'call (81) fails', 1, 0;
		END CATCH;

		BEGIN TRY
			EXEC	[tests].[*commit];
			EXEC	[tests].[input_RegionID]
					@Region = 'Alsace',
					@pass = 0,
					@expected = 1,
					@testname = '03. No CountryID';
			EXEC	[tests].[*commit];
		END TRY
		BEGIN CATCH
			EXEC [tests].[*rollback];
			EXEC [tests].[Results] @object, '03. No CountryID', 'call (''Alsace'') fails', 1, 0;
		END CATCH;

		-------------------------------------------
		-- EVERYTHING ELSE THAT FAILS GRACEFULLY --
		-------------------------------------------

		EXEC	[tests].[input_RegionID]
				@Region = NULL,
				@CountryID = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '04. All null';
		
		EXEC	[tests].[input_RegionID]
				@Region = 1,
				@CountryID = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '05. INT Region';
		
		EXEC	[tests].[input_RegionID]
				@Region = 'Narnia',
				@CountryID = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '06. Invalid Region';

		EXEC	[tests].[input_RegionID]
				@Region = 'Alsace',
				@CountryID = NULL,
				@pass = 1,
				@expected = 1,
				@testname = '07. Valid Region, no CountryID';

		EXEC	[tests].[input_RegionID]
				@Region = 'Alsace',
				@CountryID = 76,
				@pass = 1,
				@expected = 2,
				@testname = '08. Valid Region, with CountryID';

		EXEC	[tests].[input_RegionID]
				@Region = 'Alsace',
				@CountryID = 50,
				@pass = 0,
				@expected = NULL,
				@testname = '09. Wrong valid Region and CountryID combo';

		EXEC	[tests].[input_RegionID]
				@Region = 'Alsace',
				@CountryID = 81,
				@pass = 1,
				@expected = 3,
				@testname = '10. Valid Region, with different CountryID';

		EXEC	[tests].[input_RegionID]
				@Region = 'Alsace',
				@CountryID = 500,
				@pass = 0,
				@expected = NULL,
				@testname = '11. Valid Region, with invalid CountryID';

		EXEC	[tests].[input_RegionID]
				@Region = 'Brandenburg',
				@CountryID = NULL,
				@pass = 1,
				@expected = 4,
				@testname = '12. Valid Region, null CountryID';

		EXEC	[tests].[input_RegionID]
				@Region = 'Brandenburg',
				@CountryID = 81,
				@pass = 1,
				@expected = 5,
				@testname = '13. Valid Region, with CountryID';

		EXEC	[tests].[input_RegionID]
				@Region = 'Brandenburg',
				@CountryID = 500,
				@pass = 0,
				@expected = NULL,
				@testname = '14. Valid Region, with invalid CountryID';

		EXEC	[tests].[input_RegionID]
				@Region = 'Cumb',
				@CountryID = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '15. Region is part matched, null CountryID';

		-- RECORD TEST SUITE PASS
		EXEC [tests].[Results] @object, '16. Test Suite', 'Attempted all tests', 1, 0;
	END TRY
	BEGIN CATCH
		-- RECORD TEST SUITE FAIL
		EXEC [tests].[*error_handler] @object, '16. Test Suite', 'Attempted all tests', 0, 0;

		RETURN -1
	END CATCH
END;