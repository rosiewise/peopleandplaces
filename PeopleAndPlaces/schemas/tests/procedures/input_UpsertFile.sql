﻿/*
Excutes a single input test for UpsertFile with given parameters
----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-10
Updated:	2016-01-08
*/

CREATE PROCEDURE [tests].[input_UpsertFile](
	@DirectoryID INT,
	@FileName NVARCHAR(255),
	@Extension NVARCHAR(5),
	@MimeType NVARCHAR(50),
	@HashID INT,
	@Created SMALLDATETIME,
	@Updated SMALLDATETIME,
	@Size INT,
	@InSynch BIT,
	@pass BIT,
	@expected INT,
	@testname VARCHAR(100)
) AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	DECLARE @object VARCHAR(50) = 'UpsertFile';
	DECLARE @testnote VARCHAR(350);
	DECLARE @start DATETIME;
	DECLARE @ms INT = NULL;
	DECLARE @fail BIT = ~@pass;
	DECLARE @result INT = NULL;

	SELECT @testnote = 'call ' + [Value]
	FROM [strings].[input_UpsertFile](
		@DirectoryID, @FileName, @Extension, @MimeType, @HashID,
		@Created, @Updated, @Size, @InSynch, @pass);

	-------------------
	-- START OF TEST --
	-------------------
	BEGIN TRY
		-- STABLISE START POINT
		EXEC [data].[*commit];

		-- TEST START
		SET @start = GETDATE();

		EXEC @result = [data].[UpsertFile] @DirectoryID, @FileName, @Extension, @MimeType, @HashID,
			@Created, @Updated, @Size, @InSynch, @HashID;

		SET @ms = [tests].[Since](@start);

		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;

		-- REPORT RESULT
		IF @pass = 1 EXEC [tests].[Results] @object, @testname, @testnote, @pass, @ms
		ELSE EXEC [tests].[*error_handler] @object, @testname, @testnote, @pass, @ms;
			
		-- STABILISE END POINT
		EXEC [data].[*commit];

	END TRY
	BEGIN CATCH
		-- RESET TRANSACTION STATE
		EXEC [data].[*rollback];

		-- REPORT RESULT
		IF @ms IS NULL SET @ms = [tests].[Since](@start);
		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;
		IF @fail = 1 EXEC [tests].[Results] @object, @testname, @testnote, @fail, @ms;
		ELSE EXEC [tests].[*error_handler] @object, @testname, @testnote, @fail, @ms;
	END CATCH;
END;