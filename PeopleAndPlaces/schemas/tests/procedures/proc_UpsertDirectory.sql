﻿/*
Test Suite for UpsertDirectory procedure
----------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-28
Updated:	2016-01-28
*/

CREATE PROCEDURE [tests].[proc_UpsertDirectory] AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	DECLARE @object VARCHAR(50);

	SET @object = 'UpsertDirectory';

	--DECLARE @trancount INT;
	--SET @trancount = @@TRANCOUNT;

	BEGIN TRY

		EXEC [tests].[*reset];

		-- Volume can't be null
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = NULL,
				@Path = NULL,
				@Actual = NULL,
				@HashID = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '01. All null';

		-- Path can't be null
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = 'abc',
				@Path = NULL,
				@Actual = NULL,
				@HashID = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '02. Just Volume';

		-- Volume can't be null
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = NULL,
				@Path = 'xyz',
				@Actual = NULL,
				@HashID = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '03. Just Path';

		-- Actual can't be null
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = 'abc',
				@Path = 'xyz',
				@Actual = NULL,
				@HashID = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '04. Volume and Path only';

		-- PASS, HashID can be null in table, but proc enforces additional check
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = 'abc',
				@Path = 'xyz',
				@Actual = 0,
				@HashID = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '05. Minimum insert';

		-- FK conflict on FK_Directories_HashID
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = 'abc',
				@Path = 'xyz',
				@Actual = 0,
				@HashID = 0,
				@pass = 0,
				@expected = NULL,
				@testname = '06. HashID is not in FK';

		-- FK conflict on FK_Directories_HashID
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = 'abc',
				@Path = 'xyz',
				@Actual = 0,
				@HashID = 1,
				@pass = 0,
				@expected = NULL,
				@testname = '07. HashID is not in FK before insert';

		PRINT @Object + ': ' + SYSTEM_USER + ' ' + USER;
		-- Add a hash of each type we can reference
		EXEC	[data].[UpsertHash] 'directory', 'xyz';
		EXEC	[data].[UpsertHash] 'file', 'abc';
		PRINT @Object + ': ' + SYSTEM_USER + ' ' + USER;

		-- PASS, we are linking to the right type
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = 'abc',
				@Path = 'xyz',
				@Actual = 0,
				@HashID = 1,
				@pass = 1,
				@expected = 1,
				@testname = '08. HashID is a directory KeyType';

		-- Want to add some checking functions here

		-- PASS, this is an Upsert we are testing, not an insert
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = 'abc',
				@Path = 'xyz',
				@Actual = 0,
				@HashID = 1,
				@pass = 1,
				@expected = 1,
				@testname = '09. Duplicate call on abc, xyz';

		-- Want to check that nothing changed.

		-- Still a duplicate entry
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = 'abc',
				@Path = 'xyz',
				@Actual = 1,
				@HashID = 1,
				@pass = 1,
				@expected = 1,
				@testname = '10. Updating abc, xyz';

		-- Wrong KeyType for HashID
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = 'abc',
				@Path = 'xyz',
				@Actual = 0,
				@HashID = 2,
				@pass = 0,
				@expected = NULL,
				@testname = '11. HashID is a file KeyType';

		-- Wrong KeyType for HashID
		EXEC	[tests].[input_UpsertDirectory]
				@Volume = 'abc',
				@Path = 'xyz',
				@Actual = 0,
				@HashID = 3,
				@pass = 0,
				@expected = NULL,
				@testname = '12. HashID nonexistant';

		-- RECORD TEST SUITE PASS
		EXEC [tests].[Results] @object, '13. Test Suite', 'Attempted all tests', 1, 0;
	END TRY
	BEGIN CATCH
		-- RECORD TEST SUITE FAIL
		EXEC [tests].[*error_handler] @object, '13. Test Suite', 'Attempted all tests', 0, 0;

		RETURN -1
	END CATCH
END;