﻿/*
Excutes a single input test for UpsertHash with given parameters
----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-09
Updated:	2016-01-08
*/

CREATE PROCEDURE [tests].[input_UpsertHash](
	@KeyType NVARCHAR(9),
	@Hash NVARCHAR(32),
	@pass BIT,
	@expected INT,
	@testname VARCHAR(100)
) AS
BEGIN

	--------------------
	-- INITIALISATION --
	--------------------
	DECLARE @object VARCHAR(50) = 'UpsertHash';
	DECLARE @testnote VARCHAR(100);
	DECLARE @start DATETIME;
	DECLARE @ms INT = NULL;
	DECLARE @fail BIT = ~@pass;
	DECLARE @result INT = NULL;
	
	SELECT @testnote = 'call ' + [Value]
	FROM [strings].[input_UpsertHash](@KeyType, @Hash, @pass);

	-------------------
	-- START OF TEST --
	-------------------
	BEGIN TRY
		-- STABLISE START POINT
		EXEC [data].[*commit];

		-- TEST START
		SET @start = GETDATE();

		EXEC @result = [data].[UpsertHash] @KeyType, @Hash;

		SET @ms = [tests].[Since](@start);
		
		--EXEC _TestStatus @pass, @fail, @result, @expected, @testnote;
		/*
		PRINT 
			' pass:' + ISNULL(CONVERT(VARCHAR(12), @pass), 'NULL') +  
			' fail:' + ISNULL(CONVERT(VARCHAR(12), @fail), 'NULL') + 
			' result:' + ISNULL(CONVERT(VARCHAR(12), @result), 'NULL') + 
			' expected:' + ISNULL(CONVERT(VARCHAR(12), @expected), 'NULL') + ' for ' + @testnote;
		*/	
		
		-- REPORT RESULT
		IF @pass = 1 EXEC [tests].[Results] @object, @testname, @testnote, @pass, @ms
		ELSE EXEC [tests].[*error_handler] @object, @testname, @testnote, @pass, @ms;

		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;
		
		-- STABILISE END POINT
		EXEC [data].[*commit];

	END TRY
	BEGIN CATCH
		-- RESET TRANSACTION STATE
		EXEC [data].[*rollback];

		-- REPORT RESULT
		IF @ms IS NULL SET @ms = [tests].[Since](@start);
		-- EXEC _TestStatus @pass, @fail, @result, @expected, @testnote;
		/*
		PRINT 
			' pass:' + ISNULL(CONVERT(VARCHAR(12), @pass), 'NULL') +  
			' fail:' + ISNULL(CONVERT(VARCHAR(12), @fail), 'NULL') + 
			' result:' + ISNULL(CONVERT(VARCHAR(12), @result), 'NULL') + 
			' expected:' + ISNULL(CONVERT(VARCHAR(12), @expected), 'NULL') + ' for ' + @testnote;
			*/
		IF @fail = 1 EXEC [tests].[Results] @object, @testname, @testnote, @fail, @ms;
		ELSE EXEC [tests].[*error_handler] @object, @testname, @testnote, @fail, @ms;

		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;
	END CATCH;
END;