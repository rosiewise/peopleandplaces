﻿/*
Excutes a single input test for UpsertFileInfo with given parameters
--------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-10
Updated:	2016-01-08
*/

CREATE PROCEDURE [tests].[input_UpsertFileInfo](
	@FullPath NVARCHAR(MAX),
	@FileHash VARCHAR(34),
	@Created DATETIME,
	@Updated DATETIME,
	@Size INT,
	@InSynch BIT,
	@pass BIT,
	@expected INT,
	@testname VARCHAR(100)
) AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	DECLARE @object VARCHAR(50) = 'UpsertFileInfo';
	DECLARE @testnote VARCHAR(350);
	DECLARE @start DATETIME;
	DECLARE @ms INT = NULL;
	DECLARE @fail BIT = ~@pass;
	DECLARE @result INT = NULL;
		
	SELECT @testnote = 'call ' + [Value]
	FROM [strings].[input_UpsertFileInfo](@FullPath, @FileHash, @Created, @Updated, @Size, @InSynch, @pass);

	-------------------
	-- START OF TEST --
	-------------------
	BEGIN TRY
		-- STABLISE START POINT
		EXEC [data].[*commit];

		-- TEST START
		SET @start = GETDATE();

		EXEC [gui].[UpsertFileInfo] @FullPath, @FileHash, @Created, @Updated, @Size, @InSynch;

		SET @ms = [tests].[Since](@start);

		EXEC [tests].[Status] @pass, @fail, @result, @expected, @testnote;

		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;

		-- REPORT RESULT
		IF @pass = 1 EXEC [tests].[Results] @object, @testname, @testnote, @pass, @ms
		ELSE EXEC [tests].[*error_handler] @object, @testname, @testnote, @pass, @ms;
			
		-- STABILISE END POINT
		EXEC [data].[*commit];

	END TRY
	BEGIN CATCH
		-- RESET TRANSACTION STATE
		EXEC [data].[*rollback];

		EXEC [tests].[Status] @pass, @fail, @result, @expected, @testnote;

		-- REPORT RESULT
		IF @ms IS NULL SET @ms = [tests].[Since](@start);
		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;
		IF @fail = 1 EXEC [tests].[Results] @object, @testname, @testnote, @fail, @ms;
		ELSE EXEC [tests].[*error_handler] @object, @testname, @testnote, @fail, @ms;
	END CATCH;
END;