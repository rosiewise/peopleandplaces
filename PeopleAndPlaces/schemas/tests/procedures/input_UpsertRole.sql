﻿/*
Excutes a single input test for UpsertRole with given parameters
----------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-10
Updated:	2016-01-08
*/

CREATE PROCEDURE [tests].[input_UpsertRole](
	@ID INT,
	@Role NVARCHAR(50),
	@pass BIT,
	@expected INT,
	@testname VARCHAR(100)
) AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	DECLARE @object VARCHAR(50) = 'UpsertRole';
	DECLARE @testnote VARCHAR(350);
	DECLARE @start DATETIME;
	DECLARE @ms INT = NULL;
	DECLARE @fail BIT = ~@pass;
	DECLARE @result INT;
	
	SELECT @testnote = 'call ' + [Value]
	FROM [strings].[input_UpsertRole](@ID, @Role, @pass);

	-------------------
	-- START OF TEST --
	-------------------
	BEGIN TRY
		-- STABLISE START POINT
		EXEC [data].[*commit];

		-- TEST START
		SET @start = GETDATE();

		EXEC @result = [data].[UpsertRole] @ID, @Role;

		SET @ms = [tests].[Since](@start);

		EXEC [tests].[Status] @pass, @fail, @result, @expected, @testnote;

		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;

		-- REPORT RESULT
		IF (@pass = 1 AND @result > 0)
			EXEC [tests].[Results] @object, @testname, @testnote, 1, @ms
		ELSE
			EXEC [tests].[*error_handler] @object, @testname, @testnote, @fail, @ms;
			
		-- STABILISE END POINT
		EXEC [data].[*commit];

	END TRY
	BEGIN CATCH
		-- RESET TRANSACTION STATE
		EXEC [data].[*rollback];

		-- REPORT RESULT
		IF @ms IS NULL SET @ms = [tests].[Since](@start);

		EXEC [tests].[Status] @pass, @fail, @result, @expected, @testnote;

		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;
		IF @fail = 1 EXEC [tests].[Results] @object, @testname, @testnote, @fail, @ms;
		ELSE EXEC [tests].[*error_handler] @object, @testname, @testnote, @fail, @ms;
	END CATCH;
END;