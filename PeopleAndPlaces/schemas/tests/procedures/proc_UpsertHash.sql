﻿/*
Test Suite for UpsertHash procedure
------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-09
Updated:	2016-01-28
*/

CREATE PROCEDURE [tests].[proc_UpsertHash] AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	DECLARE @object VARCHAR(50);

	SET @object = 'UpsertHash';

	BEGIN TRY

		EXEC [tests].[*reset];

		--------------------
		-- EXPECTED FAILS --
		--------------------

		EXEC	[tests].[input_UpsertHash]
				@KeyType = NULL,
				@Hash = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '01. All null';

		EXEC	[tests].[input_UpsertHash]
				@KeyType = 'a',
				@Hash = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '02. Just invalid KeyType';

		EXEC	[tests].[input_UpsertHash]
				@KeyType = 'file',
				@Hash = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '03. Just valid KeyType (file)';

		EXEC	[tests].[input_UpsertHash]
				@KeyType = 'directory',
				@Hash = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '04. Just valid KeyType (directory)';

		EXEC	[tests].[input_UpsertHash]
				@KeyType = NULL,
				@Hash = 'a',
				@pass = 0,
				@expected = NULL,
				@testname = '05. Just Hash';

		EXEC	[tests].[input_UpsertHash]
				@KeyType = 'a',
				@Hash = 'a',
				@pass = 0,
				@expected = NULL,
				@testname = '06. Invalid KeyType';

		-- PASS, has valid key type
		EXEC	[tests].[input_UpsertHash]
				@KeyType = 'file',
				@Hash = 'a',
				@pass = 1,
				@expected = 1,
				@testname = '07. KeyType (file)';

		-- PASS, has valid key type but duplicate, so ignore
		EXEC	[tests].[input_UpsertHash]
				@KeyType = 'file',
				@Hash = 'a',
				@pass = 1,
				@expected = 1,
				@testname = '08. Test duplicate unique key ignore';

		-- PASS, has valid key type
		EXEC	[tests].[input_UpsertHash]
				@KeyType = 'directory',
				@Hash = 'a',
				@pass = 1,
				@expected = 2,
				@testname = '09. KeyType (directory)';

		-- RECORD TEST SUITE PASS
		EXEC [tests].[Results] @object, '10. Test Suite', 'Attempted all tests', 1, 0;
	END TRY
	BEGIN CATCH
		-- RECORD TEST SUITE FAIL
		EXEC [tests].[*error_handler] @object, '10. Test Suite', 'Attempted all tests', 0, 0;

		RETURN -1
	END CATCH
END;