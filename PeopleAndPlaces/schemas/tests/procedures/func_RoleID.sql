﻿/*
Test Suite for RoleID function
------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-31
Updated:	2016-01-28
*/

CREATE PROCEDURE [tests].[func_RoleID] AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	DECLARE @object VARCHAR(50);

	SET @object = 'RoleID';

	--DECLARE @trancount INT;
	--SET @trancount = @@TRANCOUNT;

	BEGIN TRY

		EXEC [tests].[*reset];

		-- For these tests, a FAIL is a False or empty result
		PRINT @Object + ': ' + SYSTEM_USER + ' ' + USER;
		SELECT * FROM [data].[PersonRoles];

		INSERT INTO [data].[PersonRoles] ([Role])
		VALUES ('Guitar'), ('Bass'), ('Drums'), ('Lead');

		SELECT * FROM [data].[PersonRoles];
		PRINT @Object + ': ' + SYSTEM_USER + ' ' + USER;

		-- Role can't be null
		EXEC	[tests].[input_RoleID]
				@Role = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '01. All null';
		
		-- Role is bad data
		EXEC	[tests].[input_RoleID]
				@Role = 1,
				@pass = 0,
				@expected = NULL,
				@testname = '02. INT Role';
		
		-- Role is fake
		EXEC	[tests].[input_RoleID]
				@Role = 'Fake',
				@pass = 0,
				@expected = NULL,
				@testname = '03. Invalid Role';

		-- Role is valid
		EXEC	[tests].[input_RoleID]
				@Role = 'Guitar',
				@pass = 1,
				@expected = 1,
				@testname = '04. Valid Role';

		-- Role is valid
		EXEC	[tests].[input_RoleID]
				@Role = 'Bass',
				@pass = 1,
				@expected = 2,
				@testname = '05. Valid Role';

		-- Role is valid
		EXEC	[tests].[input_RoleID]
				@Role = 'Drums',
				@pass = 1,
				@expected = 3,
				@testname = '06. Valid Role';

		-- Role is valid
		EXEC	[tests].[input_RoleID]
				@Role = 'Lead',
				@pass = 1,
				@expected = 4,
				@testname = '07. Valid Role';

		-- Role is partly valid
		EXEC	[tests].[input_RoleID]
				@Role = 'Bas',
				@pass = 0,
				@expected = NULL,
				@testname = '08. Role is part matched';

		-- RECORD TEST SUITE PASS
		EXEC [tests].[Results] @object, '09. Test Suite', 'Attempted all tests', 1, 0;
	END TRY
	BEGIN CATCH
		-- RECORD TEST SUITE FAIL
		EXEC [tests].[*error_handler] @object, '09. Test Suite', 'Attempted all tests', 0, 0;

		RETURN -1
	END CATCH
END;