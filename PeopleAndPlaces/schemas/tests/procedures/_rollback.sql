﻿CREATE PROCEDURE [tests].[*rollback]
WITH EXECUTE AS 'data_user'
AS
	EXEC [data].[*commit];
RETURN 0