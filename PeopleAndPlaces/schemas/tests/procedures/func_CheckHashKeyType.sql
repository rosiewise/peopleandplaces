﻿/*
Test Suite for CheckHashKeyType procedure
-----------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-29
Updated:	2016-01-28
*/

CREATE PROCEDURE [tests].[func_CheckHashKeyType]
WITH EXECUTE AS 'data_user'
AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	DECLARE @object VARCHAR(50);

	SET @object = 'CheckHashKeyType';

	--DECLARE @trancount INT;
	--SET @trancount = @@TRANCOUNT;

	BEGIN TRY

		EXEC [tests].[*reset];

		-- For these tests, a FAIL is a False or empty result

		--EXECUTE AS USER = 'data_user';
			-- Add a hash of each type we can reference
			EXEC	[data].[UpsertHash] 'directory', 'xyz';
			EXEC	[data].[UpsertHash] 'file', 'abc';
		--REVERT;

		-- ID can't be null
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = NULL,
				@KeyType = NULL,
				@pass = 0,
				@expected = 0,
				@testname = '01. All null';

		-- ID is bad data
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = '1',
				@KeyType = NULL,
				@pass = 0,
				@expected = 0,
				@testname = '02. VCHAR ID';

		-- ID is valid
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = 1,
				@KeyType = NULL,
				@pass = 0,
				@expected = 0,
				@testname = '03. Valid ID only';

		-- ID is invalid
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = 3,
				@KeyType = NULL,
				@pass = 0,
				@expected = 0,
				@testname = '04. Invalid ID only';

		-- KeyType is bad data
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = NULL,
				@KeyType = -1,
				@pass = 0,
				@expected = 0,
				@testname = '05. INT KeyType';

		-- KeyType is valid
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = NULL,
				@KeyType = 'file',
				@pass = 0,
				@expected = 0,
				@testname = '06. Valid KeyType only';

		-- KeyType is invalid
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = NULL,
				@KeyType = 'bad',
				@pass = 0,
				@expected = 0,
				@testname = '07. Invalid KeyType only';

		-- ID, KeyType is bad data
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = 1,
				@KeyType = -1,
				@pass = 0,
				@expected = 0,
				@testname = '08. ID, INT KeyType';

		-- ID, KeyType is valid but wrong for ID
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = 1,
				@KeyType = 'file',
				@pass = 0,
				@expected = 0,
				@testname = '09. ID, valid KeyType but not expected';

		-- ID, KeyType is invalid
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = 1,
				@KeyType = 'bad',
				@pass = 0,
				@expected = 0,
				@testname = '10. ID, invalid KeyType';

		-- ID, KeyType is valid
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = 1,
				@KeyType = 'directory',
				@pass = 1,
				@expected = 1,
				@testname = '11. ID, valid KeyType';

		-- ID, KeyType is valid
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = 2,
				@KeyType = 'file',
				@pass = 1,
				@expected = 1,
				@testname = '12. ID, valid KeyType';

		-- ID and KeyType is bad data
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = -1,
				@KeyType = -1,
				@pass = 0,
				@expected = 0,
				@testname = '13. ID, KeyType both bad';

		-- invalid ID, KeyType is valid but wrong for ID
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = 3,
				@KeyType = 'file',
				@pass = 0,
				@expected = 0,
				@testname = '14. invalid ID, valid KeyType but not expected';

		-- invalid ID, KeyType is invalid
		EXEC	[tests].[input_CheckHashKeyType]
				@ID = 3,
				@KeyType = 'bad',
				@pass = 0,
				@expected = 0,
				@testname = '15. invalid ID, invalid KeyType';

		-- RECORD TEST SUITE PASS
		EXEC [tests].[Results] @object, '16. Test Suite', 'Attempted all tests', 1, 0;
	END TRY
	BEGIN CATCH
		-- RECORD TEST SUITE FAIL
		EXEC [tests].[*error_handler] @object, '16. Test Suite', 'Attempted all tests', 0, 0;

		RETURN -1
	END CATCH
END;