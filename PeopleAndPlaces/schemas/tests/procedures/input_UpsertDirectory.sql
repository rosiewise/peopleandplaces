﻿/*
Excutes a single input test for UpsertDirectory with given parameters
---------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-28
Updated:	2016-01-08
*/

CREATE PROCEDURE [tests].[input_UpsertDirectory](
	@Volume NVARCHAR(5),
	@Path NVARCHAR(255),
	@Actual BIT,
	@HashID INT,
	@pass BIT,
	@expected INT,
	@testname VARCHAR(100)
) AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	DECLARE @xstate SMALLINT;
	DECLARE @object VARCHAR(50) = 'UpsertDirectory';
	DECLARE @testnote VARCHAR(350);
	DECLARE @start DATETIME;
	DECLARE @ms INT = NULL;
	DECLARE @fail BIT = ~@pass;
	DECLARE @result INT = NULL;
		
	SELECT @testnote = 'call ' + [Value]
	FROM [strings].[input_UpsertDirectory](@Volume, @Path, @Actual, @HashID, @pass);

	-------------------
	-- START OF TEST --
	-------------------
	BEGIN TRY
		-- STABLISE START POINT
		EXEC [data].[*commit];

		-- TEST START
		SET @start = GETDATE();

		EXEC @result = [data].[UpsertDirectory] @Volume, @Path, @Actual, @HashID;

		SET @ms = [tests].[Since](@start);

		EXEC [tests].[Status] @pass, @fail, @result, @expected, @testnote;

		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;

		-- REPORT RESULT
		IF @pass = 1 EXEC [tests].[Results] @object, @testname, @testnote, @pass, @ms
		ELSE EXEC [tests].[*error_handler] @object, @testname, @testnote, @pass, @ms;
			
		-- STABILISE END POINT
		EXEC [data].[*commit];

	END TRY
	BEGIN CATCH
		-- RESET TRANSACTION STATE
		EXEC [data].[*rollback];

		-- REPORT RESULT
		IF @ms IS NULL SET @ms = [tests].[Since](@start);

		EXEC [tests].[Status] @pass, @fail, @result, @expected, @testnote;

		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;
		IF @fail = 1 EXEC [tests].[Results] @object, @testname, @testnote, @fail, @ms;
		ELSE EXEC [tests].[*error_handler] @object, @testname, @testnote, @fail, @ms;
	END CATCH;
END;