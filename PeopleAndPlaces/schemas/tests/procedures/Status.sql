﻿/*
Prints out the status for a test
---------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2016-01-08
Updated:	
*/

CREATE PROCEDURE [tests].[Status]
	@pass BIT,
	@fail BIT,
	@result INT,
	@expected INT,
	@testnote VARCHAR(MAX)
AS
	PRINT 
		' pass:' + ISNULL(CONVERT(VARCHAR(12), @pass), 'NULL') +  
		' fail:' + ISNULL(CONVERT(VARCHAR(12), @fail), 'NULL') + 
		' result:' + ISNULL(CONVERT(VARCHAR(12), @result), 'NULL') + 
		' expected:' + ISNULL(CONVERT(VARCHAR(12), @expected), 'NULL') + ' for ' + @testnote;
RETURN 0