﻿/*
Test Suite for FileID function
------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-20
Updated:	2016-01-28
*/

CREATE PROCEDURE [tests].[func_FileID] AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	SET NOCOUNT ON;

	DECLARE @trancount INT = @@TRANCOUNT;
	DECLARE @object VARCHAR(50) = 'FileID';

	BEGIN TRY

		EXEC [tests].[*reset];

		EXEC [gui].[UpsertFileInfo] 'c:\images\a.jpg', 'ahash';
		EXEC [gui].[UpsertFileInfo] 'c:\images\b.jpg', 'bhash';
		EXEC [gui].[UpsertFileInfo] 'd:\images\a.jpg', 'ahash';
		EXEC [gui].[UpsertFileInfo] 'd:\images\b.jpg', 'bhash';

		--EXEC [gui].[UpsertFileInfo] 'c', 'images', 'a', 'jpg', 'image/jpg', 'ahash', 'directoryhash1';
		--EXEC [gui].[UpsertFileInfo] 'c', 'images', 'b', 'jpg', 'image/jpg', 'bhash', 'directoryhash1';
		--EXEC [gui].[UpsertFileInfo] 'c', 'images', 'c', 'jpg', 'image/jpg', 'chash', 'directoryhash1';
		--EXEC [gui].[UpsertFileInfo] 'd', 'images', 'a', 'jpg', 'image/jpg', 'ahash', 'directoryhash1';
		--EXEC [gui].[UpsertFileInfo] 'd', 'images', 'b', 'jpg', 'image/jpg', 'bhash', 'directoryhash1';
		--EXEC [gui].[UpsertFileInfo] 'd', 'images', 'c', 'jpg', 'image/jpg', 'chash', 'directoryhash1';
		/*
		INSERT INTO [data].[Files] ([DirectoryID], [FileName], [Extension])
		VALUES (1, 'a'), (1, 'b'), (2, 'a'), (2, 'b');*/

		-----------------------------------
		-- TEST EXPECTED TO FAIL FATALLY --
		-----------------------------------
		BEGIN TRY
			EXEC	[tests].[*commit];
			EXEC	[tests].[input_FileID]
					@pass = 0,
					@expected = NULL,
					@testname = '01. No input';
			EXEC	[tests].[*commit];
		END TRY
		BEGIN CATCH
			EXEC [tests].[*rollback];
			EXEC [tests].[Results] @object, '01. No input', 'call () fails', 1, 0;
		END CATCH;

		BEGIN TRY
			EXEC	[tests].[*commit];
			EXEC	[tests].[input_FileID]
					@FileName = 'a',
					@pass = 0,
					@expected = NULL,
					@testname = '02. No DirectoryID';
			EXEC	[tests].[*commit];
		END TRY
		BEGIN CATCH
			EXEC [tests].[*rollback];
			EXEC [tests].[Results] @object, '02. No DirectoryID', 'call (''a'') fails', 1, 0;
		END CATCH;

		BEGIN TRY
			EXEC	[tests].[*commit];
			EXEC	[tests].[input_FileID]
					@DirectoryID = 1,
					@pass = 0,
					@expected = 1,
					@testname = '03. No FileName';
			EXEC	[tests].[*commit];
		END TRY
		BEGIN CATCH
			EXEC [tests].[*rollback];
			EXEC [tests].[Results] @object, '03. No FileName', 'call (1) fails', 1, 0;
		END CATCH;

		-------------------------------------------
		-- EVERYTHING ELSE THAT FAILS GRACEFULLY --
		-------------------------------------------

		EXEC	[tests].[input_FileID]
				@DirectoryID = NULL,
				@FileName = NULL,
				@Extension = NULL,
				@pass = 0,
				@expected = NULL,
				@testname = '04. All null';
		
		BEGIN TRY
			EXEC	[tests].[*commit];
			EXEC	[tests].[input_FileID]
					@DirectoryID = 'a',
					@FileName = NULL,
					@pass = 0,
					@expected = NULL,
					@testname = '05. varchar DirectoryID';
			EXEC	[tests].[*commit];
		END TRY
		BEGIN CATCH
			EXEC [tests].[*rollback]; --@@PROCID, @trancount;
			EXEC [tests].[Results] @object, '05. varchar DirectoryID', 'call (''a'', NULL) fails', 1, 0;
		END CATCH;

		EXEC	[tests].[input_FileID]
				@DirectoryID = NULL,
				@FileName = 1,
				@Extension = 'jpg',
				@pass = 0,
				@expected = NULL,
				@testname = '06. INT FileName';
		
		EXEC	[tests].[input_FileID]
				@DirectoryID = 100,
				@FileName = NULL,
				@Extension = 'jpg',
				@pass = 0,
				@expected = NULL,
				@testname = '07. Invalid DirectoryID';

		EXEC	[tests].[input_FileID]
				@DirectoryID = NULL,
				@FileName= 'fake',
				@Extension = 'jpg',
				@pass = 0,
				@expected = NULL,
				@testname = '08. Invalid DirectoryID, valid FileName';

		EXEC	[tests].[input_FileID]
				@DirectoryID = 1,
				@FileName = NULL,
				@Extension = 'jpg',
				@pass = 0,
				@expected = NULL,
				@testname = '09. Valid DirectoryID, invalid FileName';

		EXEC	[tests].[input_FileID]
				@DirectoryID = 1,
				@FileName = 'a',
				@Extension = 'jpg',
				@pass = 1,
				@expected = 1,
				@testname = '10. Valid DirectoryID, FileName combo';

		EXEC	[tests].[input_FileID]
				@DirectoryID = 1,
				@FileName = 'b',
				@Extension = 'jpg',
				@pass = 1,
				@expected = 2,
				@testname = '11. Valid DirectoryID, FileName combo';

		EXEC	[tests].[input_FileID]
				@DirectoryID = 1,
				@FileName = 'c',
				@Extension = 'jpg',
				@pass = 0,
				@expected = NULL,
				@testname = '12. Valid DirectoryID, invalid FileName';

		EXEC	[tests].[input_FileID]	
				@DirectoryID = 2,
				@FileName = 'a',
				@Extension = 'jpg',
				@pass = 1,
				@expected = 3,
				@testname = '13. Valid DirectoryID, FileName combo';

		EXEC	[tests].[input_FileID]
				@DirectoryID = 2,
				@FileName = 'b',
				@Extension = 'jpg',
				@pass = 1,
				@expected = 4,
				@testname = '14. Valid DirectoryID, FileName combo';

		EXEC	[tests].[input_FileID]
				@DirectoryID = 2,
				@FileName = 'c',
				@Extension = 'jpg',
				@pass = 0,
				@expected = NULL,
				@testname = '15. Valid DirectoryID, invalid FileName';

		-- RECORD TEST SUITE PASS
		EXEC [tests].[Results] @object, '16. Test Suite', 'Attempted all tests', 1, 0;
	END TRY
	BEGIN CATCH
		-- RECORD TEST SUITE FAIL
		EXEC [tests].[*error_handler] @object, '16. Test Suite', 'Attempted all tests', 0, 0;

		RETURN -1
	END CATCH
END;