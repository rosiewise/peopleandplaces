﻿/*
Excutes the test suites
-----------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-09
Updated:	2015-11-17
*/

CREATE PROCEDURE [tests].[Do]
AS
BEGIN TRANSACTION;
	SET NOCOUNT ON;
	--SET XACT_ABORT OFF;
	--DECLARE @trancount INT = @@TRANCOUNT;

	DELETE FROM [tests].[_TestResults] WHERE [Date] < CURRENT_TIMESTAMP;

	EXEC [tests].[*commit];

	EXEC [tests].[func_CheckHashKeyType]; EXEC [tests].[*commit];
	EXEC [tests].[func_CountryID]; EXEC [tests].[*commit];
	EXEC [tests].[func_FileID]; EXEC [tests].[*commit];
	EXEC [tests].[func_HashID]; EXEC [tests].[*commit];
	EXEC [tests].[func_OrganisationID]; EXEC [tests].[*commit];
	EXEC [tests].[func_RegionID]; EXEC [tests].[*commit];
	EXEC [tests].[func_RoleID]; EXEC [tests].[*commit];

	EXEC [tests].[proc_UpsertHash]; EXEC [tests].[*commit];
	EXEC [tests].[proc_UpsertDirectory]; EXEC [tests].[*commit]; -- Relies on CheckHashKeyType
	EXEC [tests].[proc_UpsertRole]; EXEC [tests].[*commit]; -- Relies on RoleID

	EXEC [tests].[*reset];
	DECLARE @Created DATETIME = DATEADD(MONTH, -6, GETDATE());
	DECLARE @Updated DATETIME = DATEADD(DAY, -6, GETDATE());
	EXEC [tests].[input_UpsertFileInfo]
		@FullPath = 'c:\images\image.jpg',
		@FileHash = 'hash',
		@Created = @Created,
		@Updated = @Updated,
		@Size = 2034945,
		@InSynch = true,
		@pass = true,
		@expected = 1,
		@testname = 'input_UpsertFileInfo basic test';

	/*
	EXEC [_Test_Table_Addresses];
	EXEC [_Test_Table_Associations]; -- DONE
	EXEC [_Test_Table_PersonRoles]; -- DONE
	EXEC [_Test_Table_Countries];

	EXEC [_Test_PK_Addresses];
	EXEC [_Test_PK_Associations];
	EXEC [_Test_PK_Countries];
	EXEC [_Test_PK_Directories];
	EXEC [_Test_PK_Files];
	--EXEC [_Test_PK_Hashes];
	EXEC [_Test_PK_Images];
	EXEC [_Test_PK_Locations];
	EXEC [_Test_PK_Organisations];
	EXEC [_Test_PK_PersonRoles];
	EXEC [_Test_PK_People];
	EXEC [_Test_PK_Places];
	EXEC [_Test_PK_Regions];
	*/

	SELECT * FROM [tests].[Summary];
	SELECT * FROM [tests].[Fails]();
	SELECT * FROM [tests].[_TestResults] ORDER BY [Pass], [Object], [TestName];

	--EXEC [*commit_or_save] @@PROCID, @trancount;

	--SET XACT_ABORT ON;
COMMIT TRANSACTION;
RETURN 0