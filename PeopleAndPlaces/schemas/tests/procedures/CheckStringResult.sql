﻿/*
Checks and records a varchar test result
----------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-11-13
Updated:
*/

CREATE PROCEDURE [tests].[CheckStringResult] (
	@result VARCHAR(MAX),
	@expected VARCHAR(MAX),
	@object VARCHAR(50),
	@testname VARCHAR(100),
	@ms INT = NULL,
	@testnote VARCHAR(350) = 'Expected result?'
) AS
BEGIN
	IF @result = @expected OR (@result IS NULL AND @expected IS NULL)
		EXEC Results @object, @testname, @testnote, 1, @ms
	ELSE
		EXEC Results @object, @testname, @testnote, 0, @ms;
END;