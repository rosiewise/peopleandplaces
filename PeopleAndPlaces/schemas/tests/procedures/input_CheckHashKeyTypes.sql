﻿/*
Excutes a single input test for CheckHashKeyType with given parameters
----------------------------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-10-28
Updated:	2015-11-15
*/

CREATE PROCEDURE [tests].[input_CheckHashKeyType](
	@ID INT,
	@KeyType NVARCHAR(9),
	@pass BIT,
	@expected BIT,
	@testname VARCHAR(100)
) AS
BEGIN
	--------------------
	-- INITIALISATION --
	--------------------
	DECLARE @object VARCHAR(50) = 'CheckHashKeyType';
	DECLARE @testnote VARCHAR(350);
	DECLARE @start DATETIME;
	DECLARE @ms INT = NULL;
	DECLARE @fail BIT = ~@pass;
	DECLARE @result BIT = 0;
	DECLARE @count BIT = 0;

	SELECT @testnote = 'call ' + [Value]
	FROM [strings].[input_CheckHashKeyType](@ID, @KeyType, @pass);

	-------------------
	-- START OF TEST --
	-------------------
	BEGIN TRY
		-- STABLISE START POINT
		EXEC [data].[*commit];

		-- TEST START
		SET @start = GETDATE();

		SELECT @count = COUNT([Value]),
			@result = [Value]
		FROM [data].[CheckHashKeyType](@ID, @KeyType)
		GROUP BY [Value];
		
		SET @ms = [tests].[Since](@start);

		EXEC [tests].[Status] @pass, @fail, @result, @expected, @testnote;

		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;

		-- REPORT RESULT
		IF (@pass = 1 AND @count = 1 AND @result = 1) OR (@fail = 1 AND (@count = 0 OR @result = 0))
			EXEC [tests].[Results] @object, @testname, @testnote, 1, @ms;
		ELSE
			EXEC [tests].[*error_handler] @object, @testname, @testnote, 0, @ms;
			
		-- STABILISE END POINT
		EXEC [data].[*commit];

	END TRY
	BEGIN CATCH
		-- RESET TRANSACTION STATE
		EXEC [data].[*rollback];

		-- REPORT RESULT
		IF @ms IS NULL SET @ms = [tests].[Since](@start);

		EXEC [tests].[Status] @pass, @fail, @result, @expected, @testnote;

		EXEC [tests].[CheckIntResult] @result, @expected, @object, @testname, @ms;
		IF @fail = 1 EXEC [tests].[Results] @object, @testname, @testnote, @fail, @ms;
		ELSE EXEC [tests].[*error_handler] @object, @testname, @testnote, @fail, @ms;
	END CATCH;
END;