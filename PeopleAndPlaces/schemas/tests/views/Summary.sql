﻿CREATE VIEW [tests].[Summary]
	AS
SELECT [Object],
	COUNT([Object]) AS 'Tests',
	SUM(CASE [Pass] WHEN 1 THEN 1 ELSE 0 END) AS 'Passes',
	SUM(CASE [Pass] WHEN 1 THEN 0 ELSE 1 END) AS 'Fails',
	SUM([Time]) AS 'Total Time',
	CONVERT(DECIMAL(6,2), SUM([Time]) / COUNT([Object])) AS 'Avg Time'
FROM [tests].[_TestResults]
GROUP BY [Object]