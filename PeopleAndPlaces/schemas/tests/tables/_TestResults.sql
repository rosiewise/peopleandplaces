﻿CREATE TABLE [tests].[_TestResults] (
    [Date]           DATETIME        NOT NULL,
    [Object]         VARCHAR (50)    NOT NULL,
    [TestName]       VARCHAR (100)   NOT NULL,
    [Note]           VARCHAR (MAX)   NOT NULL,
    [Pass]           BIT             NOT NULL,
    [Time]           INT             NULL,
    [Error]          NVARCHAR (4000) NULL,
    [ErrorNumber]    INT             NULL,
    [ErrorSeverity]  INT             NULL,
    [ErrorState]     INT             NULL,
    [ErrorProcedure] NVARCHAR (128)  NULL,
    [ErrorLine]      INT             NULL,
    [ShortNote]      VARCHAR (25)    DEFAULT ('') NOT NULL,
    CONSTRAINT [PK__TestResults] PRIMARY KEY CLUSTERED ([Object] ASC, [TestName] ASC, [ShortNote] ASC)
);