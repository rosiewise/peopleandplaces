﻿/*
Returns a list of test failures that need attention
---------------------------------------------------
Author:		Rosemarie Wise <rosie@websiteowner.info>
Created:	2015-07-01
Updated:	2015-11-13
*/

CREATE FUNCTION [tests].[Fails]()
RETURNS @returntable TABLE
(
    [Date]           DATETIME        NOT NULL,
    [Object]         VARCHAR (50)    NOT NULL,
	[TestName]		 VARCHAR (100)	 NOT NULL,
    [Note]           VARCHAR (500)   NOT NULL,
    [Pass]           BIT             NOT NULL,
    [Time]           INT             NULL,
    [Error]          NVARCHAR (4000) NULL,
    --[ErrorNumber]    INT             NULL,
    --[ErrorSeverity]  INT             NULL,
    --[ErrorState]     INT             NULL,
    [ErrorProcedure] NVARCHAR (128)  NULL,
    [ErrorLine]      INT             NULL
)
AS
BEGIN
	INSERT @returntable
	SELECT [Date],
		[Object],
		[TestName],
		[Note],
		[Pass],
		[Time],
		[Error],
		--[ErrorNumber],
		--[ErrorSeverity],
		--[ErrorState],
		[ErrorProcedure],
		[ErrorLine]
	FROM [tests].[_TestResults]
	WHERE [Pass] = 0;
	RETURN
END