﻿CREATE SCHEMA [gui];
GO

CREATE TYPE [gui].[ListFiles] AS TABLE (
	[FilePathID] INT,
	[FilePath] NVARCHAR(MAX),
	[File] NVARCHAR(260),
	[DirID] INT NOT NULL,
	[Drive] NVARCHAR(5) NOT NULL,
	[Path] NVARCHAR(MAX) NOT NULL,
	[Folder] NVARCHAR(200) NULL,
	[DirActual] BIT NOT NULL,
	[DirHashID] INT NOT NULL,
	[DirHashKeyType] VARCHAR(9) NOT NULL,
	[DirHash] VARCHAR(34) NOT NULL,
	[FileID] INT NOT NULL,
	[FileDirID] INT NULL,
	[FileName] NVARCHAR(255) NOT NULL,
	[Extension] NVARCHAR(5) NOT NULL,
	[MimeType] NVARCHAR(50) NOT NULL,
	[Actual] BIT NULL,
	[Size] INT NULL,
	[Created] DATETIME NULL,
	[Updated] DATETIME NULL,
	[InSynch] BIT NULL,
	[Synchronised] DATETIME NULL,
	[RowUpdated] DATETIME NULL,
	[Imported] DATETIME NULL,
	[FileHashID] INT NOT NULL,
	[FileHashKeyType] VARCHAR(9) NOT NULL,
	[FileHash] VARCHAR(34) NOT NULL,
	PRIMARY KEY ([FilePathID])
);
GO

CREATE TYPE [gui].[ListDateTimes] AS TABLE
(
	[ID] INT NOT NULL,
	[DateTime] DATETIME NULL,
	--[Year] INT NULL, -- DERIVED
	--[Quarter] INT NULL, -- DERIVED
	--[Month] INT NULL, -- DERIVED
	--[Day] INT NULL, -- DERIVED
	--[ISOWeek] INT NULL, -- DERIVED
	--[Weekday] VARCHAR(10) NULL, -- DERIVED
	--[Date] DATE NULL, -- DERIVED
	--[Hour] INT NULL, -- DERIVED
	PRIMARY KEY ([ID])
);
GO