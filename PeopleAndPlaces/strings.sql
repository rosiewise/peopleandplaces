﻿CREATE SCHEMA [strings]
GO

CREATE TYPE [strings].[ListInt] AS TABLE
(
	[ID] INT NOT NULL,
	PRIMARY KEY ([ID])
);
GO

CREATE TYPE [strings].[ListFilePaths] AS TABLE
(
	[ID] INT NOT NULL,
	[FilePath] NVARCHAR(MAX) NOT NULL,
	PRIMARY KEY ([ID])
);
GO

CREATE TYPE [strings].[ListNVarChars] AS TABLE
(
	[ID] INT NOT NULL,
	[String] NVARCHAR(MAX) NOT NULL,
	PRIMARY KEY ([ID])
);
GO

CREATE TYPE [strings].[SplitStringGroups] AS TABLE
(
	[ID] INT NOT NULL,
	[SortOrder] INT NOT NULL,
	[Token] NVARCHAR (4000) NULL
);
GO

CREATE TYPE [strings].[SplitStringOrdering] AS TABLE
(
	[ID] INT NOT NULL,
	[SortOrder] INT NOT NULL,
	[Token] NVARCHAR (4000) NULL,
	[ReverseOrder] INT NOT NULL
);
GO